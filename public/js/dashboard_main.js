/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 8);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/dashboard_main.js":
/*!***********************************************!*\
  !*** ./resources/assets/js/dashboard_main.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ../ts/Post */ "./resources/assets/ts/Post.ts");

__webpack_require__(/*! ../ts/PostLink */ "./resources/assets/ts/PostLink.ts");

__webpack_require__(/*! ../ts/BatchAction */ "./resources/assets/ts/BatchAction.ts");

/***/ }),

/***/ "./resources/assets/ts/BatchAction.ts":
/*!********************************************!*\
  !*** ./resources/assets/ts/BatchAction.ts ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var BatchAction = /** @class */ (function () {
    function BatchAction() {
        this.triggerCheckBoxes();
        this.childrenCheckBox = '[data-children-checkbox]';
        this.submitSelected();
    }
    BatchAction.prototype.triggerCheckBoxes = function () {
        var cls = this;
        $('[data-master-checkbox]').on('change', function (e) {
            if ($(e.currentTarget).is(':checked')) {
                $(cls.childrenCheckBox).prop('checked', true);
            }
            else {
                $(cls.childrenCheckBox).prop('checked', false);
            }
        });
    };
    BatchAction.prototype.submitSelected = function () {
        var cls = this;
        $(document).on('click', '[data-checkbox-trigger]', function (e) {
            e.preventDefault();
            var data = [];
            $(cls.childrenCheckBox + ':checked').each(function () {
                data.push($(this).val());
            });
            var form = cls.setForm($(e.currentTarget).attr('href'), data);
            $(e.currentTarget).append(form.join(" "));
            $('#BatchDeleteForm').submit();
        });
    };
    BatchAction.prototype.setForm = function (href, data) {
        var csrf = $('meta[name="csrf-token"]').attr('content');
        var template = [
            '<form id="BatchDeleteForm" action="' + href + '" method="POST" style="display: none;">',
            '<input type="hidden" name="_token" value="' + csrf + '">',
            '<textarea name="deleteIds">' + JSON.stringify(data) + '</textarea>',
            '</form>'
        ];
        return template;
    };
    return BatchAction;
}());
var __batchAction = new BatchAction();


/***/ }),

/***/ "./resources/assets/ts/Post.ts":
/*!*************************************!*\
  !*** ./resources/assets/ts/Post.ts ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var Post = /** @class */ (function () {
    function Post(form) {
        this.form = form;
        this.sendAjax();
    }
    Post.prototype.beforeSend = function (xhr, currentForm) {
        var Spinner = $('body').attr("data-sinner");
        currentForm.find(':input').prop('disabled', true);
        currentForm.find(':input[type="submit"]').html('<img src="' + Spinner + '" width="20" uk-responsive>');
    };
    Post.prototype.sendAjax = function () {
        var form = $(this.form);
        var cls = this;
        $(document).on('submit', this.form, function (e) {
            var currentForm = $(e.currentTarget);
            var btnText = currentForm.find(':input[type="submit"]').text();
            e.preventDefault();
            $.ajax({
                url: currentForm.attr('action'),
                type: currentForm.attr('method'),
                data: currentForm.serializeArray(),
                beforeSend: function (xhr) {
                    cls.beforeSend(xhr, currentForm);
                },
                timeout: 20000
            })
                .fail(function (jqXHR, textStatus, errorThrown) {
                cls.errorHandler(jqXHR, textStatus, errorThrown, btnText, currentForm);
            })
                .done(function (data, textStatus, jqXHR) {
                cls.success(data, textStatus, jqXHR, currentForm, btnText);
            });
        });
    };
    Post.prototype.success = function (data, textStatus, jqXHR, curForm, btnText) {
        if (jqXHR.status == 200) {
            curForm.find('.has-error').removeClass('has-error');
            curForm.find('.help-block').text('');
            curForm.find(':input[type="submit"]').text(btnText);
            if (data.message) {
                toastr.success(data.message);
            }
            curForm.find(':input[type="submit"]').text(btnText);
            if (!data.isPaused) {
                var wait = setInterval(function () {
                    window.location.href = data.redirect_url;
                }, 1000);
            }
            else {
                window.location.href = data.redirect_url;
            }
        }
    };
    Post.prototype.errorHandler = function (jqXHR, textStatus, errorThrown, btnText, curForm) {
        // console.log(jqXHR.status);
        if (jqXHR.status == 500 || jqXHR.status == 422) {
            var data = JSON.parse(jqXHR.responseText);
            if (typeof data.errors === 'string' || data.errors instanceof String) {
                toastr.error(data.errors, "Error");
                curForm.find(':input').prop('disabled', false);
                curForm.find(':input[type="submit"]').text(btnText);
            }
            else {
                curForm.find('.has-error').removeClass('has-error');
                curForm.find('.help-block').text('');
                $.each(data.errors, function (ind, val) {
                    curForm.find('.' + ind).addClass('has-error');
                    curForm.find('.' + ind + ' .help-block').text(val[0]);
                });
                curForm.find(':input').prop('disabled', false);
                curForm.find(':input[type="submit"]').text(btnText);
            }
        }
        else if (jqXHR.statusText == "timeout") {
            curForm.find('.has-error').removeClass('has-error');
            curForm.find('.help-block').text('');
            toastr.error('Request Timeout! Please try again.');
            curForm.find(':input').prop('disabled', false);
            curForm.find(':input[type="submit"]').text(btnText);
        }
    };
    return Post;
}());
$(function () {
    var __initPost = new Post("[data-post]");
});


/***/ }),

/***/ "./resources/assets/ts/PostLink.ts":
/*!*****************************************!*\
  !*** ./resources/assets/ts/PostLink.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var PostLink = /** @class */ (function () {
    function PostLink() {
        this._el = $('.post-link');
        this.csrf = $('meta[name="csrf-token"]').attr('content');
        this.setLink();
    }
    ;
    PostLink.prototype.setLink = function () {
        var cls = this;
        $(document).on('click', '.post-link', function (e) {
            e.preventDefault();
            var __tar = $(e.currentTarget);
            var id = cls.generateId(7);
            var template = cls.setTemplate(__tar.attr('href'), id, cls.csrf);
            __tar.after(template.join(" "));
            $('#' + id).submit();
        });
    };
    PostLink.prototype.setTemplate = function (href, id, csrf) {
        var template = [
            '<form id="' + id + '" action="' + href + '" method="POST" style="display: non;">',
            '<input type="hidden" name="_token" value="' + csrf + '">',
            '</form>'
        ];
        return template;
    };
    PostLink.prototype.generateId = function (len) {
        var text = "";
        var charset = "abcdefghjklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXZ";
        for (var i = 0; i < len; i++)
            text += charset.charAt(Math.floor(Math.random() * charset.length));
        return text;
    };
    return PostLink;
}());
var ___initPostLink = new PostLink();


/***/ }),

/***/ 8:
/*!*****************************************************!*\
  !*** multi ./resources/assets/js/dashboard_main.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\xampp\htdocs\suvenn\resources\assets\js\dashboard_main.js */"./resources/assets/js/dashboard_main.js");


/***/ })

/******/ });