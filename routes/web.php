<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

 
Route::group(['prefix' => 'admin', 'namespace'=>'Admin'], function () {
    Route::match(['post', 'get'], '/eco-brandables/add', 'BrandableCtrl@add')->name('admin:brandable:add');
    Route::match(['post', 'get'], '/eco-brandables/{id}/edit', 'BrandableCtrl@edit_brandable')->name('admin:brandable:edit');
    Route::match(['post', 'get'], '/eco-brandables/{id}/delete', 'BrandableCtrl@delete_brandable')->name('admin:brandable:delete');
    Voyager::routes(); 
});
   
//DESIGNER 
Route::group(['prefix' => 'designer', 'middleware' => ['web']], function () {
    Route::match(['post', 'get'], '/start-design', 'BrandableCtrl@product_list')->name('app:ecommerce:product_list');
    Route::match(['post', 'get'], '/design', 'BrandableCtrl@design')->name('app:ecommerce:design');
    Route::match(['post', 'get'], '/design/upload-image', 'BrandableCtrl@upload_design')->name('app:ecommerce:upload_design');
    Route::match(['post', 'get'], '/design/fetch-arts', 'BrandableCtrl@fetch_arts')->name('app:ecommerce:fetch_arts');
    Route::match(['post', 'get'], '/design/fetch-templates', 'BrandableCtrl@fetch_templates')->name('app:ecommerce:fetch_templates');
    Route::match(['post', 'get'], '/success', 'BrandableCtrl@design_success')->name('app:ecommerce:design_success');
    Route::match(['post', 'get'], '/design/process', 'BrandableCtrl@process_design')->name('app:ecommerce:process_design');

}); 

Route::group(['prefix' => 'influencer', 'middleware' => ['web']], function () {
    Route::match(['post', 'get'], '/onboard', 'InfluencerCtrl@landing_page')->name('app:influencer:landing_page');
    Route::match(['post', 'get'], '/apply', 'InfluencerCtrl@apply')->name('app:influencer:apply');
    Route::match(['post', 'get'], '/apply-success', 'InfluencerCtrl@apply_success')->name('app:influencer:apply_success');
    Route::match(['post', 'get'], '/sign-up', 'InfluencerCtrl@sign_up')->name('app:influencer:sign_up');
});

Route::group(['prefix' => 'designer', 'middleware' => ['web']], function () {
    Route::match(['post', 'get'], '/onboard', 'DesignerCtrl@landing_page')->name('app:designer:landing_page');
    Route::match(['post', 'get'], '/sign-up', 'DesignerCtrl@sign_up')->name('app:designer:sign_up');
    Route::match(['post', 'get'], '/public-profile', 'DesignerCtrl@public_profile')->name('app:designer:public_profile');
    Route::match(['post', 'get'], '/catalogue', 'DesignerCtrl@catalogue')->name('app:designer:catalogue');
});



//USER ROUTES
Route::group(['prefix' => 'user', 'middleware' => ['web', 'app.auth']], function () {
    Route::match(['post', 'get'], '/', 'UserCtrl@index')->name('app:user:index');
    Route::match(['post', 'get'], '/my-information', 'UserCtrl@basic_settings')->name('app:user:basic_settings');
    Route::match(['post', 'get'], '/settings/change-password', 'UserCtrl@password_settings')->name('app:user:password_settings');
    Route::match(['post', 'get'], '/settings/change-email', 'UserCtrl@email_settings')->name('app:user:email_settings');
    //ECOMMERCE INTERLINK
    Route::match(['post', 'get'], '/my-orders', 'UserCtrl@user_orders')->name('app:user:orders');
    Route::match(['post', 'get'], '/my-shipping-address', 'UserCtrl@user_shipping_address')->name('app:user:shipping_address');
    Route::match(['post', 'get'], '/my-saved-items', 'UserCtrl@user_saved_items')->name('app:user:saved_items');
    Route::match(['post', 'get'], '/delete/my-saved-items/{id}', 'UserCtrl@user_delete_saved_items')->name('app:user:delete_saved_items');

    Route::match(['post', 'get'], '/user/set-modal/add-address/{id}', 'UserCtrl@add_address_zone_modal')->name('app:user: add_address_zone_modal');
    Route::match(['post', 'get'], '/user/set-modal/edit-address/{id}/{address_id}', 'UserCtrl@edit_address_zone_modal')->name('app:user: edit_address_zone_modal');
    Route::match(['post', 'get'], '/user/set-modal/select-address/{id}', 'UserCtrl@set_default_zone_modal')->name('app:user:set_default_zone_modal');
    Route::match(['post', 'get'], '/user/set-modal/select-address/{id}', 'UserCtrl@set_default_zone_modal')->name('app:user:set_default_zone_modal');
    Route::match(['post', 'get'], '/user/add-shipping-address/{zone_id}', 'UserCtrl@add_shipping_address')->name('app:user:add_shipping_address');
    Route::match(['post', 'get'], '/user/edit-shipping-address/{zone_id}/{address_id}', 'UserCtrl@edit_shipping_address')->name('app:user:edit_shipping_address');
    Route::match(['post', 'get'], '/user/delete-shipping-address/{id}', 'UserCtrl@delete_shipping_address')->name('app:user:delete_shipping_address');
    Route::match(['post', 'get'], '/user/shipping/select-address/{zone_id}', 'UserCtrl@select_shipping_address')->name('app:user:select_shipping_address');
    Route::match(['post', 'get'], '/user/shipping/select-zone/{zone_id}', 'UserCtrl@select_zone')->name('app:user:select_zone');
    Route::match(['post', 'get'], '/reviews', 'UserCtrl@review_product')->name('app:user:review_product');
    Route::match(['post', 'get'], '/reviews/add/{id}', 'UserCtrl@add_product_review')->name('app:user:add_product_review');

});

Route::group(['prefix' => 'auth', 'middleware' => ['web']], function () {
    Route::match(['post', 'get'], '/login', 'UserCtrl@login')->name('app:user:login');
    Route::match(['post', 'get'], '/logout', 'UserCtrl@logout')->name('app:user:logout');
    Route::match(['post', 'get'], '/sign-up', 'UserCtrl@register')->name('app:user:register');
    Route::match(['post', 'get'], '/verify/email/{token}', 'UserCtrl@verify_email')->name('app:user:verify_email');
    Route::match(['post', 'get'], '/forgot-password', 'UserCtrl@forgot_password')->name('app:user:forgot_password');
    Route::match(['post', 'get'], '/reset-password/{token}', 'UserCtrl@reset_password')->name('app:user:reset_password');
    Route::match(['post', 'get'], '/resend/email-verification', 'UserCtrl@resend_email_verify')->name('app:user:resend_verification');

    //Social Authentication
    Route::match(['post', 'get'], '/social/{provider}', 'UserCtrl@social')->name('app:user:social');
    Route::match(['post', 'get'], '/social/callback/{provider}', 'UserCtrl@social_callback')->name('app:user:social_callback');
    Route::match(['post', 'get'], '/social/step/complete', 'UserCtrl@complete_social')->name('app:user:complete_social');

});

//Seller
Route::group(['prefix' => 'dashboard', 'middleware' => ['web', 'app.auth']], function () {
    Route::match(['post', 'get'], '/', 'DashboardCtrl@index')->name('app:dashboard:index');
    Route::match(['post', 'get'], '/my-orders', 'DashboardCtrl@my_orders')->name('app:dashboard:my_orders');
    Route::match(['post', 'get'], '/my-customers', 'DashboardCtrl@customer_orders')->name('app:dashboard:my_customers');
    Route::match(['post', 'get'], '/profile-settings', 'DashboardCtrl@profile_settings')->name('app:dashboard:profile-settings');
    Route::match(['post', 'get'], '/withdraw', 'DashboardCtrl@withdraw')->name('app:dashboard:withdraw');
    Route::match(['post', 'get'], '/my-products', 'DashboardCtrl@my_products')->name('app:dashboard:my_products');
    Route::match(['post', 'get'], '/payment-history', 'DashboardCtrl@payment_history')->name('app:dashboard:payment_history');
    
    Route::match(['post', 'get'], '/profile-settings/basic', 'DashboardCtrl@profile_settings_basic')->name('app:dashboard:profile_settings_basic');
    Route::match(['post', 'get'], '/profile-settings/password', 'DashboardCtrl@profile_settings_password')->name('app:dashboard:profile_settings_password');
    Route::match(['post', 'get'], '/profile-settings/bank', 'DashboardCtrl@profile_settings_bank')->name('app:dashboard:profile_settings_bank');
    Route::match(['post', 'get'], '/my-products/delete', 'DashboardCtrl@delete_product')->name('app:dashboard:product_delete');
    Route::match(['post', 'get'], '/my-products/search', 'DashboardCtrl@search_product')->name('app:dashboard:product_search');
    Route::match(['post', 'get'], '/product/edit/{id}', 'DashboardCtrl@edit_product')->name('app:dashboard:product_edit');

//Stores
    Route::match(['post', 'get'], '/stores', 'DashboardCtrl@all_store')->name('app:dashboard:all_store');
    Route::match(['post', 'get'], '/create-stores', 'DashboardCtrl@create_store')->name('app:dashboard:create_store');
    Route::match(['post', 'get'], '/edit-store/{id}', 'DashboardCtrl@edit_store')->name('app:dashboard:edit_store');
    Route::match(['post', 'get'], '/edit-store/{id}/store-header', 'DashboardCtrl@store_header_edit')->name('app:dashboard:edit_store:store_header');
    Route::match(['post', 'get'], '/edit-store/{id}/store-banner', 'DashboardCtrl@store_banner_edit')->name('app:dashboard:edit_store:store_banner');
    Route::match(['post', 'get'], '/edit-store/{id}/store-products', 'DashboardCtrl@store_products_edit')->name('app:dashboard:edit_store:store_products');
    Route::match(['post', 'get'], '/edit-store/{id}/store-about', 'DashboardCtrl@store_about')->name('app:dashboard:edit_store:store_about');

//INFLUENCER
Route::match(['post', 'get'], '/gigs', 'DashboardCtrl@influencer_all_gigs')->name('app:dashboard:influencer:all_gigs');
Route::match(['post', 'get'], '/gig/{id}/bid', 'DashboardCtrl@influencer_bid_gig')->name('app:dashboard:influencer:bid_gig');

}); 

Route::group(['prefix' => 'dashboard/api', 'middleware' => ['']], function () {
    Route::match(['post', 'get'], '/profile-settings/upload/{id}', 'DashboardCtrl@profile_settings_save_image')->name('app:dashboard:profile-settings_upload');
    Route::match(['post', 'get'], '/profile-settings/upload-delete/{id}', 'DashboardCtrl@profile_settings_delete_image')->name('app:dashboard:profile-settings_upload_delete');

    Route::match(['post', 'get'], '/edit-store/upload', 'DashboardCtrl@edit_store_save_image')->name('app:dashboard:edit_store_upload');
    Route::match(['post', 'get'], '/edit-store/upload-delete', 'DashboardCtrl@edit_store_delete_image')->name('app:dashboard:edit_store_upload_delete');
});



Route::group(['prefix' => '', 'middleware' => ['web']], function () {
    Route::match(['post', 'get'], '/', 'BaseCtrl@index')->name('app:base:index');
    Route::match(['post', 'get'], '/seller', '\Ukalator\Modules\Dashboard\Controllers\SellerCtrl@seller_home')->name('app:seller:index');
    Route::match(['post', 'get'], '/start-design', 'BrandableCtrl@product_list')->name('app:ecommerce:product_list');
    
    Route::match(['post', 'get'], '/product/search', 'ShopCtrl@product_search')->name('app:ecommerce:product_search');
    Route::match(['post', 'get'], '/product/favorite/{task}', 'ShopCtrl@favorite_product')->name('app:ecommerce:favorite_product');
    Route::match(['post', 'get'], '/product/ask/question', 'ShopCtrl@ask_question')->name('app:ecommerce:ask_question');
    Route::match(['post', 'get'], '/shop/buy-direct/{code?}', 'ShopCtrl@buy_direct')->name('app:ecommerce:buy_direct');
    Route::match(['post', 'get'], '/shop/add-to-cart/{id}', 'ShopCtrl@add_to_cart')->name('app:ecommerce:add_to_cart');
    Route::match(['post', 'get'], '/shop/step/cart-review', 'ShopCtrl@cart_review')->name('app:ecommerce:cart_review');
    Route::match(['post', 'get'], '/shop/step/shipping', 'ShopCtrl@step_shipping')->name('app:ecommerce:step_shipping');
    Route::match(['post', 'get'], '/shop/step/pay', 'ShopCtrl@step_pay')->name('app:ecommerce:step_pay');
    Route::match(['post', 'get'], '/shop/step/pay', 'ShopCtrl@step_pay')->name('app:ecommerce:step_pay');
    Route::match(['post', 'get'], '/shop/step/payment/verify', 'ShopCtrl@payment_verify')->name('app:ecommerce:payment_verify');
    Route::match(['post', 'get'], '/shop/step/payment/notify/{status}', 'ShopCtrl@payment_notify')->name('app:ecommerce:payment_notify_status');

    /**
     * Tasks routes
     */
    Route::match(['post', 'get'], '/shop/action/delete-cart-item/{hash}', 'ShopCtrl@delete_cart_item')->name('app:ecommerce:delete_cart_item');
    Route::match(['post', 'get'], '/shop/action/update-cart-item/{hash}', 'ShopCtrl@update_cart_item')->name('app:ecommerce:update_cart_item');
    Route::match(['post', 'get'], '/shop/set-modal/add-address/{id}', 'ShopCtrl@add_address_zone_modal')->name('app:ecommerce: add_address_zone_modal');
    Route::match(['post', 'get'], '/shop/set-modal/edit-address/{id}/{address_id}', 'ShopCtrl@edit_address_zone_modal')->name('app:ecommerce: edit_address_zone_modal');
    Route::match(['post', 'get'], '/shop/set-modal/select-address/{id}', 'ShopCtrl@set_default_zone_modal')->name('app:ecommerce:set_default_zone_modal');
    Route::match(['post', 'get'], '/shop/add-shipping-address/{zone_id}', 'ShopCtrl@add_shipping_address')->name('app:ecommerce:add_shipping_address');
    Route::match(['post', 'get'], '/shop/edit-shipping-address/{zone_id}/{address_id}', 'ShopCtrl@edit_shipping_address')->name('app:ecommerce:edit_shipping_address');
    Route::match(['post', 'get'], '/shop/delete-shipping-address/{id}', 'ShopCtrl@delete_shipping_address')->name('app:ecommerce:delete_shipping_address');
    Route::match(['post', 'get'], '/shop/shipping/select-address/{zone_id}', 'ShopCtrl@select_shipping_address')->name('app:ecommerce:select_shipping_address');
    Route::match(['post', 'get'], '/shop/shipping/select-zone/{zone_id}', 'ShopCtrl@select_zone')->name('app:ecommerce:select_zone');
    Route::match(['post', 'get'], '/shop/shipping/process-price/verify', 'ShopCtrl@process_shipping_price_for_payment')->name('app:ecommerce:process_shipping_price_for_payment');
    Route::match(['post', 'get'], '/shop/shipping/process-payment/{type}', 'ShopCtrl@process_payment')->name('app:ecommerce:process_payment');

    //dynamic routes
    Route::match(['post', 'get'], '/{category}', 'ShopCtrl@product_catlogue')->name('app:ecommerce:product_catlogue');
    Route::match(['post', 'get'], '/{category}/{param}', 'ShopCtrl@single_product')->name('app:ecommerce:single_product');
    Route::match(['post', 'get'], '/store/{slug}', 'DashboardCtrl@store_view')->name('app:base:store_view');

});
