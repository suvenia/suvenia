<a class="btn btn-warning" id="bulk_approve_btn"><i class="voyager-check"></i> <span>{{ __('Approve Influencers') }}</span></a>

{{-- Bulk delete modal --}}
<div class="modal modal-warning fade" tabindex="-1" id="bulk_approve_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <i class="voyager-check"></i> {{ __('Are you sure you want to approve') }} <span id="bulk_approve_count"></span> <span id="bulk_approve_display_name"></span>?
                </h4>
            </div>
            <div class="modal-body" id="bulk_approve_modal_body">
            </div>
            <div class="modal-footer">
                <form action="{{ route('voyager.'.$dataType->slug.'.index') }}/0" id="bulk_approve_form" method="POST">
                    {{ method_field("POST") }}
                    {{ csrf_field() }}
                    <input type="hidden" name="ids" id="bulk_approve_input" value="">
                    <input type="submit" class="btn btn-warning pull-right b-confirm"
                             value="{{ __('Are you sure you want to approve') }} {{ strtolower($dataType->display_name_plural) }}">
                </form>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    {{ __('voyager::generic.cancel') }}
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
window.onload = function () {
    // Bulk delete selectors
    var $bulkApproveBtn = $('#bulk_approve_btn');
    var $bulkApproveModal = $('#bulk_approve_modal');
    var $bulkApproveCount = $('#bulk_approve_count');
    var $bulkApproveDisplayName = $('#bulk_approve_display_name');
    var $bulkApproveInput = $('#bulk_approve_input');
    // Reposition modal to prevent z-index issues
    $bulkApproveModal.appendTo('body');
    // Bulk delete listener
    $bulkApproveBtn.click(function () {
        var ids = [];
        var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');
        var count = $checkedBoxes.length;
        if (count) {
            // Reset input value
            $bulkApproveInput.val('');
            // Deletion info
            var displayName = count > 1 ? '{{ $dataType->display_name_plural }}' : '{{ $dataType->display_name_singular }}';
            displayName = displayName.toLowerCase();
            $bulkApproveCount.html(count);
            $bulkApproveDisplayName.html(displayName);
            // Gather IDs
            $.each($checkedBoxes, function () {
                var value = $(this).val();
                ids.push(value);
            })
            // Set input value
            $bulkApproveInput.val(ids);
            // Show modal
            $bulkApproveModal.modal('show');
        } else {
            // No row selected
            toastr.warning('{{ __('You have not selected any user to approve') }}');
        }
    });
}
</script>
