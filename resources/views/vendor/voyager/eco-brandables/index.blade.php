@extends('voyager::master')

@section('page_title', __('All Brandables'))

@section('content')

<div class="container-fluid">
        <h1 class="page-title">
                <i class="voyager-paint-bucket"></i> Brandables
            </h1>
            @can('add', app('App\Brandable'))
            <a href="{{ route('admin:brandable:add') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
             @endcan

<div class="page-content browse container-fluid">

<div class="row">
<div class="col-md-12">

<div class="panel panel-bordered">
<div class="panel-body">
<div class="row">
        @forelse($brandables as $brandable)
        <div class="col-md-3 text-center">
        <p style="margin:0;"><img src="{{ $brandable->front_image}}" width="150"></p>
        <p style="margin:2px; text-transform:capitalize;"><b>{{ $brandable->name }}</b></p>
        <p style="margin:2px; text-transform:capitalize;">
            <a href="{{ route('admin:brandable:edit', ['id'=> $brandable->id]) }}" class="btn btn-info btn-sm">Edit this</a>
            <a href="{{ route('admin:brandable:delete', ['id'=> $brandable->id]) }}" class="btn btn-danger btn-sm">Delete this</a>
        </p>
        
        </div>
        @empty
        <div class="col-md-12 text-center">
            <p><i class="voyager-bag" style="font-size:100px;"></i></p>
            <b>You do not have any product</b>
            </div>
        @endforelse

</div> 
</div> 
</div>

 
</div>
</div>


</div>
</div>

@endsection