@extends('voyager::master')

@section('page_title', __('Add Brandable'))

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
<link rel="stylesheet" href="{{ mix('css/brandable.css') }}">
@endsection

@section('content')

<div class="container-fluid">
    <h1 class="page-title">
        <i class="voyager-paint-bucket"></i> Add A Brandable Design
    </h1>

    <div class="page-content browse container-fluid">

            <div class="row">
            <div class="col-md-12">
            
            <div class="panel panel-bordered">
            <div class="panel-body">
            <div class="row">
                    <form action="{{ route('admin:brandable:add') }}" method="POST" data-post>
                        @csrf
                            <div class="box box-default">
                                <div class="box-header with-border mb-3">
                                    <span class="pull-right"><button class="btn btn-info" type="submit">Save Changes</button></span>
                                </div>
                                <div class="box-body">

                                    {{ csrf_field() }}

                                    <textarea id="FrontDesignAreaDim" name="front_dimension" style="display:none;"></textarea>
                                    <textarea id="FrontProduct" name="front_product" style="display:none;"></textarea>
                                    <textarea id="BackDesignAreaDim" name="back_dimension" style="display:none;"></textarea>
                                    <textarea id="BackProduct" name="back_product" style="display:none;"></textarea>

                                    <div class="form-group name">
                                        <label for="name" class="control-label">Name</label>
                                        <input type="text" class="form-control" name="name" placeholder="Name">
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div class="form-group price">
                                        <label for="name" class="control-label">Price</label>
                                        <input type="text" class="form-control" name="price" placeholder="Price">
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div class="form-group description">
                                        <label for="description" class="control-label">Category</label>
                                        <select class="form-control" name="category">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id}}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div class="form-group description">
                                        <label for="description" class="control-label">Description</label>
                                        <textarea class="form-control" name="description"></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div class="funkyradio">

                                        <div class="funkyradio-default">
                                            <input type="checkbox" name="enable_sizes" id="radio1" checked value="1">
                                            <label for="radio1">Enable Sizes</label>
                                        </div>
                                        <p></p><small class="text-muted">Disabling this is useful for products like "MUGS" which do not need sizes.</small>

                                    </div>

                                    <div class="funkyradio">
                                        <div class="funkyradio-default">
                                            <input type="checkbox" name="enable_back_view" id="BackViewTrigger" value="1">
                                            <label for="BackViewTrigger">Enable Back View</label>
                                        </div>

                                    </div>

                                </div>

                            </div>
                            <!-- /.box -->
                        </form>

            </div>

            <div class="row mt-3">
                    <h5 class="text-center text-bold">Front View Product Settings</h5>
                    <div class="col-md-4 mb-3">
                        <div class="well well-sm text-center">
                            <small>Upload Front-View Product</small>
                            <p style="margin:1px;"><span class="btn btn-info btn-file">
        Browse Products <input type="file" class="file-upload"  accept=".png, .jpg, .jpeg" data-target="#frontImagePreview" data-input="#FrontProduct">
        </span></p>
                        </div>
                    </div>

                    <div class="col-md-8" style="overflow:auto;">

                        <div class="DesignAreaContainer" style="width:530px; height:630px; border:0px solid red; overflow:auto;">
                            <div class="designArea"></div>
                            <div class="CanvaArea">
                                <img src="{{ asset('brands/default_img.png') }}" class="image-fixed" id="frontImagePreview">
                            </div>
                        </div>

                    </div>

                </div>


                <div class="row uk-margin-top" id="BackView" style="position:relative;">
                        <span class="overlay"></span>
                        <h5 class="text-center text-bold">Back ViewProduct Settings</h5>
                        <div class="col-md-4">
                            <div class="well well-sm text-center">
                                <small>Upload Back-View Product</small>
                                <p style="margin:1px;"><span class="btn btn-info btn-file">
            Browse Products <input type="file" class="file-upload"  accept=".png, .jpg, .jpeg" data-target="#backImagePreview" data-input="#BackProduct">
            </span></p>
                            </div>
                        </div>

                        <div class="col-md-8" style="overflow:auto;">

                            <div class="DesignAreaContainerBack" style="width:530px; height:630px; border:0px solid red; overflow:auto;">
                                <div class="designAreaBack"></div>
                                <div class="CanvaArea">
                                    <img src="{{ asset('brands/default_img.png') }}" class="image-fixed" id="backImagePreview">
                                </div>
                            </div>

                        </div>

                    </div>

            </div>
            </div>
            </div>
            </div>
    </div>

</div>



@endsection


@section('javascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="{{ mix('js/brandable.js') }}"></script>
@endsection