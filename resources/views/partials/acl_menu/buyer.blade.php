
@if(isset($type) && $type == 'admin_sidebar')

<li class="nav-item super-header">
        <a href="{{ route('app:dashboard:index') }}" class="nav-link active">
            Buyer <i class="icon icon-minus" style="font-size: 20px; float:right;"></i>
        </a>
</li>

<li class="nav-item">
    <a href="{{ route('app:dashboard:index') }}" class="nav-link active">
        <i class="icon icon-speedometer" style="font-size: 20px;"></i> My Dashboard
    </a>
</li>

<li class="nav-item">
        <a href="{{route('app:dashboard:my_products') }}" class="nav-link active">
            <i class="icon icon-handbag" style="font-size: 20px;"></i> Products
        </a>
    </li>
<li class="nav-item">
    <a href="{{route('app:dashboard:my_orders') }}" class="nav-link active">
        <i class="icon icon-basket"></i> My Orders
    </a>
</li>
<li class="nav-item">
    <a href="{{route('app:dashboard:all_store') }}" class="nav-link active">
        <i class="icon icon-home" style="font-size: 20px;"></i> Stores
    </a>
</li>
<li class="nav-item">
    <a href="{{route('app:dashboard:my_customers') }}" class="nav-link active">
        <i class="icon icon-basket" style="font-size: 20px;"></i> Customer Orders
    </a>
</li>
<li class="nav-item">
    <a href="{{route('app:dashboard:profile-settings') }}" class="nav-link active">
        <i class="icon icon-user" style="font-size: 20px;"></i> Profile Settings
    </a>
</li>
<li class="nav-item">
    <a href="{{route('app:dashboard:payment_history') }}" class="nav-link active">
        <i class="icon icon-credit-card" style="font-size: 20px;"></i> Payment History
    </a>
</li>
<li class="nav-item">
    <a href="{{route('app:dashboard:withdraw') }}" class="nav-link active">
        <i class="icon icon-wallet" style="font-size: 20px;"></i> Withdrawal
    </a>
</li>

@endif