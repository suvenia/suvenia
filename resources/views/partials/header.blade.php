@php $categories = $utils->get_categories(); @endphp

@if(!$utils->device()->isMobile() || $utils->device()->isTablet())

<div class="uk-navbar-container top-nav">
    <div class="uk-container uk-container-small">
        <div class="row nav-ext">
            <div class="col-md-3">
            <a href="{{ route('app:base:index') }}" class="{{ $utils->makeActive(['app:base:index'], 'uk-active') }}"><span class="icon-suvenia" style="font-size:18px;"></span> Marketplace</a>
            </div>
            <div class="col-md-3">
                <a href="{{ route('app:seller:index') }}" class="{{ $utils->makeActive(['app:seller:index'], 'uk-active') }}"><span class="icon-seller" style="font-size:18px;"></span>seller</a>
            </div>
            <div class="col-md-3">
                <a href="{{ route('app:designer:landing_page') }}" class="{{ $utils->makeActive(['app:designer:landing_page', 'app:designer:sign_up'], 'uk-active') }}"><span class="icon-bulb" style="font-size:18px;"></span> designer</a>
            </div>
            <div class="col-md-3">
                <a href="{{ route('app:influencer:landing_page') }}" class="{{ $utils->makeActive(['app:influencer:landing_page', 'app:influencer:apply'], 'uk-active') }}"><span class="icon-influencer" style="font-size:18px;"></span> influencer</a>
            </div>

        </div>
    </div>
</div> 
 
<div class="uk-navbar-container middle-nav">
    <div class="uk-container">
        <div class="uk-navbar">
            <div class="uk-navbar-left">
                <a href="{{ route('app:base:index') }}" class="uk-navbar-item uk-logo"><img src="{{ $utils->get_image('site.logo') }}" uk-responsive width="120"></a> 
            </div> 

            <div class="uk-navbar-right">

                <div class="uk-navbar-item boundary-align">
                    <form action="{{ route('app:ecommerce:product_search') }}" method="post" autocomplete="off">
                        @csrf
                        <div class="input-group  app-search-form">
                            <input type="search" class="form-control" placeholder="What are you looking for today?" name="q" id="appWideSearchInp" data-url="{{ route('app:ecommerce:product_search') }}">
                            <div uk-dropdown="pos: bottom-justify; boundary: .boundary-align; boundary-align: true; mode: click" id="appWideSearchContainer">
                            </div>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-info" id="app-search-btn">Search</button>
                            </div>
                        </div>
                    </form>

                </div>

                <div class="uk-navbar-item">
                    <a href="{{ route('app:ecommerce:product_list') }}" class="text-link">Sell on suvenia</a>
                </div>

                <div class="uk-navbar-item">
                    <a href="{{ route('app:ecommerce:cart_review') }}" class="text-link-cart">
                        <div class="d-inline"><span class="icon-cart"></span>
                            <div class="cart-counter" id="__data_cart_counter">{{ LaraCart::count() }}</div>
                        </div> Cart
                    </a>
                </div>
 
                <div class="uk-navbar-item">
                    @guest
                    <a href="{{ route('app:user:login') }}" class="text-link-auth">
                        <span class="m-0">Login</span>
                        <div class="uk-navbar-subtitle m-0">Sign up</div>
                    </a>

                    @else
                    
                    <a href="javascript:;" class="text-link-auth text-link-auth-logged">
                        <span class="m-0">{{ Auth::user()->name  }}</span>
                        <span uk-icon="chevron-down"></span>
                    </a>
                    <div uk-dropdown>
                        <ul class="uk-nav uk-dropdown-nav app-user-drop-down">
                            <li class=""><a href="{{ route('app:user:basic_settings') }}">My Information</a></li>
                            <li class=""><a href="{{ route('app:user:orders') }}">My Orders</a></li>
                            <li class=""><a href="{{ route('app:user:shipping_address') }}">Shipping Address</a></li>
                            <li class=""><a href="{{ route('app:user:saved_items') }}">Saved Items</a></li>
                            <li class=""><a href="{{ route('app:user:logout') }}" class="post-link">Logout</a></li>

                        </ul>
                    </div>

                    @endguest
                </div>

            </div>

        </div>
    </div>
</div>

<div class="uk-navbar-container bottom-nav">
    <div class="uk-container">
        <div class="uk-navbar">
            <div class="uk-navbar-center">
                <ul class="uk-navbar-nav">
                    @foreach ($categories as $category)
                    <li class=""><a href="{{ route('app:ecommerce:product_catlogue', ['param'=> $category->slug ]) }}">{{ $category->name }} 
                                            @if(count($category->children) > 0)
                                            &nbsp;<span uk-icon="chevron-down"></span>
                                            @endif
                                        </a> @if(count($category->children) > 0)
                        <div uk-dropdown="offset: 0">
                            <ul class="uk-nav uk-navbar-dropdown-nav">
                                @foreach ($category->children as $child)
                                <li><a href="{{ route('app:ecommerce:product_catlogue', ['param'=> $child->slug ]) }}" class="uk-text-capitalize">{{ $child->name }}
                                                        @if(count($child->children) > 0) 
                                                        <span uk-icon="chevron-right" style="float:right;"></span>
                                                        @endif
                                                    </a> @if(count($child->children) > 0)
                                    <div uk-dropdown="pos: right-top; offset: 0;">
                                        <ul class="uk-nav uk-navbar-dropdown-nav">
                                            @foreach($child->children as $child_item)

                                            <li><a href="{{ route('app:ecommerce:product_catlogue', ['param'=> $child_item->slug ]) }}" class="uk-text-capitalize">#{{ $child_item->name }}
                                                                    </a>
                                            </li>

                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif

                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </li>
                    @endforeach

                </ul>
            </div>
        </div>
    </div>
</div>

@else


<div class="uk-navbar-container mobile-nav" uk-sticky="top: 100; animation: uk-animation-slide-top; cls-active: sticky-nav-web">
        <div class="uk-container ">
            <nav class="uk-navbar">
    
                <div class="uk-navbar-left">
                    <a href="javascript:;" class="uk-navbar-item mobile-nav-text-color" uk-toggle="target: #NavOffcanvas" style="focus: text-decoration:none;"><span class="icon-menu" style="font-size:18px; font-weight:bold;"></span></a>
                </div>
    
                <div class="uk-navbar-left ml-1">
                    <a href="{{ route('app:base:index') }}" class="uk-navbar-item uk-logo">
                        <img src="{{ $utils->get_image('site.logo') }}" width="100" uk-responsive>
                    </a>
                </div>
    
                <div class="uk-navbar-right">
                    <ul class="uk-navbar-nav">
                        <li class="">
                                <a href="{{ route('app:ecommerce:cart_review') }}" class="text-link-cart text-link-cart-mobile">
                                        <div class="d-inline"><span class="icon-cart"></span>
                                            <div class="cart-counter-mobile" id="__data_cart_counter">{{ LaraCart::count() }}</div>
                                        </div>
                                </a>
                        </li>
    
                    </ul>
                </div>

            </nav>
        </div>
        <div class="uk-container ">
            <nav class="uk-navbar">

                <div class="uk-navbar-left">
                        <div class="uk-navbar-item uk-width-expand" style="min-height: 50px;">
                                <form class="uk-search uk-search-navbar app-search-mobile uk-width-1-1">
                                        <span uk-search-icon></span>
                                    <input class="uk-search-input" type="search" placeholder="Search..." autofocus>
                                </form>
                            </div>
                </div>
    
            </nav>
        </div>

</div>
    
    <div id="NavOffcanvas" uk-offcanvas="overlay: true">
        <div class="uk-offcanvas-bar">
            <button class="uk-offcanvas-close" type="button" uk-close></button>
    
            <div class="mt-4 border-top pt-2 uk-text-left mobile-side-nav">
                @if(Auth::guest())
                <p class="mob-item"><span uk-icon="icon: user"></span> <a href="" class="">&nbsp; Sign up / Signin</a></p>
                @else
                <p class="mob-item"><span uk-icon="icon: user"></span> <a href="" class="">&nbsp; {{ str_limit(Auth::user()->email, '20','..') }}</a></p>
                @endif
            </div>
    
            <div class="mt-2">
                <ul class="list-unstyled mobile-side-nav">
                   
                </ul>
            </div>
    
            <div class="mt-4 border-top pt-2 uk-text-left">
                @if(!Auth::guest())
                <p><a href="{{ route('app:user:logout') }}" class="post-link">&nbsp; Logout <span uk-icon="icon: sign-out" style="float: right;"></span></a></p>
                @endif
            </div>
    
        </div>
</div>
    
@endif