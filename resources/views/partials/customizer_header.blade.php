<div class="uk-navbar-container middle-nav">
        <div class="uk-container">
        <div class="uk-navbar">
                <div class="uk-navbar-left">
                        <a href="{{ route('app:base:index') }}" class="uk-navbar-item uk-logo"><img src="{{ $utils->get_image('site.logo') }}" uk-responsive width="120"></a>
                </div>


                <div class="uk-navbar-right">

                        
                        <div class="uk-navbar-item">
                            <a href="{{ route('app:ecommerce:cart_review') }}" class="text-link-cart">
                                    <div class="d-inline"><span class="icon-cart"></span> <div class="cart-counter" id="__data_cart_counter">{{ LaraCart::count() }}</div></div> Cart
                            </a>
                        </div>

                        <div class="uk-navbar-item">
                                @guest
                                <a href="{{ route('app:user:login') }}" class="text-link-auth">
                                        <span class="m-0">Login</span>
                                        <div class="uk-navbar-subtitle m-0">Sign up</div>
                                </a> 

                                @else
                                <a href="javascript:;" class="text-link-auth text-link-auth-logged">
                                <span class="m-0">{{ Auth::user()->username  }}</span>
                                <span uk-icon="chevron-down"></span>
                                </a>
                                <div uk-dropdown> 
                                    <ul class="uk-nav uk-dropdown-nav app-user-drop-down">
                                    <li class=""><a href="{{ route('app:user:basic_settings') }}">My Information</a></li>
                                            <li class=""><a href="{{ route('app:user:orders') }}">My Orders</a></li>
                                            <li class=""><a href="{{ route('app:user:shipping_address') }}">Shipping Address</a></li>
                                            <li class=""><a href="{{ route('app:user:saved_items') }}">Saved Items</a></li>
                                    <li class=""><a href="{{ route('app:user:logout') }}" class="post-link">Logout</a></li>
                                    
                                    </ul>
                                </div>

                                @endguest
                            </div>

                              
                </div>

        </div>
        </div>
</div>