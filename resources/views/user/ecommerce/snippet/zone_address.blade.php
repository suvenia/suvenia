<form class="app-form data-post" action="{{ route('app:ecommerce:add_shipping_address', ['id'=> $id]) }}" method="POST" data-post>
    @csrf 
    <div class="form-group firstname">
            <label for="" class="ship_modal_label">First Name</label>
        <input type="text" class="form-control" placeholder="Firstname" name="firstname"  value="">
        <p class="form-text help-block"></p>
    </div>
    <div class="form-group lastname">
            <label for="" class="ship_modal_label">Last Name</label>
        <input type="text" class="form-control" placeholder="Lastname" name="lastname"  value="">
        <p class="form-text help-block"></p>
    </div>
    <div class="form-group email">
            <label for="" class="ship_modal_label">Email</label>
        <input type="text" class="form-control" placeholder="Email" name="email"  value="">
        <p class="form-text help-block"></p>
    </div>
    
    @if($states->count() > 0)
    <div class="form-group">
            <label for="" class="ship_modal_label">What state do you want to ship to?</label>
        <label for="">Select State</label>
        <select class="form-control ship_inp_grey" name="location">
            @foreach($states as $state)
        <option value="{{ $state->id }}">{{ $state->name }}</option>
            @endforeach
        </select>
    </div>
    @else

    <div class="form-group">
            <label for="" class="ship_modal_label">What country do you want to ship to?</label>
            <select class="form-control ship_inp_grey" name="country" id="__country_select">
                @foreach(config('ecommerce_config.countries') as $key => $value)
                @php $chooseNigeria = $key =="NG" ? 'selected' : ''; @endphp
            <option value="{{ strtolower($key) }}" {{ $chooseNigeria }}>{{ $value }}</option>
                @endforeach
            </select>
     </div>

    @endif

    <div class="form-group address">
            <label for="" class="ship_modal_label">Shipping Address</label>
        <textarea class="form-control" name="address" placeholder="Your Address"></textarea>
        <p class="form-text help-block"></p>
    </div>

    <div class="form-group phone_number">
            <label for="" class="ship_modal_label d-block">Phone Number</label>
            <input type="tel" class="form-control" placeholder="phone Number" name="phone_number"  value="" id="___phone" style="width:100%;">
            <p class="form-text help-block"></p>
    </div>
    <button type="submit" class="btn btn-info float-right">SAVE</button>

</form>

<script>
___setNumber('#___phone', '#__country_select');
</script>