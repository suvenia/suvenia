@extends('layouts.layout')

@section('title', 'Chnage Password' )

@section('content')
<div class="row justify-content-center">

    <div class="col-md-6">
           
                <form class="" action="{{ route('app:user:password_settings') }}" method="POST">
                 @csrf 
                    <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                      <label for="password">Password</label>
                      <input type="password" class="form-control" placeholder="Password" name="password"  value="{{ old('password') }}">
                      <p class="form-text help-block">{{ $errors->first('password') }}</p>
                    </div>

                    <div class="form-group {{ $errors->has('confirm_password') ? 'has-error' : ''}}">
                      <label for="confirm password">Confirm Password</label>
                      <input type="password" class="form-control" placeholder="Confirm Password" name="confirm_password"  value="{{ old('confirm_password') }}">
                      <p class="form-text help-block">{{ $errors->first('confirm_password') }}</p>
                    </div>
                   
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
    </div>

</div>



@endsection