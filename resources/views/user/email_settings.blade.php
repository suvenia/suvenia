@extends('layouts.layout')

@section('title', 'Change Email' )

@section('content')
<div class="row justify-content-center">

    <div class="col-md-6">
           
                <form class="" action="{{ route('app:user:email_settings') }}" method="POST">
                 @csrf 
                <p>Your email address is currently <b>{{ $user->email }}</b></p>

                    <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                      <label for="email">New Email address</label>
                      <input type="email" class="form-control" placeholder="Email Address" name="email"  value="{{ old('email') }}">
                    <p class="form-text help-block">{{ $errors->first('email') }}</p>
                    </div>
                   
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
              
     </div>

</div>
@endsection