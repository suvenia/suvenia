@extends('layouts.layout')

@section('title', 'Login' )

@section('content')

<div class="uk-container mt-2 mb-3">
  <div class="row justify-content-center">
    <div class="col-8">
        @includeIf('partials.user_nav')
    </div>
  </div>

  <div class="row justify-content-center">
    <div class="col-8">
      <div class="app-card bg-white p-4">
        <div class="row">
          <form class="col-6 profile-form" action="{{ route('app:user:basic_settings') }}" method="POST">
              @csrf 

                 <div class="form-group {{ $errors->has('username') ? 'has-error' : ''}}">
                   <label for="username">Username*</label>
                   <input type="text" class="form-control" placeholder="Username" name="username"  value="{{ !old('username') ? $user->username : old('username') }}">
                   <p class="form-text help-block">{{ $errors->first('username') }}</p>
                 </div>
                
                 <div class="form-group {{ $errors->has('firstname') ? 'has-error' : ''}}">
                   <label for="firstname">Firstname</label>
                   <input type="text" class="form-control" placeholder="Firstname" name="firstname"  value="{{ !old('firstname') ? $user->firstname : old('firstname') }}">
                   <p class="form-text help-block">{{ $errors->first('firstname') }}</p>
                 </div>
                
                 <div class="form-group">
                 <span>Email &nbsp;&nbsp;&nbsp; </span> <label style="text-transform:lowercase;color: #000;">{{ $user->email }}</label>
                 </div>

                 <div class="form-group {{ $errors->has('lastname') ? 'has-error' : ''}}">
                   <label for="lastname">Lastname</label>
                   <input type="text" class="form-control" placeholder="Lastname" name="lastname"  value="{{ !old('lastname') ? $user->lastname : old('lastname') }}">
                   <p class="form-text help-block">{{ $errors->first('lastname') }}</p>
                 </div>

                 <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
                   <label for="phone">Phone</label>
                   <input type="number" class="form-control" placeholder="Phone Number" name="phone"  value="{{ !old('phone') ? $user->phone : old('phone') }}">
                   <p class="form-text help-block">{{ $errors->first('phone') }}</p>
                 </div>
                
                 
                
                 <button type="submit" class="btn btn-info-dark uk-text-uppercase">Save Changes</button>
          </form>
        </div>
      </div>
    </div>
  </div>


</div>

@endsection