@extends('layouts.layout')

@section('title', 'Login' )

@section('content')

<div class="row justify-content-center">

    <div class="col-md-6">
           
                <form class="" action="{{ route('app:user:complete_social') }}" method="POST">
                 @csrf 

                    <div class="form-group">
                      <label for="username">Upload Photo</label>
                      <input type="file" name="profile_photo">
                    </div>
                   
                    <div class="form-group {{ $errors->has('username') ? 'has-error' : ''}}">
                      <label for="username">Username</label>
                      <input type="text" class="form-control" placeholder="Username" name="username"  value="{{ !old('username') ? $user->username : old('username') }}">
                      <p class="form-text help-block">{{ $errors->first('username') }}</p>
                    </div>
                   
                    <div class="form-group {{ $errors->has('firstname') ? 'has-error' : ''}}">
                      <label for="firstname">Firstname</label>
                      <input type="text" class="form-control" placeholder="Firstname" name="firstname"  value="{{ !old('firstname') ? $user->firstname : old('firstname') }}">
                      <p class="form-text help-block">{{ $errors->first('firstname') }}</p>
                    </div>
                   
                    <div class="form-group {{ $errors->has('lastname') ? 'has-error' : ''}}">
                      <label for="lastname">Lastname</label>
                      <input type="text" class="form-control" placeholder="Lastname" name="lastname"  value="{{ !old('lastname') ? $user->lastname : old('lastname') }}">
                      <p class="form-text help-block">{{ $errors->first('lastname') }}</p>
                    </div>

                    <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" placeholder="Password" name="password"  value="{{ old('password') }}">
                            <p class="form-text help-block">{{ $errors->first('password') }}</p>
                          </div>
      
                    <div class="form-group {{ $errors->has('confirm_password') ? 'has-error' : ''}}">
                    <label for="confirm password">Confirm Password</label>
                    <input type="password" class="form-control" placeholder="Confirm Password" name="confirm_password"  value="{{ old('confirm_password') }}">
                    <p class="form-text help-block">{{ $errors->first('confirm_password') }}</p>
                    </div>
                    
                   
                    <button type="submit" class="btn btn-primary">Complete Registration</button>
                  </form>
    </div>

</div>
@endsection