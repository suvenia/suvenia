<ul class=" uk-child-width-1-2@s uk-child-width-1-3@m uk-margin-top" uk-grid>
@foreach($arts as $art)
@if($art->name !== 'parent')
<li class="uk-margin-bottom">
<div class="uk-card uk-card-small uk-text-center art_single">
<p style="margin:0;"><img src="{{ asset('Arts/parent/' . str_slug($art->name, '-') . '.svg') }}" uk-responsive width="100" class="ArtPicParentDyn" data-url="{{ route('app:ecommerce:fetch_arts', ['pid'=> $art->ref]) }}"></p>
<p class="art_name text-14" style="display:non; margin:0px;">{{ title_case($art->name) }}</p>
</div>
</li>
@endif
@endforeach
</ul>