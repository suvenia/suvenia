@extends('layouts.customizer')

@section('title', 'Designer' )

@section('content')

<page data-route="home" class="page-hide">
        <div class="uk-container mt-3 mb-3">
                <div class="row bg-toolbar mt-2 mb-2 ToolBar hide">
                        <div class="col-md-4">
                            <ul class="list-unstyled mt-3">
                                <li class="d-inline toolbar-label">Font: </li>
                                <li class="d-inline">
                                        <select class="selector d-inline" id="FontSelect" dir="ltr">
                                                <optgroup>
                                                        <option value="'Monoton', cursive" class="monoton" selected>Monoton</option>
                                                        <option value="'Kranky', cursive" class="kranky">Kranky</option>
                                                        <option value="The Girl Next Door" class="girl_next">Girl</option>
                                                        <option value="'Fredericka the Great', cursive" class="fredrick">Fredrick</option>
                                                        <option value="'Press Start 2P', cursive" class="press_start">Press Start</option>
                                                        <option value="'Cabin Sketch', cursive" class="cabin_sketch">Cabin Sketch</option>
                                                        <option value="'Kurale', serif" class="kurale">Kurale</option>
                                                        <option value="'Sigmar One', cursive" class="sigmar">Sigmar</option>
                                                        <option value="'Paytone One', sans-serif" class="paytone">Paytone</option>
                                                        <option value="'Luckiest Guy', cursive" class="luckiest">Luckiest</option>
                                                        <option value="'Audiowide', cursive" class="audiowide">Audio Wide</option>
                                                        <option value="'Great Vibes', cursive" class="vibes">Vibes</option>
                                                        <option value="'Philosopher', sans-serif" class="philosopher">Philosopher</option>
                                                        <option value="'Plaster', cursive" class="plaster">Plaster</option>
                                                        <option value="'Exo', sans-serif" class="exo">Exo</option>
                                                        <option value="'Dancing Script', cursive" class="dancing">Dancing</option>
                                                        <option value="'Pacifico', cursive" class="pacifico">Pacifico</option>
                                                        <option value="'Lobster', cursive" class="lobster">Lobster</option>
                                                </optgroup>
                                        </select>
                                </li>
                                <li class="d-inline toolbar-label ml-3">Outline: </li>
                                <li  class="d-inline">
                                        <select class="selector" id="TextOutline">
                                                <option value="0" selected>No Outline</option>
                                                <option value="1">Thin Outline</option>
                                                <option value="2">Medium Outline</option>
                                                <option value="3">Thick Outline</option>
                                        </select>
                                </li>

                            </ul>
                        </div>
                        <div class="col-md-4">
                                <ul class="list-unstyled mt-3">
                                        <li class="d-inline-block uk-text-center make-pointer">
                                                <span class="icon-undo"></span> 
                                                <div class="d-inline-bloc toolbar-label">Undo</div>
                                        </li>
                                        <li class="d-inline-block uk-text-center ml-1 make-pointer">
                                                <span class="icon-redo"></span> 
                                                <div class="d-inline-bloc toolbar-label">Redo</div>
                                        </li>
                                        <li class="d-inline-block uk-text-center ml-3 make-pointer" id="DuplicateApp">
                                                <span class="icon-duplicate"></span> 
                                                <div class="d-inline-bloc toolbar-label">Duplicate</div>
                                        </li>
                                        <li class="d-inline-block uk-text-center ml-1 make-pointer" id="FlipVertical">
                                                <span class="icon-flip-vertical"></span> 
                                                <div class="d-inline-bloc toolbar-label">Flip Vertical</div>
                                        </li>
                                        <li class="d-inline-block uk-text-center ml-1 make-pointer" id="FlipHorizontal">
                                                <span class="icon-flip-horizontal"></span> 
                                                <div class="d-inline-bloc toolbar-label">Flip Horizontal</div>
                                        </li>

                                </ul>
                        </div>
                        <div class="col-md-4">
                                <ul class="list-unstyled mt-4">
                                        <li class="d-inline toolbar-label">Font: </li>
                                        <li class="d-inline">
                                            <input type='text' id="AppWideColor" class="toggleColorPallete"/>
                                        </li>
                                        <li class="d-inline toolbar-label ml-3">Outline Color: </li>
                                        <li class="d-inline">
                                            <input type='text' id="AppWideOutlineColor" class="toggleColorPallete"/>
                                        </li>
                                </ul>
                        </div>
                </div>
                <div class="row"> 
                        <div class="col-3">
                                <div class="list-group features-list">
                                <a href="javascript:;" class="list-group-item list-group-item-action features-list-item" id="AddText">
                                    <span class="icon-add-text"></span> <span class="text-t">add text</span>
                                </a>
                                <a href="#ImageModal" class="list-group-item list-group-item-action features-list-item" uk-toggle>
                                    <span class="icon-image"></span> <span class="text-t">add image</span>
                                </a>
                                <a href="#GraphicModal" class="list-group-item list-group-item-action features-list-item" uk-toggle>
                                    <span class="icon-graphic"></span> <span class="text-t">add graphic</span>
                                </a>
                                <a href="#TemplateModal" class="list-group-item list-group-item-action features-list-item" uk-toggle>
                                    <span class="icon-template"></span> <span class="text-t">add template</span>
                                </a> 
                                </div> 

                                <div class="card custom-layer-card mt-3">
                                <div class="card-header"> 
                                <span class="icon-layers"></span> layers
                                </div>
                                <div class="card-body" id="LayerPane">
                                </div>
                                </div>

                        </div>
                        <div class="col-md-6" style="overflow:auto; border:0px solid red;" >
                                <div class="flip-container">
                                        <div class="flipper">
                                        <div style="width: 530px; height: 630px;" id="side_front" class="flip_front">
                                                <img class="ProductFacing" src="">
                                                <div class="drawingArea" style="">
                                                <canvas id="canvas_front" width="" height="" class="hove"></canvas>
                                                </div>
                                        </div>
                                        <div id="side_back" style="width: 530px; height: 630px;" class="flip_back">
                                                <img class="ProductFacing" src="">
                                                <div class="drawingArea" style="" id="drawarea_back">
                                                <canvas id="canvas_back" width="" height="" class="hove"></canvas>
                                                </div>
                                        </div>
                                        </div>
                                </div>
                        </div>

                        <div class="col-3">
                        <ul class="list-group">
                        <li class="list-group-item right-task-item uk-text-center" id="_FlipToggles">
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-info active">
                                <input type="radio" name="options" autocomplete="off"  id="_flipFront"> FRONT
                                </label>
                                <label class="btn btn-info">
                                <input type="radio" name="options" autocomplete="off" id="_flipBack" > BACK
                                </label>
                                </div>
                        </li>
                        <li class="list-group-item right-task-item uk-text-center">
                                <select class="custom-select product-select uk-text-capitalize" id="ProductSelect">
                                @foreach ($brandables as $product)
                                <option value="{{ $product->data }}" data-name="{{ str_slug($product->name) }}" data-id="{{ $product->id}}">{{ $product->name }}</option>
                                @endforeach
                                </select>
                        </li>
                        <li class="list-group-item right-task-item">
                                <p class="color-title m-0">Choose Colour</p>
                                <p class="color-sub-title m-0">Select 4 more colour </p>
                                <div class="color-pane">
                                   <ul class="list-unstyled">
                                        @foreach(config('ecommerce_config.colors') as $key => $color)
                                        @php 
                                        $disabledFirst = $loop->first ? 'disabled checked' : ''; 
                                             
                                        @endphp
                                        <li class="d-inline incr">
                                                
                                        <label class="btn-color product-color" id="checkState{{ $loop->index }}" style="background: {{ $color }}">
                                                <input type="checkbox" class="product-color-ball" name="checkState{{ $loop->index }}" value="{{ $color }}" {{ $disabledFirst }}>
                                        <span class="color-icon {{ $loop->first ? 'icon-checked': '' }}"></span>
                                           </label>
                                        </li>
                                        @endforeach
                                   </ul>
                                </div>
                        </li>

                        <li class="list-group-item right-task-item uk-text-center">
                                <a href="javascript:;" class="btn btn-info-dark btn-block mb-1" id="SellPageTrigger">SELL THIS</a>
                                <a href="buy" data-navigo class="btn btn-info-dark btn-block">PROCEED TO ORDER</a>
                        </li>

                        </ul>
                        </div>

                </div>
        </div>
</page>

<page data-route="sell" class="page-hide">
        <div class="uk-container mt-3 mb-3">
                <div class="row"> 
                    <div class="col-12">
                            <a href="home" data-navigo class="btn btn-info-dark btn-sm"><i uk-icon="arrow-left"></i> Edit Design</a>
                    </div>
                </div>

                <div class="row mt-3">
                        <div class="col-7">
                                <div class="card sell-card">
                                <h5 class="card-header sell-card-title">Fill out your product information</h5>
                                <div class="card-body">
                                        <form action="{{ route('app:ecommerce:process_design') }}" method="post" data-post-customizer>
                                                        {{ csrf_field() }}
                                                        <input type="hidden" id="data_base_price" name="base_price">
                                                        <input type="hidden" id="data_percentage" name="percentage">
                                                        <input type="hidden" id="data_price" name="price">
                                                        <input type="hidden" id="data_user_price" name="user_price">
                                                        <input type="hidden" id="data_total_earning" name="total_earning">

                                                <div class="row mb-4 no-gutters">
                                                        <div class="col-4">
                                                         <span class="_perc_title">Set Earning Percentage:</span>
                                                        </div>
                                                        <div class="col-8">
                                                        <p style="margin:0;"><input type="text" class="span2" value="" data-slider-min="0" data-slider-max="200" data-slider-step="1" data-slider-value="0" data-slider-id="RC" id="PriceSlider" data-slider-tooltip="" data-slider-handle="square"  style="width:100%;"/>
                                                        </p>
                                                        </div>
                                                </div>
                                                <div class="row mb-2">
                                                        <div class="col-6">
                                                                <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                <span class="input-group-text">Your Profit</span>
                                                                <span class="input-group-text">&#8358;</span>
                                                                </div>
                                                                <input type="number" class="form-control" aria-label="" value="" id="data_earning">
                                                                </div>
                                                        </div>
                                                        <div class="col-6">
                                                                <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                <span class="input-group-text">If You Sell</span>
                                                                </div>
                                                                <input type="number" class="form-control" aria-label="" id="data_quantity" value="1">
                                                                </div>
                                                        </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col-12 mb-2 name">
                                                        <label class="sell-label">Product Name</label>
                                                        <input type="text" class="form-control" placeholder="Product Name" name="name">
                                                        <p class="mt-1 help-block"></p>
                                                        </div>
                                                        <div class="col-12 description">
                                                        <label class="sell-label">Description</label>
                                                       <textarea class="form-control" name="description"></textarea>
                                                       <p class="mt-1 help-block"></p>
                                                        </div>
                                                </div>
                                               
                                                <div class="row">
                                                        <div class="col-12 mb-2">
                                                        <label class="sell-label">Tags</label><span class="ml-2 label-decs">comma seperated e.g lagos,ilovedad,suvenia</span>
                                                        <div class="d-block">
                                                        <input type="text" class="form-control" placeholder="" id="TagsInput" value="lagos,ilovedad,suvenia,IloveNigeria" name="tag">
                                                        </div>
                                                        </div>
                                                               
                                                </div>

                                                <div class="row">
                                                <div class="col-12 mb-2">
                                                        <button class="btn btn-info-dark" type="submit">SAVE DESIGN</button>
                                                </div>
                                                </div>

                                        </form>
                                </div>
                                </div>
                        </div>
                        <div class="col-5">
                                        <div class="card" style="border-radius:0;">
                                                        <ul class="list-group list-group-flush">
                                                           <li class="list-group-item">
                                                              <span style="text-tone text-16">THIS PRODUCT COST</span>
                                                              <h2 class="text-app app-text-primary text-heading-xl" style="margin:0;">&#8358;<span id="display_base_price" class="text-heading-xl app-text-primary">0</span></h2>
                                                           </li>
                                                           <li class="list-group-item">
                                                              <span style="text-tone text-16">YOU'LL SELL AT</span>
                                                              <h2 class="text-app app-text-head app-text-primary text-heading-xl" style="margin:0;">&#8358;<span id="display_price" class="text-heading-xl app-text-primary">0</span></h2>
                                                           </li>
                                                           <li class="list-group-item">
                                                              <span style="text-tone text-16">YOU COULD EARN UP TO</span>
                                                              <h2 class="text-app app-text-head app-text-primary text-heading-xl" style="margin:0;">&#8358;<span id="display_total_earning" class="text-heading-xl app-text-primary">0</span></h2>
                                                           </li>
                                                        </ul>
                                        </div> 
                                        
                                        <div class="uk-margin-top uk-text-center">
                                                        <!--<img src="" uk-responsive width="300" id="frontimageHolder">
                                                        <div id="frontimageHolder"></div>
                                                        <div id="backimageHolder"></div>-->
                                                        
                                                         <div style="width: 530px; height: 630px; position: relative;" class="frontimageHolder">
                                                              <img class="ProductFacing" src="">
                                                              <div class="drawingArea" style="border:0px solid red; z-index:1000;">
                                                                
                                                              </div>
                                                           </div>
                                                           <div class="backimageHolder" style="width: 530px; height: 630px;position: relative;">
                                                              <img class="ProductFacing" src="">
                                                              <div class="drawingArea"  style="border:0px solid red; z-index:1000;">
                                                              </div>
                                                           </div>
                                                        
                                                     </div>
                        
                        </div>
                </div>
        </div>
</page>

<page data-route="buy" class="page-hide">
        buy page
</page>


<div id="ImageModal" uk-modal="bg-close: false">
<div class="uk-modal-dialog app-modal">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-header">
        <h2 class="uk-modal-title">Add Image</h2>
        </div>
        <div class="uk-modal-body">
          <p class="m-0 upload-instructions">Accepted File Types (Max file size: 10MB)
                        We accept the following file types: png, jpg, pdf</p>
        <div class="upload-pane mt-3">
                
                        <div id="ImgUploader"  class="dropzone uk-text-center" uploadUrl="{{ route('app:ecommerce:upload_design') }}">
                        <div class="fallback">
                        <input name="file" type="file" multiple />
                        </div>
                        </div>
                

                <progress id="js-progressbar" class="uk-progress" value="0" max="100" hidden></progress>

        </div>

        <div class="uk-margin uk-text-center">
          <label class="m-auto"><input class="uk-checkbox" type="checkbox" checked> <span class="upload-copyright">By Uploading an Image you agree that you hold the right to modify and sell image</span></label>
                        
        </div>

        <div class="submit-pane uk-text-center">
           <button class="btn btn-info-dark" id="SaveUpload">UPLOAD</button>
        </div>

        </div>
</div>
</div>

<div id="GraphicModal" uk-modal="bg-close: false">
<div class="uk-modal-dialog app-modal">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-header">
        <h2 class="uk-modal-title">Add Graphic</h2>
        </div>
        <div class="uk-modal-body">
                <div id="Artscontent">
                <ul class=" uk-child-width-1-2@s uk-child-width-1-3@m uk-margin-top" uk-grid>
                @foreach($arts as $art)
                @if($art->name !== 'parent')
                <li class="uk-margin-bottom">
                <div class="uk-card uk-card-small uk-text-center art_single">
                <p style="margin:0;"><img src="{{ asset('Arts/parent/' . str_slug($art->name, '-') . '.svg') }}" uk-responsive width="100" class="ArtPicParent" data-url="{{ route('app:ecommerce:fetch_arts', ['pid'=> $art->ref]) }}"></p>
                <p class="art_name text-14" style="display:non; margin:0px;">{{ title_case($art->name) }}</p>
                </div>
                </li>
                @endif
                @endforeach
                </ul>
                <div class="border-top uk-margin-top uk-text-center" style="text-align:center !important;" id="PaginLink" data-link="{{ route('app:ecommerce:fetch_arts') }}">
                <p></p>
                </div>
                </div>
        </div>
</div>
</div>

<div id="TemplateModal" uk-modal="bg-close: false">
<div class="uk-modal-dialog app-modal">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-header">
        <h2 class="uk-modal-title">Add Template</h2>
        </div>
        <div class="uk-modal-body">
                <div id="Templatecontent">
                <ul class=" uk-child-width-1-2@s uk-child-width-1-3@m uk-margin-top" uk-grid>
                @foreach($templates as $template)

                <li class="uk-margin-bottom">
                <div class="uk-card uk-card-small uk-text-center template_single">
                <p style="margin:0;"><img src="{{ $template->photo_url }}" uk-responsive width="100" class="TemplateSingle" data-url="{{ route('app:ecommerce:fetch_templates', ['pid'=> $template->ref]) }}" data="{{ $template->data }}"></p>
                <p class="template_name text-14" style="display:non; margin:0px;">{{ title_case($template->name) }}</p>
                </div>
                </li>

                @endforeach
                </ul>

                </div>
                </div>
        </div>
</div>
</div>



@endsection
               
@push('customizer_js')

    <script src="{{ asset('suv/s/pg_customizer.bundle.js') }}"></script>
    
@endpush