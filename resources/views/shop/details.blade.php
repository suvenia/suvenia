@extends('layouts.layout')

@section('title', $product->name )

@section('content')
@php
    $design_colors = json_decode(json_decode($product->design->colors));
    $_prod_colors = $design_colors;
    $_prod_sizes = ['XS', 'S', 'M', 'L', 'XL', 'XXL'];
@endphp
<input id="ProductId" type="hidden" value="{{ $product->id }}">
<input id="__buyUrl" type="hidden" value="{{ route('app:ecommerce:buy_direct') }}">
<input id="hasSize" type="hidden" value="{{ count($_prod_sizes) > 0 ? true : false }}">
<div class="bg-prod-title">

    <div class="uk-container">
        <div class="row">
            <div class="col-md-3">
            <p class="mt-2"><a href="{{ route('app:ecommerce:product_catlogue', ['category'=> $category->slug]) }}" class="back_link"><i uk-icon="chevron-left" ></i> Go Back to {{ $category->name }}</a></p>
            </div>
            
            <div class="col-md-6 uk-text-center">
                <h2 class="prod-title mt-1">{{ $product->name }}</h2>
            </div>
            
        </div>
    </div> 

</div>


<div class="uk-container uk-container-smal mt-1 mb-4">
    <div class="bg-white">
        <div class="row p-4 justify-content-center no-gutters">
            <div class="col-md-7">
                    <div class="uk-position-relative" uk-slideshow="animation: fade">
                        
                            <ul class="uk-slideshow-items uk-text-center" uk-height-viewport="offset-bottom: 30">
                                @foreach ($product->photos as $photo)
                                    <li class="">
                                    <img src="{{ asset($photo->public_url) }}" alt="" width="398" class="productImgColor">
                                    </li>
                                @endforeach
                            </ul>
                        
                            <div class="uk-position-left uk-position-small mt-4">
                                <ul class="uk-thumbnav uk-thumbnav-vertical">
                                    @foreach ($product->photos as $key => $photo)
                                <li uk-slideshow-item="{{ $key }}" class="uk-box-shadow-small mb-3"><a href="#"><img src="{{ asset($photo->public_url) }}" width="70" alt=""></a></li>
                                    @endforeach
                                    
                                </ul>
                            </div>

                            <div class="uk-position-right uk-position-small uk-text-left" >
                                @if(Auth::check())
                                 @if(!$utils->check_if_favorited($product->id))
                                <a href="javascript:;" class="favorite-ico" id="productFavorite" data-url={{ route('app:ecommerce:favorite_product', ['task'=> 'add']) }} data-id="{{ $product->id }}"><span class="icon-heart"></span></a>
                                @else
                                <a href="javascript:;" class="favorite-ico active" id="productFavorite" data-url={{ route('app:ecommerce:favorite_product', ['task'=> 'rem']) }} data-id="{{ $product->id }}"><span class="icon-heart"></span></a>
                                @endif

                                @else
                                <a href="{{ route('app:user:login') }}" class="favorite-ico"></span></a>
                                
                                @endif
                            </div>
                        
                    </div>
            </div>
    
            <div class="col-md-5">
                <div class="prod-desc-pane p-3 m-auto">
                    <div class="pane-wrap">
                        <div class="prod-desc">
                            {{ ucfirst($product->description) }}
                        </div>
                        <div class="rate-intro mt-1">
                            <p class="owner mt-0 mb-0">By <a href="">{{ ucfirst($product->user->username) }}</a></p>
                            <div class="rate-summary mt-0">
                                <div class="d-inline-block">
                                        @php $rateValues = [1, 2, 3, 4, 5] ;@endphp
                                        <select class="bar-rated" >
                                                @foreach ($rateValues as $item)
                                                <option value="{{ $item }}" {{ $product->avgRating == $item ? 'selected' : ''}}>{{ $item }}</option>
                                                @endforeach
                                        </select>
                                </div>
                               
                                <div class="d-inline-block">
                                    <p class="r-review-prompt"><a href="">Write a review ></a></p>
                                </div>

                                <div class="d-inline-block float-right mt-3">
                                    <p class="m-0 view-counter"><span class="icon-eye"></span> {{ $utils->get_product_view_count($product->id) }}</p>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="prod-attr pane-wrap pt-3 pb-3">
                        <div class="prod-color">
                            <p class="prod-attr-title mb-1">Color</p>
                            <ul class="list-unstyled m-0">
                                @foreach ($_prod_colors as $key => $item)
                                    <li class="d-inline">
                                        <label class="color-box" style="background: {{ $item }}" for="colorBox{{ $key }}">
                                            <input class="app-hide color-box-input" type="radio" value="{{ $item }}" id="colorBox{{ $key }}" name="color_box">
                                         </label>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                        @if(count($_prod_sizes) > 0)
                        <div class="prod-sizes mt-1">
                            <p class="prod-attr-title mb-1">Sizes</p>
                            <ul class="list-unstyled m-0">
                                @foreach ($_prod_sizes as $key => $item)
                                    <li class="d-inline">
                                        <label class="size-box" for="sizeBox{{ $key }}">{{ $item }}
                                            <input class="app-hide size-box-input" type="radio" value="{{ $item }}" id="sizeBox{{ $key }}" name="size_box">
                                        </label>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        
                        <div class="prod-quantity mt-2">
                            <p class="prod-attr-title mb-1">Quantity</p>
                            <ul class="list-unstyled m-0">
                                <li class="d-inline"><button class="btn-oval" id="numberMinus">-</button></li>
                                <li class="d-inline"><input type="number" class="inp-qty" value="1" min="1" id="numberInputSpiner"></li>
                                <li class="d-inline"><button class="btn-oval" id="numberAdd">+</button></li>
                                
                            </ul>
                        </div>
                        
                    </div>
                    <div class="prod-pricing mt-3">
                        <p class="prod-attr-title mb-0">total price</p>
                        <div class="p-price">
                            &#8358; {{ $product->price + $product->brandable->price }}
                            {{-- $product->brandable->price + $product->price --}}
                        </div>
                        <ul class="list-unstyled m-0">
                            <li class="d-inline-block p-price-disc">&#8358; 3000</li>
                            <li class="d-inline-block w-50 p-price-disc-box"><div>-20%</div></li>
                            <li class="d-inline-block p-perc-disc">20% off</li>
                        </ul>
                    </div>
                    <div class="act-button mt-3">
                        <a href="javascript:;" class="btn btn-buy" id="__buy_direct">Buy now</a>
                        <a href="javascript:;" class="btn btn-pro-cart" data-url="{{ route('app:ecommerce:add_to_cart', ['id'=> $product->id] ) }}" id="__add_to_cart">Add to cart</a>
                    </div>
                    <div class="sell-info mt-1">
                            Got a kickass design idea? <a href="">Make money selling it here</a>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row mt-3" uk-grid uk-height-match="target: > div > .box-content">
        <div class="col-md-6">
            <p class="box-title mb-1 uk-text-uppercase">DELIVERY INFO</p>
            <div class="d-block bg-white p-3 box-content">
                    Items are produced after ordering. Please expect your package to arrive within 3-7 business days after ordering. Kindly contact us if you need urgent shipping.
                    <p class="mt-1">Shipping costs vary by location and all production is currently done in Nigeria.<p>
            </div>
        </div>
        <div class="col-md-3">
            <p class="box-title mb-1 uk-text-uppercase">product details</p>
            <div class="d-block bg-white p-3 box-content">
                <div>Style: <span>Casual</span></div>
                <div>Material: <span>Cotton, Polyester</span></div>
                <div>Sleeve Length: <span>short</span></div>
                <div>Collar: <span>Round Neck</span></div>
                <div>Store Name <a class="btn btn-info btn-xs">VISIT STORE</a></div>
            </div>
        </div>
        <div class="col-md-3">
            <p class="box-title mb-1 uk-text-uppercase">policy</p>
            <div class="d-block bg-white p-3 box-content">
                We source and make use of high quality product. however, if you're not 100% satisfied, kindly <a href="">let us know</a> and we will fix your satisfaction just right.
            </div>
        </div>
        
    </div>

    <div class="row mt-3" uk-grid uk-height-match="target: > div > .box-content">
        <div class="col-md-8">
            <p class="box-title mb-1 uk-text-uppercase">Reviews</p>
            <div class="d-block bg-white box-content">
                <div class="row no-gutters p-3">
                    <div class="col-2 uk-text-center">
                        <div class="star-div m-auto" style="width: 70px; height:70px;">
                            <div class="uk-background-contain uk-panel uk-flex uk-flex-middle uk-flex-center" style="background-image: url({{ asset('img/star.svg') }}); width: 70px; height:70px;" >
                                <div class="main-rate-counter">{{ number_format($product->avgRating, 1) }}</div>
                            </div>
                        </div>
                        @if($product->sumRating > 0)
                        <p class="no-rating m-1">No Rating yet</p>
                        @endif
                    </div>
                    <div class="col-md-6">
                        @php $fetchRatingPercentage = function($val)use($product){
                               $ratingTotal = \Illuminate\Support\Facades\DB::table('ratings')
                                        ->where([['ratingable_id', $product->id] ])->count();

                               $SingleratingTotal = \Illuminate\Support\Facades\DB::table('ratings')
                                        ->where([['rating', '=', $val], ['ratingable_id', $product->id] ])->count();
                            $RotTotal  = $SingleratingTotal > 0 ? $ratingTotal * 100/$SingleratingTotal : 0 ;
                            return $RotTotal;

                        }
                        @endphp
                          
                        <ul class="list-unstyled">
                            <li class="row no-gutters">
                                <div class="col-2"><span class="rate-list-desc">5 Stars</span></div>
                                <div class="col-8">
                                    <div class="progress rate-progress mt-2">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fetchRatingPercentage(5) }}%"></div>
                                    </div>  
                                </div>
                                <div class="col-2"><span class="rate-list-brac">({{ number_format($fetchRatingPercentage(5)) }}%)</span></div>
                            </li>
                            <li class="row no-gutters">
                                <div class="col-2"><span class="rate-list-desc">4 Stars</span></div>
                                <div class="col-8">
                                    <div class="progress rate-progress mt-2">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fetchRatingPercentage(4) }}%"></div>
                                    </div>  
                                </div>
                                <div class="col-2"><span class="rate-list-brac">({{ number_format($fetchRatingPercentage(4), 0) }}%)</span></div>
                            </li>
                            <li class="row no-gutters">
                                <div class="col-2"><span class="rate-list-desc">3 Stars</span></div>
                                <div class="col-8">
                                    <div class="progress rate-progress mt-2">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fetchRatingPercentage(3) }}%"></div>
                                    </div>  
                                </div>
                                <div class="col-2"><span class="rate-list-brac">({{ number_format($fetchRatingPercentage(3), 0) }}%)</span></div>
                            </li>
                            <li class="row no-gutters">
                                <div class="col-2"><span class="rate-list-desc">2 Stars</span></div>
                                <div class="col-8">
                                    <div class="progress rate-progress mt-2">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fetchRatingPercentage(2) }}%"></div>
                                    </div>  
                                </div>
                                <div class="col-2"><span class="rate-list-brac">({{ number_format($fetchRatingPercentage(2), 0) }}%)</span></div>
                            </li>
                            <li class="row no-gutters">
                                <div class="col-2"><span class="rate-list-desc">1 Star</span></div>
                                <div class="col-8">
                                    <div class="progress rate-progress mt-2">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: {{ $fetchRatingPercentage(1) }}%"></div>
                                    </div>  
                                </div>
                                <div class="col-2"><span class="rate-list-brac">({{ number_format($fetchRatingPercentage(1), 0) }}%)</span></div>
                            </li>

                        </ul>
                    </div>
                    <div class="col-4 rate-border p-4 uk-text-center">
                        <p class="rate-prompt-text">Be the first to rate</p>
                        
                        <div class="uk-position-relative">
                            <div class="uk-position-bottom-center">
                                    <select class="bar-rated" >
                                            @foreach ($rateValues as $item)
                                            <option value="{{ $item }}" {{ $product->avgRating == $item ? 'selected' : ''}}>{{ $item }}</option>
                                            @endforeach
                                    </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row p-3">
                        <p class="feedback-heading mt-2 mb-2">Feedback</p>
                        @if($reviews->count() < 1)
                        <div class="no-feedbacks">
                        
                            <p class="mb-2">No Reviews for this product yet</p>
                            <a href="" class="btn btn-info">REVIEW</a>
                        </div>
                        @endif

                        @if($reviews->count() > 0)
                        <div class="feedbacks">
                                <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider="center: true">
                                        <ul class="uk-slider-items uk-grid">
                                          @foreach($reviews as $review)
                                            <li class="uk-width-1-2">
                                                <div class="uk-panel review-card">
                                                   <p class="m-1 title">{{ $review->title }}</p>
                                                   <div class="content">
                                                       {{ $review->content }}
                                                   </div>
                                                </div>
                                            </li>
                                            @endforeach
                                            
                                        </ul>
                                    
                                        <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                                        <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>
                                    
                                </div>
                        </div>
                        @endif

                    
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <p class="box-title mb-1 uk-text-uppercase">faq</p>
            <div class="box-content bg-white p-3">
                <div class="d-block">
                    <div class="form-group">
                        <label class="faq-text-hd">Here are some FAQs about the product.</label>
                        <select class="form-control faq-input" id="FaqInputTrigger">
                            
                            @forelse($faqs as $faq)
                        <option value="{{ $faq->response }}" data-title={{ $faq->question }}>{{ $faq->question }}</option>
                            @empty
                            <option value="">No Questions</option>
                            @endforelse
                        </select>
                    </div> 
                </div>
                <div class="d-block" id="FaqResponse">
                
                </div>
                <div class="d-block">
                    <form action="{{ route('app:ecommerce:ask_question') }}" method="POST">
                        @csrf
                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                    <div class="form-group {{ $errors->has('question') ? 'has-error' : ''}}">
                        <label class="faq-text-hd">Need more information about the product?</label>
                        <textarea class="form-control faq-editor" rows="5" name="question"></textarea>
                        <p class="form-text help-block">{{ $errors->first('question') }}</p>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-info" type="submit">ASK</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

</div>

<div class="uk-section bg-white p-3">
    <div class="header-pane uk-text-center">
        <p class="header-pane-top m-0">PRODUCTS</p>
        <p class="header-pane-bottom m-1">CUSTOMERS WHO VIEWED THIS ITEM ALSO VIEWED</p>
    </div>
    <div class="border-bottom border-top">
        <div class="uk-container">
            <div uk-slider="center: true">
                    <div class="uk-position-relative uk-visible-toggle uk-dark">
                            <ul class="uk-slider-items uk-child-width-1-5@s uk-grid p-4">
                                @foreach ($utils->fetch_related_product($category->id) as $item)
                                <li >
                                    @include('Ecommerce::partials.single_product', ['product'=> $item])
                                    </li>
                                @endforeach
                            </ul>
                            <a class="uk-position-center-left uk-position-small uk-slidenav-large" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                            <a class="uk-position-center-right uk-position-small uk-slidenav-large" href="#" uk-slidenav-next uk-slider-item="next"></a>
                    
                    </div>
            </div>
            

        </div>
    </div>

    <div class="header-pane uk-text-center mt-3">
        <p class="header-pane-top m-0">PRODUCTS</p>
        <p class="header-pane-bottom m-1">BASED ON YOUR BROWSING HISTORY</p>
    </div>

    <div class="border-bottom border-top">
        <div class="uk-container">
           
    
            <div uk-slider="center: true">
                        <div class="uk-position-relative uk-visible-toggle uk-dark">
                                <ul class="uk-slider-items uk-child-width-1-5@s uk-grid p-4">
                                    @foreach ($utils->browsing_history() as $item)
                                    <li >
                                        @include('Ecommerce::partials.single_product', ['product'=> $item])
                                        </li>
                                    @endforeach 
                                </ul>
                                <a class="uk-position-center-left uk-position-small uk-slidenav-large" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                                <a class="uk-position-center-right uk-position-small uk-slidenav-large" href="#" uk-slidenav-next uk-slider-item="next"></a>
                        
                        </div>
                </div>
                
    
        </div>
    </div>
    

</div>
@endsection

@push('product_details_page')

    <script src="{{ asset('suv/s/pg_pdetails.bundle.js') }}"></script>
    
@endpush