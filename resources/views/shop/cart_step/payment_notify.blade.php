@extends('layouts.layout')

@section('title', 'Payment Notify')

@section('content')
<div class="uk-container mt-3 mb-3">
<div class="row justify-content-center">

@if($payment->gateway == 'PAYSTACK')

@if($payment->status)
<div class="col-6 uk-text-center">
<img src="{{ asset('img/pay_success.svg') }}" uk-responsive width="100">
<h3 class="m-2 _pay_status_title">Payment was successfull</h3>
<p class="m-1 _pay_status_text">Thank you for your Order</p>
<p class="m-1 _pay_status_text">Your Order number is <a href="">{{ $payment->reference }}</a></p>
<p class="m-1 _pay_status_text">We will email your tracking details and information.</p>
<p class="m-2"><a href="{{ route('app:base:index') }}" class="btn btn-info-dark">CONTINUE SHOPPING</a></p>

</div>
@else

<div class="col-6 uk-text-center">
<img src="{{ asset('img/pay_success.svg') }}" uk-responsive width="100">
<h3 class="m-2 _pay_status_title">Sorry your Order can not be processed</h3>
<p class="m-1 _pay_status_text">Your payment for Order number <a href="">{{ $payment->reference }}</a> is not successful </p>
<p class="m-2"><a href="{{ route('app:base:index') }}" class="btn btn-info-dark">TRY AGAIN</a></p>

</div>

@endif

@endif


@if($payment->gateway == 'PAYLATER')

@if(!$payment->status)

<div class="col-6 uk-text-center">
<img src="{{ asset('img/pay_success.svg') }}" uk-responsive width="100">
<h3 class="m-2 _pay_status_title">Your order confirmation was successful</h3>
<p class="m-1 _pay_status_text">Thank you for your Order</p>
<p class="m-1 _pay_status_text">Your Order number is <a href="">{{ $payment->reference }}</a></p>
<p class="m-1 _pay_status_text">We will email you your final invoice and information or you can also download your invoice via this link.</p>
<p class="m-2"><a href="{{ route('app:base:index') }}" class="btn btn-info-dark">CONTINUE SHOPPING</a></p>

</div>

@else

<div class="col-6 uk-text-center">
<img src="{{ asset('img/pay_error.png') }}" uk-responsive width="100">
<h3 class="m-2 _pay_status_title">Sorry your Order can not be processed</h3>
<p class="m-1 _pay_status_text">Your payment for Order number <a href="">{{ $payment->reference }}</a> is not successful </p>
<p class="m-2"><a href="{{ route('app:base:index') }}" class="btn btn-info-dark">TRY AGAIN</a></p>

</div>

@endif

@endif


</div>
</div>

@endsection