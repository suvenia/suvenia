@extends('layouts.layout')

@section('title', 'Cart Review')

@section('content')

@php //print_r(LaraCart::getItems()); @endphp

<div class="uk-container uk-container-small mt-3 mb-3">
    <div class="app-sec-header sec-center">
        <h1 class="title">CART REVIEW</h1>
        <div class="underline"></div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="uk-table cart-table uk-table-middle">
                <thead>
                    <tr>
                        <th>product</th>
                        <th>price</th>
                        <th>quantity</th>
                        <th>subtotal</th>
                    </tr>
                </thead>

                <tbody>
                    @forelse(LaraCart::getItems() as $item)
                    <tr> 
                        <td>
                            <div class="media">
                                <img src="https://suvenia.com/p-upl/prod-Xv2BKWXbRY.png" alt="" class="mr-2" width="100" height="130">
                                <div class="media-body">
                                    <p class="cart_prod_seller mb-1">Seller: Frankie</p>
                                    <p class="cart_prod_name mt-1 mb-1 uk-text-capitalize">{{ $item->name }}</p>
                                    <ul class="list-unstyled m-0">
                                        <li class="d-inline-block cart_prod_size mr-3">Size: {{ $item->size }}</li>
                                        <li class="d-inline-block"><span class="cart_prod_col_text">Colour: </span> <span class="cart_prod_col_box" style="background-color: {{ $item->color }}"></span></li>
                                    </ul>

                                    <ul class="list-unstyled mt-2">
                                        <li class="d-inline-block mr-3">
                                            <a class="cart_save_order_link" href=""><span class="icon-like"></span> Save for later</a>
                                        </li>
                                        <li class="d-inline-block">
                                            <a class="cart_delete_order_link" href="javascript:;" data-url="{{ route('app:ecommerce:delete_cart_item', ['hash'=> $item->getHash()]) }}"><span class="icon-delete"></span> Delete</a>    
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                        <td>
                            <p class="cart_prod_price mb-1">&#x20a6; {{ number_format($item->price, 2) }}</p>
                            @if($item->discount_price)
                            <strike class="cart_prod_disc_price m-1">&#x20a6; {{ $item->discount_price }}</strike>
                            <p class="cart_prod_disc_price_save m-1">You Save &#x20a6; {{ $item->discount_saved_amount}}</p>
                            @endif
                        </td>
                        <td>
                            <input type="number" class="cart_prod_quantity_input" value="{{ $item->qty }}" min="1" data-url="{{ route('app:ecommerce:update_cart_item', ['hash'=> $item->getHash()]) }}" style="width:100px;"> 
                        </td>
                        <td>
                            <p class="cart_prod_subtotal m-0">&#x20a6; {{ number_format($item->price * $item->qty, 2) }}</p>    
                        </td>
                    </tr>

                    @empty
                    <tr style="background: transparent;"><td colspan="100" class="uk-text-center"> Oops! You have not added any product to cart!<p class="mt-2"><a href="{{ route('app:base:index') }}" class="btn btn-info btn-sm">SHOP NOW</a></p></td></tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        @if(LaraCart::count() > 0)
        <div class="col-12 uk-text-right">
            <div class="cart-total-pane">
                <ul class="list-unstyled">
                    <li class="d-inline"><span class="cart-shipping-price-info mr-3">Shipping fees will be added when you checkout</span></li>
                    <li class="d-inline"><span class="cart-total-price m-0">Total: &#x20a6; {{ number_format(LaraCart::total($formatted = false), 2) }}</span></li>
                </ul>
                <ul class="list-unstyled">
                    <li class="d-inline"><a class="btn btn-info" href="{{ route('app:base:index') }}">CONTINUE SHOPPING</a></li>
                    <li class="d-inline"><a class="btn btn-info-dark" href="{{ route('app:ecommerce:step_shipping') }}">CONTINUE TO CHECKOUT</a></li>
                </ul>
            </div>
        </div>
        @endif
    </div>
</div>

@endsection

@push('cart_review_page')

<script src="{{ asset('suv/s/pg_cartr.bundle.js') }}"></script>
    
@endpush