@extends('layouts.layout')

@section('title', 'Payment')

@section('content')

<div class="uk-container mt-3 mb-3">
        <div class="app-sec-header sec-center">
            <h1 class="title">PAYMENT</h1>
            <div class="underline"></div>
        </div>

        <div class="row mt-4 justify-content-center">

            @if($zone->payment_method == 'PAYSTACK')
            <div class="col-4">
                <div class="bg-white mb-3 shadowed-box">
                    <div class="p-3 cart_shipping_zone_header_bottom">
                            <!--<p class="cart_shipping_zone_header m-0">PAYSTACK</p>-->
                        <img src="{{ asset('img/paystack.png') }}" width="200">
                    </div> 
                    <div class="row no-gutters border-bottom-pay">
                        <div class="col-12 p-3 pay-text">
                            <p class="m-1">We are going to redirect you to our online secured platform, where you will be able to pay with your local (Naira) Mastercard, Visa and Verve cards.</p>
                            <p class="m-1">To complete your payment successfully, ensure your card is enabled for online transactions. Please have your PIN and hardware token ready. SMS token may also be sent to your mobile phone.</p>
                            <p class="m-1">If your card is not enabled for online transactions, you may also make bank transfer with this platform (Paystack).</p>
                        </div>
                    </div> 
                    <div class="row p-3">
                        <div class="col-6">
                            <form action="{{ route('app:ecommerce:process_payment', ['type'=> encrypt('PAYSTACK')]) }}" method="POST">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-info-dark mt-3">CONFIRM ORDER</button>
                            </form>
                        </div>
                        <div class="col-6">
                            <p class="pay_with_text mb-2 ml-0">Pay With</p>
                            <div class="mt-0 d-inlin">
                                    <img src="{{ asset('img/master_card.png') }}" width="42" height="24" class="d-inlin">
                                    <img src="{{ asset('img/visa.png') }}" width="42" height="24" class="d-inlin">
                                    <img src="{{ asset('img/verve.png') }}" width="42" height="24" class="d-inlin">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                    <div class="bg-white mb-3 shadowed-box">
                        <div class="p-3 cart_shipping_zone_header_bottom">
                                <!--<p class="cart_shipping_zone_header m-0">PAYSTACK</p>-->
                            <img src="{{ asset('img/paylater.png') }}" width="200">
                        </div>
                        <div class="row no-gutters border-bottom-pay">
                            <div class="col-12 p-3 pay-text">
                                    <p>Thank you for your purchase. You have either chosen to complete your payment later or you are shipping to an international address. After you confirm your order, you will receive an invoice which confirms your order. For international shipping address, we would send you an updated invoice reflecting the applicable shipping cost to the address you have chosen.</p>
                                    <p>You may pay later using the link provided in your invoice or simply transfer the amount to our bank account number provided.</p>
                            </div>
                        </div>
                        <div class="row p-3"> 
                            <div class="col-6">
                                    <form action="{{ route('app:ecommerce:process_payment', ['type'=> encrypt('PAYLATER')]) }}" method="POST">
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-info-dark mt-3">CONFIRM ORDER</button>
                                        </form>
                            </div>
                            
                        </div>
                    </div>
                </div>
               
            @endif

            @if($zone->payment_method == 'PAYLATER')
            <div class="col-4">
                <div class="bg-white mb-3 shadowed-box">
                    <div class="p-3 cart_shipping_zone_header_bottom">
                            <!--<p class="cart_shipping_zone_header m-0">PAYSTACK</p>-->
                        <img src="{{ asset('img/paylater.png') }}" width="200">
                    </div>
                    <div class="row no-gutters border-bottom-pay">
                        <div class="col-12 p-3 pay-text">
                                <p>Thank you for your purchase. You have either chosen to complete your payment later or you are shipping to an international address. After you confirm your order, you will receive an invoice which confirms your order. For international shipping address, we would send you an updated invoice reflecting the applicable shipping cost to the address you have chosen.</p>
                                <p>You may pay later using the link provided in your invoice or simply transfer the amount to our bank account number provided.</p>
                        </div>
                    </div>
                    <div class="row p-3"> 
                        <div class="col-6">
                                <form action="{{ route('app:ecommerce:process_payment', ['type'=> encrypt('PAYLATER')]) }}" method="POST">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-info-dark mt-3">CONFIRM ORDER</button>
                                    </form>
                        </div>
                        
                    </div>
                </div>
            </div>
            @endif

            <div class="col-4">
                    <div class="bg-white shadowed-box">
                            <div class="p-2 cart_shipping_zone_header_bottom">
                                <p class="cart_shipping_zone_header2 m-0">YOUR ORDER ({{ count(LaraCart::getItems()) }} {{ str_plural('Item', count(LaraCart::getItems()) ) }})</p>
        
                            </div>
                            <div class="row no-gutters">
                                <div class="col-12 mt-3 p-3">
                                    <p class="shipping_sub_total mb-2">Subtotal: <span style="float: right;">&#x20a6; {{ number_format(LaraCart::total($formatted = false), 2) }}</span></p>
                                    <p class="shipping_sub_total mb-0 mt-3">Shipping fee: <span style="float: right;">&#x20a6; {{ number_format(session('CartShippingPrice'), 2) }}</span></p>
                                    <p class="shipping_total mt-1">Total: <span style="float: right;">&#x20a6; {{ number_format(LaraCart::total($formatted = false) + session('CartShippingPrice'), 2) }}</span></p>
                                </div>
        
                            </div>
                    </div>
                
            </div>

        </div>
</div>


@endsection
