@extends('layouts.layout')

@section('title', 'Home' )

@section('content')

<div class="uk-cover-container uk-height-large">
        <img src="{{ $utils->get_image('site.hpbanner') }}" alt="" uk-cover>

        <div class="row">
            <div class="col-md-7"></div>
            <div class="col-md-5" style="height: 450px; border:0px solid red;">
                
                <div class="uk-position-center" style="margin-left:-44%;">
                <div class="home-hero">
                    <b>Discover</b> amazing custom designs with great quality
                </div>
                <div class="mt-4" style="width: 60%;"> 
                <form action="{{ route('app:ecommerce:product_search') }}" method="post" autocomplete="off">
                        @csrf
                <div class="input-group">
                <input type="search" class="form-control banner-search" placeholder="Find your desired product" name="q">
                <div class="input-group-append">
                <button class="btn btn-info" type="submit">EXPLORE</button>
                </div>
                </div> 
                </form>
                </div>
                </div>

            </div>
        </div>

</div>

<div class="uk-section bg-white p-4">
        <div class="uk-container">
            <div class="row">
                <div class="col-md-3">
                    <div class="card app-info-card blue">
                        <p class="m-1"><span class="card-info-icon icon-delivery-truck"></span>
                        <div class="app-card-icon-head">FAST DELIVERY</div>
                        <small>We ship directly to your doorstep</small>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card app-info-card pink">
                        <p class="m-1"><span class="card-info-icon icon-idea"></span>
                        <div class="app-card-icon-head">BEST DESIGN</div>
                        <small>From our Experts</small>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card app-info-card sky-blue">
                        <p class="m-1"><span class="card-info-icon icon-five-stars"></span>
                        <div class="app-card-icon-head">BEST QUALITY</div>
                        <small>On all products</small>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card app-info-card green">
                        <p class="m-1"><span class="card-info-icon icon-shield"></span>
                        <div class="app-card-icon-head">SECURE</div>
                        <small>Online shopping</small>
                    </div>
                </div>

            </div>
        </div>
</div>


<div class="uk-section bg-white p-4">
    @php $_in_season = \App\Product::with(['user','categories'=>function($q){
        $q->where('parent_id', '!=', null);
    }, 'photos'])->get(); @endphp

    <div class="uk-container uk-container-small">
        <div class="app-sec-header sec-center">
            <h1 class="title">in season</h1>
            <div class="underline"></div>
        </div>

        <div class="row mt-4">

            @foreach($_in_season->take(4) as $product)
            
            <div class="col-md-3">
                @include('Ecommerce::partials.single_product', ['product'=> $product])
            </div>
            @endforeach

        </div>

    </div>

</div>

<div class="uk-section bg-white p-4">
    @php $_in_season = \App\Product::with(['user','categories'=>function($q){
        $q->where('parent_id', '!=', null);
    }, 'photos'])->get(); @endphp

    <div class="uk-container uk-container-small">
        <div class="app-sec-header sec-center">
            <h1 class="title">the movement</h1>
            <div class="underline"></div>
        </div>

        <div class="row mt-4">

            @foreach($_in_season->take(4) as $product)
            <div class="col-md-3">
                @include('Ecommerce::partials.single_product', ['product'=> $product])
            </div>
            @endforeach

        </div>

    </div>

</div>

<div class="uk-position-relative uk-visible-toggl uk-light" uk-slideshow="min-height: 236; max-height: 236; animation: push">

        <ul class="uk-slideshow-items">
            <li>
                <a href="">
                <img src="{{ $utils->get_image('site.hpadsbanner1') }}" alt="" uk-responsive>
                </a>
            </li>
            
            <li>
                <a href="">
                <img src="{{ $utils->get_image('site.hpadsbanner2') }}" alt="" uk-responsive>
                </a>
            </li>
            
        </ul>
    
        <a class="uk-slidenav-large uk-position-center-left uk-position-small uk-hidden-hove" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
        <a class=" uk-slidenav-large uk-position-center-right uk-position-small uk-hidden-hove" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
    
</div>

<div class="uk-section p-4">
        @php $_in_season = \App\Product::with(['user','categories'=>function($q){
            $q->where('parent_id', '!=', null);
        }, 'photos'])->get(); @endphp

        <div class="uk-container uk-container-small">
            <div class="top-gear" style="position: relative; border:0px solid red;">
            <div class="app-sec-header sec-left">
                <h1 class="title">drinkware</h1>
                <div class="sub-title">For Your Home and Office</div>
            </div>
            <div style="position: absolute; right:0; bottom:0;"><a href="" class="see_link">SEE ALL</a></div>
            </div>

            <div class="row mt-4">

                @foreach($_in_season->take(4) as $product)
                <div class="col-md-3">
                    @include('Ecommerce::partials.single_product', ['product'=> $product])
                </div>
                @endforeach

            </div>

        </div>

</div>

<div class="uk-section p-4">
        @php $_in_season = \App\Product::with(['user','categories'=>function($q){
            $q->where('parent_id', '!=', null);
        }, 'photos'])->get(); @endphp

        <div class="uk-container uk-container-small">
            <div class="top-gear" style="position: relative; border:0px solid red;">
            <div class="app-sec-header sec-left">
                <h1 class="title">apparrel</h1>
                <div class="sub-title">For Your Trendy Look</div>
            </div>
            <div style="position: absolute; right:0; bottom:0;"><a href="" class="see_link">SEE ALL</a></div>
            </div>

            <div class="row mt-4">

                @foreach($_in_season->take(4) as $product)
                <div class="col-md-3">
                    @include('Ecommerce::partials.single_product', ['product'=> $product])
                </div>
                @endforeach

            </div>

        </div>

</div>

<div class="uk-margin-top">
<a href="" target="_blank">
        <img src="{{ $utils->get_image('site.hpcreatedesign') }}" class="uk-width-1-1" alt="" uk-responsive>
    </a>
</div>
 
<div class="uk-section bg-white p-4">
        @php $_in_season = \App\Product::with(['user','categories'=>function($q){
            $q->where('parent_id', '!=', null);
        }, 'photos'])->get(); @endphp

        <div class="uk-container uk-container-small">
            <div class="app-sec-header sec-center">
                <h1 class="title">celebrations</h1>
                <div class="underline"></div>
            </div>

            <div class="row mt-4">

                @foreach($_in_season->take(4) as $product)
                <div class="col-md-3">
                    @include('Ecommerce::partials.single_product', ['product'=> $product])
                </div>
                @endforeach

            </div>

        </div>

</div>

<div class="uk-margin-to">
        <a href="" target="_blank">
                <img src="{{ $utils->get_image('site.hprequestdesign') }}" class="uk-width-1-1" alt="" uk-responsive>
            </a> 
</div>


@endsection