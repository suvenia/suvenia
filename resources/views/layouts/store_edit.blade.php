<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('dashboard/vendor/simple-line-icons/css/simple-line-icons.css')}}">
    <link rel="stylesheet" href="{{asset('dashboard/vendor/font-awesome/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{asset('dashboard/vendor/uikit/css/uikit.min.css')}}">
    <link rel="stylesheet" href="{{asset('dashboard/vendor/toastr/toastr.css')}}">
    <link rel="stylesheet" href="{{asset('dashboard/vendor/spectrum/spectrum.css')}}">
    <link rel="stylesheet" href="{{asset('dashboard/vendor/cropbox/jquery.cropbox.css')}}">
    <link rel="stylesheet" href="{{asset('dashboard/css/styles.css')}}">

    <style>
        html,body{
            font-family: 'Poppins', sans-serif;
        }
        .sidebar{
            background-color: #1896a9; 
        }
        .sidebar .nav-title {
                color: #FFFFFF;
            }
        .sidebar .nav-link {
            font-size: 16px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #ffffff;
    }
    .sidebar .nav-link i:first-of-type {
        color: #FFFFFF;
    }
    .sidebar .nav-dropdown.open .nav-dropdown-items{
        background-color: #0F8395;
    }
    .has-error .form-control{
        border: 1px solid #C41500;
    }
    .has-error label,
    .has-error .help-block{
        color: #C41500;
    }

    .card{
        box-shadow: 0 2px 4px 0 rgba(195, 195, 195, 0.5);
    }

    .card .card-header{
        background-color: #5dc7db;
    }

    .app-table thead tr th{
        font-size: 14px;
  font-weight: 600;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: rgba(0, 0, 0, 0.87);
  text-align: center;
    }

    .app-table tbody tr td{
        font-size: 13px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #000000;
  text-align: center;
    }

   .uk-search-input{
       background-color: #fff !important;
       width: 100% !important;
   }

   .app-btn-info{
    box-shadow: 0 2px 4px 0 rgba(192, 192, 192, 0.5);
    border: 1px solid transparent !important;
  background-color: #1896a9;
  padding: 12px;
  font-size: 14px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #ffffff;
}

.btn-app-info{
    background-color: #1896a9;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #ffffff;
}

.app-btn-danger{
    box-shadow: 0 2px 4px 0 rgba(213, 213, 213, 0.5);
  background-color: #ff2828;;
    border: 1px solid transparent !important;
  padding: 12px;
  font-size: 14px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #ffffff;
}

.app-btn-canva{
    box-shadow: 0 2px 4px 0 rgba(53, 53, 53, 0.5);
  background-color: #00687a;
  font-size: 16px;
  font-weight: 500;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #ffffff;
}

.uk-iconnav .table-btn-info{
    font-size: 14px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #ffffff;
  background-color: #1896a9 !important;
}

.uk-iconnav .table-btn-info:hover,
.app-btn-info:hover,
.app-btn-danger:hover{
    color: #fff;
}

.uk-iconnav .app-icon-link{
    font-size: 14px;
  font-weight: normal;
  color: rgba(0, 0, 0, 0.87);
  font-weight: bold;

}

.dash-heading{
    font-size: 24px;
  font-weight: 600;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #000000;
}

.btn-outline-light:hover{
    color: #000 !important;
}

.btn-radius{
    border-radius: 23px;
}

.uk-offcanvas {
    display: none;
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    z-index: 1000;
    margin-top: 55px;
}

.uk-offcanvas-bar{
    background: #1896a9;
    color: rgb(255, 255, 255);
}

.uk-offcanvas-close{
    top:0;
    left:0;
}

.form-label{
    font-size: 16px;
  font-weight: 500;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #ffffff;
}

.app-hide{
    display:none;
}

.image_radio > input{ /* HIDE RADIO */
visibility: hidden; /* Makes input not-clickable */
position: absolute; /* Remove input from document flow */
}
.image_radio > input + img{ /* IMAGE STYLES */
cursor:pointer;
border:0px solid transparent;
}
.image_radio > input:checked + img{ /* (RADIO CHECKED) IMAGE STYLES */
border:4px solid red;
}
    </style>
    @stack('styles')
</head>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">
    
    <nav class="navbar page-header">
        <a href="#" class="btn btn-link sidebar-mobile-toggle d-md-none mr-auto">
            <i class="fa fa-bars"></i>
        </a>

        <a class="navbar-brand" href="{{ route('app:base:index') }}">
            <img src="{{asset('img/logo.png')}}" alt="logo" width="100">
        </a>

        <a href="#" class="btn btn-link sidebar-toggle d-md-down-none">
            <i class="fa fa-bars"></i>
        </a>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item d-md-down-none">
                <a href="#">
                    <i class="fa fa-bell"></i>
                    <span class="badge badge-pill badge-danger">5</span>
                </a>
            </li>

            <li class="nav-item d-md-down-none">
                <a href="#">
                    <i class="fa fa-envelope-open"></i>
                    <span class="badge badge-pill badge-danger">5</span>
                </a>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="avatar avatar-sm generalProfilePic" alt="{{ Auth::user()->username }}" src="{{ !is_null($utils->get_attachment('User', Auth::user()->id)) ? asset($utils->get_attachment('User', 1)->url) : asset('user/default.png') }}" alt="" width="100">
                    <span class="small ml-1 d-md-down-none capitalize">{{ Auth::user()->username }}</span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">

                    <a href="{{ route('app:dashboard:index') }}" class="dropdown-item">
                        <i class="fa fa-user"></i> My Dashboard
                    </a>

                    <!--<a href="{{route('app:dashboard:my_orders') }}" class="dropdown-item">
                        <i class="fa fa-envelope"></i> My Orders
                    </a>-->
                    <a href="{{route('app:dashboard:my_customers') }}" class="dropdown-item">
                        <i class="fa fa-envelope"></i> Customer Orders
                    </a>
                    <a href="{{route('app:dashboard:profile-settings') }}" class="dropdown-item">
                        <i class="fa fa-envelope"></i> Profile Settings
                    </a>

                <a href="{{ route('app:user:logout') }}" class="dropdown-item post-link">
                        <i class="fa fa-lock"></i> Logout
                    </a>
                </div>
            </li>
        </ul>
    </nav>

    <div class="main-container">
        
        <div class="sidebar">
            <nav class="sidebar-nav">
                <ul class="nav">
                
                    <li class="nav-item">
                        <a href="" class="nav-link active" uk-toggle="target: #offcanvas-store-header">
                            <i class="icon icon-layers" style="font-size: 20px;"></i> Store Header
                        </a>
                    </li>
                    <li class="nav-item">
                            <a href="" class="nav-link active" uk-toggle="target: #offcanvas-store-banner">
                                <i class="icon icon-picture" style="font-size: 20px;"></i> Store Banner
                            </a>
                        </li>
                   
                    <li class="nav-item">
                        <a href="" class="nav-link active" uk-toggle="target: #offcanvas-store-products">
                            <i class="icon icon-handbag" style="font-size: 20px;"></i> My Product
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link active" uk-toggle="target: #offcanvas-store-about">
                            <i class="icon icon-info" style="font-size: 20px;"></i> About
                        </a>
                    </li>
    

                 </ul>
            </nav>
        </div> 

        <div class="content">
            
           @yield('content')
        
        </div>
        
    </div>
</div>
<script src="{{asset('dashboard/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('dashboard/vendor/popper.js/popper.min.js')}}"></script>
<script src="{{asset('dashboard/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dashboard/vendor/uikit/js/uikit.min.js')}}"></script>
<script src="{{asset('dashboard/vendor/uikit/js/uikit-icons.min.js')}}"></script>
<script src="{{asset('dashboard/vendor/toastr/toastr.min.js')}}"></script>
<script src="{{asset('dashboard/vendor/spectrum/spectrum.js')}}"></script>
<script src="{{asset('dashboard/vendor/jquery-loading-overlay/loadingoverlay.min.js')}}"></script>
<script src="{{asset('dashboard/vendor/cropbox/jquery.cropbox.js')}}"></script>
<script src="{{asset('dashboard/js/carbon.js')}}"></script>
<script src="{{asset('dashboard/js/post-link.js')}}"></script>
<script src="{{asset('dashboard/js/store-post.js')}}"></script>
<script src="{{asset('dashboard/js/batch_action.js')}}"></script>
<script src="{{asset('dashboard/js/demo.js')}}"></script>
@stack('scripts')
<script>
    $(document).ready(function(){
       function StorePage(){
            this.init = ()=>{
                this.setColor();
                this.setSwitch();
                this.setAjax();
                this.sendUpload();
                this.setSingleCheckBox();
            }
            this.setColor = ()=>{
                $('.colorpickers').each(function(){
                    var el = this;
                    var inp = $(el).attr('data-target');
                    $(el).spectrum({
                    color: $(inp).val(),
                    change: function(color) {
                        $(inp).val(color.toHexString());
                    }
                     });

                });

            }
            
            this.setSwitch = ()=>{
                var radio = $('[app-switcher]').find('input[type="radio"]');
                var radio2 = $('[app-switcher]').find('input[type="radio"]:checked');
                $(radio2).each(function(){
                    var def_cont = $(this).attr('show-content');
                     var def_hide_cont = $(this).attr('hide-content');
                    $(def_hide_cont).addClass('app-hide');
                    //$(def_cont).show();
                });
                
                $(document).on('change', radio, (e)=>{
                    var cont = $(e.target).attr('show-content');
                    var hide_cont = $(e.target).attr('hide-content');
                    if($(e.target).is(':checked')){
                        $(hide_cont).addClass('app-hide');
                    $(cont).removeClass('app-hide');
                    }
                   
                });
            }

            this.setAjax = ()=>{
                $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            }

            this.setSingleCheckBox = ()=>{
                $(document).on("click", '.single_select_checkbox', function(){
                thisRadio = $(this);
                
                if (thisRadio.hasClass("imChecked")) {
                    $('.single_select_checkbox').removeClass('imChecked');
                thisRadio.removeClass("imChecked");
                thisRadio.prop('checked', false);
                } else { 
                thisRadio.prop('checked', true);
                thisRadio.addClass("imChecked");
                };
                })
            }

            this.cropImage = (div, input)=>{
                var IMG;
                var crop = $(div).cropbox({
                width: 200,
                height: 60,
                }, function() {
                //console.log('Url: ' + this.getDataURL());
               
                 $('#CropImage').removeClass('app-hide');
                
                    IMG = this;
        
                }).on('cropbox', function(e, data) {
                ///console.log('crop window: ' + JSON.stringify(data));
                //$(input).val(this.getDataURL());
                //console.log(e);

                });
                $('#CropImage').on('click', function(){
                    IMG.update();
                   $(input).val(IMG.getDataURL());
                   console.log(IMG);
                });

                
            }

            this.sendUpload = ()=>{
                var cls = this;

                $('.js-upload').each(function(ind, val){
                    var bar = document.getElementById($(val).attr('progress'));
                    UIkit.upload(val, {

                        url: $('.js-upload').attr('data-url'),
                        //mime: true,
                        //allow:"*.png,*.jpg",
                        loadStart: function (e) {
                            console.log('loadStart', arguments);

                            bar.removeAttribute('hidden');
                            bar.max = e.total;
                            bar.value = e.loaded;
                        },

                        progress: function (e) {
                            console.log('progress', arguments);

                            bar.max = e.total;
                            bar.value = e.loaded;
                        },

                        loadEnd: function (e) {
                            console.log('loadEnd', arguments);

                            bar.max = e.total;
                            bar.value = e.loaded;
                        },

                        completeAll: function () {
                            var ImgUrl = JSON.parse(arguments[0].response);
                            var previewFrame = $(val).attr('preview');
                            var ImageInput = $(val).attr('input');
                            $(previewFrame).attr('src', ImgUrl.img);
                            $(ImageInput).val(ImgUrl.img);
                            if($(val).attr('cropable') !== undefined){
                                cls.cropImage(previewFrame, ImageInput);
                            }
                            setTimeout(function () {
                                bar.setAttribute('hidden', 'hidden');
                            }, 1000);
                        }

                    });
                });
                

            }

       };
       var hh = new StorePage();
       hh.init();
    });
</script>
</body>
</html>
