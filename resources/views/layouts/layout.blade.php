<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Suvenia - @yield('title')</title>
    <meta name="description" content="">
    @if($utils->device()->isMobile() || $utils->device()->isTablet())
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @endif 
    <link rel="stylesheet" href="https://i.icomoon.io/public/temp/1431ddd692/UntitledProject/style.css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/14.0.6/css/intlTelInput.css" rel="stylesheet">
     @stack('PAGE_STYLES')
    <link rel="stylesheet" href="{{ mix('css/vendor.css') }}">
    <link rel="stylesheet" href="{{ mix('css/main.css') }}"> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/14.0.6/js/intlTelInput.min.js"></script>

    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/main.js') }}"></script>

    <script> 
        function ___setNumber(t,e){var u=document.querySelector(t),n=document.querySelector(e),o=n.value,i=window.intlTelInput(u,{initialCountry:"auto",geoIpLookup:function(t){t(o)},utilsScript:"../../build/js/utils.js?1537727621611"});n.addEventListener("change",function(t){i.setCountry(t.target.value)})}
    </script>
 
    @include('partials.notify') 
    @stack('PAGE_SCRIPTS')

</head>

<body>
    <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    @include('partials.header')

    <div uk-height-viewport="expand: true">
        @yield('content')
    </div>

    @include('partials.footer')

</body>

</html>