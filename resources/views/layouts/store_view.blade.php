<!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>@yield('title')</title>
        <meta name="description" content="">
        @if($utils->device()->isMobile())
        <meta name="viewport" content="width=device-width, initial-scale=1">
        @endif
        
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @php
        $Fonturl = empty($store_meta['store_logo_font']) ? collect(config('dashboard_config.fonts'))->first()['url'] : 'https://fonts.googleapis.com/css?family=' . config('dashboard_config.fonts')[$store_meta['store_logo_font']]['url'];
        @endphp
    <link href="" rel="stylesheet">
       <link href="{{ asset('store/store-front.min.css')}}" rel="stylesheet">
       <link href="{{ asset('store/storefront_temp.css')}}" rel="stylesheet">
         
        <script src="{{ asset('store/store-front.min.js')}}"></script>
        <script src="{{ asset('store/blogger.js')}}"></script>
 
       <style>
           .product-box{
  border: 0px solid #fff;
  box-shadow: 0 2px 4px 0 rgba(215, 215, 215, 0.5);
  background-color: #ffffff;
  border-radius: 2px;
  text-align: center;
  position: relative;
}

.product-box .owner{
    margin:2px;
}

.product-box .owner a{
    font-size: 12px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #000000;
  text-transform: capitalize;
}

.product-box .divider{
    height: 2px;
    background: #979797;
    width: 50%;
    margin: 0 auto;
}

.product-box .title{
    margin: 1px;
}
.product-box .title a{
    font-size: 16px;
  font-weight: 600;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #000000;
  text-transform: capitalize;
}

.product-box .category{
    margin:1px;
}
.product-box .category a{
    font-size: 11px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #848484;
  text-transform: uppercase;
}

.product-box .discount{
  width: 40px;
  height: 20px;
  background-color: #ffffff;
  border:1px solid #116b79;
  color: #116b79;
  font-size: 12px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  position: absolute;
  right:20px;
}

           .nav-capsule li a{
               
           }
           .input-trans{
               background: rgba(5, 112, 126, 0.5);
               border: 0px;
               border-radius: 0;
               color: rgba(255, 255, 255, 0.6);
           }
           .input-trans:disabled{
               opacity:1;
               background: rgba(5, 112, 126, 0.5);
           }
           
           .input-trans:focus{
               background: rgba(5, 112, 126, 0.5);
               border: 0px;
               border-radius: 0;
               color: rgba(255, 255, 255, 1);
           }
           .input-trans::placeholder{
               color: rgba(255, 255, 255, 0.6);
           }
           
           
            .middle-nav .uk-navbar li a, .middle-nav .uk-navbar .uk-navbar-item {
            color: {{ $store_meta['store_theme_color'] }};
            }

            #NavOffcanvas .uk-offcanvas-bar .mobile-side-nav li a i, #NavOffcanvas .uk-offcanvas-bar .mobile-side-nav li a{
                color: {{ $store_meta['store_theme_color'] }};
            }

            #NavOffcanvas .uk-offcanvas-bar .mobile-side-nav li a i, #NavOffcanvas .uk-offcanvas-bar .mobile-side-nav li a{
                color: {{ $store_meta['store_theme_color'] }};
            }

            #NavOffcanvas .uk-offcanvas-bar .mobile-side-nav li:hover {
            background: rgba(255, 255, 255, 0.4);
            }

            #NavOffcanvas .uk-offcanvas-bar .uk-offcanvas-close, #NavOffcanvas .uk-offcanvas-bar .uk-nav-default li a, #NavOffcanvas .uk-offcanvas-bar ul li a, #NavOffcanvas .uk-offcanvas-bar *{
                color: {{ $store_meta['store_theme_color'] }};
            }

            .btn-secondary{
                background-color: {{ $store_meta['store_theme'] }} !important;
                border: 1px solid transparent;
                color: {{ $store_meta['store_theme_color'] }};
            }

            .btn-secondary:hover{
                background: rgba(255, 255, 255, 0.6);
                border:1px solid transparent;
            }

            .text-logo *{
                color: {{ $store_meta['store_logo_color'] }} !important;
                font-family: {{ $store_meta['store_logo_font'] }};
            }

       </style>
    </head>
    <body>

<!--<div class="uk-section uk-section-small uk-background-secondary uk-light p-2 m-0">
<div class="uk-container">
<div class="row justify-content-center">
<div class="col-md-8 uk-text-center">
<p class="m-0">Discover, buy and sell kickass merchandise on <a href="{{ route('app:base:index') }}">Suvenia.com</a></p>
</div>
</div>
</div>
</div>-->

@if(!$utils->device()->isMobile() || $utils->device()->isTablet())
<div class="uk-navbar-container middle-nav" style="background-color: {{ $store_meta['store_theme'] }} !important;"> 
<div class="uk-container uk-container-expand">
    <nav class="uk-navbar">
         <div class="uk-navbar-left">
             <ul class="uk-navbar-nav">
             <li class=""><a href="">Home</a></li>
             <li class=""><a href="">Shop</a></li>
             <li class=""><a href="">About us</a></li>
             <!--<li class=""><a href="">Blog</a></li>-->
             </ul>
        </div>
         
    <div class="uk-navbar-center">
        
            @if($store_meta['default_brand'] == 'IMAGE')
            <a class="uk-navbar-item uk-logo" href="">
        <img src="{{ asset('store_img/' . $store_meta['store_logo']) }}" uk-responsive width="100">
            </a>
            @else
            <a class="uk-navbar-item uk-logo text-logo" href="">
                <h3>{{ $store_meta['store_logo_text'] }}</h3>
            </a>
            @endif
       
    </div>

   <div class="uk-navbar-right">
              <a class="uk-navbar-item uk-logo" href="javascript:;" uk-toggle="target: #searchModal"><i uk-icon="search"></i></a>
        </div>

    </nav>
</div>
</div>

<div id="searchModal" uk-modal>
    <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">
    <h4 class="">Search For Products</h4>
    <button class="uk-modal-close-default" type="button" uk-close></button>
    <div class="mt-3">
        <form action="" method="post">
            {{csrf_field() }}
        <div class="uk-margin">
        <input class="uk-input" type="search" placeholder="Search" name="search_string">
        </div>
            
        </form>
        
    </div>
    </div>
    </div>

@else

<div class="uk-navbar-container middle-nav" style="background-color: {{ $store_meta['store_theme'] }} !important;">
<div class="uk-container uk-container-expand">
    <nav class="uk-navbar">
        
     <div class="uk-navbar-left">
    <a href="javascript:;" class="uk-navbar-item" uk-toggle="target: #NavOffcanvas" style="focus: text-decoration:none;"><i uk-icon="icon:menu; ratio: 1.5;"></i></a>  
    </div>

    <div class="uk-navbar-center">
    @if($store_meta['default_brand'] == 'IMAGE')
    <a class="uk-navbar-item uk-logo" href="">
    <img src="{{ asset('store_img/' . $store_meta['store_logo']) }}" uk-responsive width="100">
    </a>
    @else
    <a class="uk-navbar-item uk-logo text-logo" href="">
        <h3>{{ $store_meta['store_logo_text'] }}</h3>
    </a>
    @endif
    </div>

   <div class="uk-navbar-right">
              <a class="uk-navbar-item uk-logo" href="javascript:;" uk-toggle="target: #searchModal"><i uk-icon="search"></i></a>
        </div>

    </nav>
</div>
</div>



 <div id="NavOffcanvas" uk-offcanvas="overlay: true">
        <div class="uk-offcanvas-bar" style="background: {{ $store_meta['store_theme'] }} !important;">
        <button class="uk-offcanvas-close" type="button" uk-close></button>
            
        <div class="mt-4">
            <ul class="list-unstyled mobile-side-nav">
                <li class="d-block"><a href="">Home</a></li>
             <li class="d-block"><a href="">Shop</a></li>
             <li class="d-block"><a href="">About us</a></li>
            <!-- <li class="d-block"><a href="">Blog</a></li>-->
            </ul>
        </div>

        </div>
        </div>

    <div id="searchModal" uk-modal>
    <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">
    <h4 class="">Search For Products</h4>
    <button class="uk-modal-close-default" type="button" uk-close></button>
    <div class="mt-3">
        <form action="" method="post">
            {{csrf_field() }}
        <div class="uk-margin">
        <input class="uk-input" type="search" placeholder="Search" name="search_string">
        </div>
            
        </form>
        
    </div>
    </div>
    </div>


@endif
<div class="" uk-height-viewport="expand: true">
 @yield('content') 
</div>
@includeIf('front.stores.store_partials.footer')

</body>
</html>
