<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>@yield('title')</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600" rel="stylesheet">
    @stack('PAGE_STYLES')
    <link rel="stylesheet" href="{{ mix('css/dashboard_vendor.css') }}">
    <link rel="stylesheet" href="{{ mix('css/dashboard_main.css') }}"> 
    @stack('PAGE_STYLES')
   
</head>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">
    
    <nav class="navbar page-header">
        <a href="#" class="btn btn-link sidebar-mobile-toggle d-md-none mr-auto">
            <i class="fa fa-bars"></i>
        </a>

        <a class="navbar-brand" href="{{ route('app:base:index') }}">
            <img src="{{ $utils->get_image('site.logo') }}" alt="logo" width="100">
        </a>

        <a href="#" class="btn btn-link sidebar-toggle d-md-down-none">
            <i class="fa fa-bars"></i>
        </a>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item d-md-down-none">
                <a href="#">
                    <i class="fa fa-bell"></i>
                    <span class="badge badge-pill badge-danger">5</span>
                </a>
            </li>

            <li class="nav-item d-md-down-none">
                <a href="#">
                    <i class="fa fa-envelope-open"></i>
                    <span class="badge badge-pill badge-danger">5</span>
                </a>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="avatar avatar-sm generalProfilePic" alt="{{ Auth::user()->username }}" src="{{ !is_null($utils->get_attachment('User', Auth::user()->id)) ? asset($utils->get_attachment('User', 1)->url) : asset('user/default.png') }}" alt="" width="100">
                    <span class="small ml-1 d-md-down-none capitalize">{{ Auth::user()->username }}</span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">

                    <a href="{{ route('app:dashboard:index') }}" class="dropdown-item">
                        <i class="fa fa-user"></i> My Dashboard
                    </a>

                    <!--<a href="{{route('app:dashboard:my_orders') }}" class="dropdown-item">
                        <i class="fa fa-envelope"></i> My Orders
                    </a>-->
                    <a href="{{route('app:dashboard:my_customers') }}" class="dropdown-item">
                        <i class="fa fa-envelope"></i> Customer Orders
                    </a>
                    <a href="{{route('app:dashboard:profile-settings') }}" class="dropdown-item">
                        <i class="fa fa-envelope"></i> Profile Settings
                    </a>

                <a href="{{ route('app:user:logout') }}" class="dropdown-item post-link">
                        <i class="fa fa-lock"></i> Logout
                </a>
                </div>
            </li>
        </ul>
    </nav>

    <div class="main-container">
        
        <div class="sidebar">
            <nav class="sidebar-nav">
                <ul class="nav">

                   
                    @foreach ($utils->userRoles() as $role)
                        @if ($role == 'user')
                        @include('partials.acl_menu.' . $role, ['type'=>'admin_sidebar', 'role'=> $role])
                        @endif
                        @if ($role == 'influencer')
                        @include('partials.acl_menu.' . $role, ['type'=>'admin_sidebar', 'role'=> $role])
                        @endif
                        @if ($role == 'designer')
                        @include('partials.acl_menu.' . $role, ['type'=>'admin_sidebar', 'role'=> $role])
                        @endif
                    @endforeach

                 </ul>
            </nav>
        </div>

        <div class="content">
             
           @yield('content')
        
        </div>
        
    </div>
</div>
<script src="{{ mix('js/dashboard_vendor.js') }}"></script>
<script src="{{ mix('js/dashboard_main.js') }}"></script>
@stack('PAGE_SCRIPTS')
</body>
</html>
