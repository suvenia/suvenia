@extends('layouts.layout')

@section('title', 'Become A Designer' )

@section('content')

<a href="{{ route('app:designer:sign_up') }}" class="btn btn-info">Sign Up</a>

@endsection