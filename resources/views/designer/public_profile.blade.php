@extends('layouts.layout')

@section('title', 'Sign Up As A Designer' )

@section('content')


<div class="uk-container  mt-3 mb-3">
    <div class="designer-top-card p-3">
        <div class="row justify-content-center">
            <div class="col-md-5 mb-2">
                <div class="row">
                    <div class="col-12">
                        <div class="profile_pic_holder"><div class="logged_stat"></div><img src="{{ asset('designer/large.jpg') }}" width="100" uk-responsive class="uk-border-circle"></div>
                        <div class="des_profile_name mt-1 uk-text-center">Flo Graphix</div>
                        <div class="des_profile_desc mt-1 uk-text-center">
                                Professional Graphic Designer Who Cares More About Giving Client Creative and Unique Designs.
                        </div> 
                        <div class="mt-4 gig-tab">
                            @php
                                $gigs = [
                                    [
                                        'id'=> 1,
                                        'title'=> 'Basic',
                                        'amount'=> 2000,
                                        'delivery_time'=> 4,
                                        'revisions'=> 'unlimited',
                                        'is_printable'=> true,
                                        'source_file'=> false,
                                        'design_concepts'=> '3',
                                        'description'=> 'I will give you JPEG, PNG files, free 3D mockup',
                                    ],
                                    [
                                        'id'=> 2,
                                        'title'=> 'Standard',
                                        'amount'=> 2000,
                                        'delivery_time'=> 4,
                                        'revisions'=> 'unlimited',
                                        'is_printable'=> true,
                                        'source_file'=> true,
                                        'design_concepts'=> '3',
                                        'description'=> 'I will give you JPEG, PNG files, free 3D mockup',
                                    ],
                                    [
                                        'id'=> 3,
                                        'title'=> 'Premium',
                                        'amount'=> 2000,
                                        'delivery_time'=> 4,
                                        'revisions'=> 'unlimited',
                                        'is_printable'=> true,
                                        'source_file'=> true,
                                        'design_concepts'=> '3',
                                        'description'=> 'I will give you JPEG, PNG files, free 3D mockup',
                                    ],

                        ];

                                $gigs_obj = json_decode(json_encode($gigs));
                            @endphp
                                <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                                    @foreach ($gigs_obj as $gig)
                                        <li class="nav-item">
                                        <a class="nav-link {{ $loop->first ? 'active' : ''}}" id="gig_tab_ar{{ $gig->id }}" data-toggle="tab" href="#gig_tab{{ $gig->id }}" role="tab" aria-controls="{{ $gig->title }}" aria-selected="true">{{ $gig->title }}</a>
                                        </li>
                                    @endforeach
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        @foreach ($gigs_obj as $gig)
                                        <div class="tab-pane fade {{ $loop->first ? 'show active' : ''}}" id="gig_tab{{ $gig->id }}" role="tabpanel" aria-labelledby="gig_tab_ar{{ $gig->id }}">
                                            <div class="p-3">
                                                <div class="tab-gig-price">&#8358; {{ $gig->amount }}</div>
                                                <div class="tab-gig-desc">{{ $gig->description }}</div>
                                                <div class="tab-gig-desc mb-0">
                                                    <ul class="list-unstyled mb-0 mt-1">
                                                        <li class="d-inline-block"><i class="icon-clock mr-2"></i>Time: 4 Days</li>
                                                        <li class="d-inline-block ml-2"><i class="icon-refresh mr-2"></i>Revisions: Unlimited</li>
                                                    </ul>
                                                </div>
                                                <div class="tab-gig-desc mt-2">
                                                    <ul class="list-unstyled">
                                                        <li class="d-block"><i class="icon-checked mr-2 color-check"></i>Time: 4 Days</li>
                                                        <li class="d-block">
                                                                @if($gig->is_printable)
                                                                <i class="icon-checked mr-2 color-check"></i>
                                                                @else
                                                                <i class="icon-cancel mr-2 color-cancel"></i>
                                                                @endif
                                                            Print-Ready
                                                        </li>
                                                        <li class="d-block">
                                                            @if($gig->source_file)
                                                            <i class="icon-checked mr-2 color-check"></i>
                                                            @else
                                                            <i class="icon-cancel mr-2 color-cancel"></i>
                                                            @endif
                                                            Source Files</li>
                                                    </ul>
                                                </div>
                                                <div class="mt-1">
                                                    <a href="" class="btn btn-info-dark btn-sm">HIRE</a>
                                                </div>

                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                @php 
                    $__portfolios = [
                        [
                            'photo_url'=> 'https://secure.i.telegraph.co.uk/multimedia/archive/03161/D56P4E-smile_3161171b.jpg'
                        ],
                        [
                            'photo_url'=> 'http://theresekerr.com/wp-content/uploads/2013/03/Untitled.png'
                        ],
                        [
                            'photo_url'=> 'https://i.ytimg.com/vi/SINosl8McV0/maxresdefault.jpg'
                        ],
                        [
                            'photo_url'=> 'https://image.shutterstock.com/image-photo/horizontal-shot-positive-gorgeous-young-260nw-1074509096.jpg'
                        ],
                    ];

                $__getPortfolios = json_decode(json_encode($__portfolios));
                @endphp
                    <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow>

                            <ul class="uk-slideshow-items">
                                @foreach ($__getPortfolios as $photo)
                                    <li>
                                        <img src="{{ $photo->photo_url }}" alt="" uk-cover>
                                    </li>
                                @endforeach
                        
                            </ul>
                        
                            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
                            <ul class="uk-thumbnav mt-3">
                                    @foreach ($__getPortfolios as $photo)
                                    <li class="uk-active" uk-slideshow-item="{{ $loop->index }}"><a href="javascript:;"><img src="{{ $photo->photo_url }}" width="70" alt=""></a></li>
                                    @endforeach
                            </ul>
                    </div>

                
                        
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card mt-3 plan-card">
                <div class="card-header">
                    About Our Work
                </div>
                <div class="card-body about_content">
                        <p>Hello, I&nbsp;provide high-quality logo design&nbsp;service with unmatched quality and outstanding delivery time (within 24 hours)</p><h4>WHAT YOU WILL GET!</h4><ul><li>High quality logo&nbsp;+ free 3D mockup</li><li>High quality logo files</li><li>Unlimited modifications for a design</li><li>Customer satisfaction</li><li>Your amazing design within 24 hours or less</li><li>300 DPI&nbsp; image</li></ul><h4>WHY CHOOSE ME</h4><ul><li>Professional</li><li>Easy to communicate with</li><li>High Quality and Fast Delivery</li><li>Creative and Original</li><li>Always give what you request</li><li>Professional Unique, Creative and modern Logo designs ideas.</li><li>Friendly Customer Service</li><li>In-time Delivery</li><li>Money back Guaranty in any failure.</li></ul><p>I also design business card, letterhead, envelope and presentation folder which can be added when placing your order. For more information about my service, please send me a message.</p>
                </div>
            </div>
        </div>

        <div class="col-md-6">
                <div class="card mt-3 plan-card">
                        <div class="card-header">
                            Compare Plans
                        </div>
                         @php
                        $price_compare_data = [
                            ['name'=> '', 'key'=> 'title'],
                            ['name'=> 'Description', 'key'=> 'description'],
                            ['name'=> 'Print-Ready', 'key'=> 'is_printable'],
                            ['name'=> 'Source File', 'key'=> 'source_file'],
                            ['name'=> 'Design Concepts', 'key'=> 'design_concepts'],
                            ['name'=> 'Revision', 'key'=> 'revisions'],
                            ['name'=> 'Delivery Time', 'key'=> 'delivery_time'],
                            ['name'=> '', 'key'=> 'button'],
                        ];
                        $__compare_data = json_decode(json_encode($price_compare_data));
                    @endphp
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-bordered compare_table mb-0">
                                        
                                    <tbody>
                                    @foreach($__compare_data as $compare_data)
                                    <tr>
                                        <th class="compare_title">{{ $compare_data->name }} </th>
                                        @foreach($gigs_obj as $gig)
                                        @php $kkey = $compare_data->key @endphp
                                            @if($compare_data->key == 'title')
                                            <td class="cell_price">
                                                <div class="cell_gig_title">{{ $gig->$kkey }}</div>
                                            <span class="d-block">N{{ $gig->amount }}</span>
                                            </td>
                                            @elseif($compare_data->key == 'delivery_time')
                                            <td class="cell_con">{{ $gig->$kkey }} Days</td>
                                            @elseif($compare_data->key == 'is_printable' || $compare_data->key ==  'source_file')
                                                <td class="cell_con">
                                                        @if($gig->$kkey)
                                                        <i class="icon-checked mr-2 color-check"></i>
                                                        @else
                                                        <i class="icon-cancel mr-2 color-cancel"></i>
                                                        @endif
                                                </td>
                                            @elseif($compare_data->key == 'button')
                                                <td class="cell_con">
                                                    <a class="btn btn-info-dark btn-sm" style="color: #fff;">SELECT</a>
                                                </td>
                                            @else
                                                <td class="cell_con">{{ $gig->$kkey }}</td>
                                            @endif
                                        @endforeach
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
        </div>
    </div>

</div>


@php 

$related_designers = [
    [
        'display_name'=> 'mattis aliquam',
        'cover_photo'=> 'https://secure.i.telegraph.co.uk/multimedia/archive/03161/D56P4E-smile_3161171b.jpg',
        'profile_photo'=> 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/power-of-smile-1446825602.jpg?crop=0.798xw:1xh;center,top&resize=100:*',
        'catch_phrase'=> 'mattis aliquam faucibus purus in',
        'bio'=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Commodo sed egestas egestas fringilla phasellus. Fermentum leo vel orci porta non pulvinar. Amet facilisis magna etiam tempor. Odio ut enim blandit volutpat.',
        'minimum_price'=> '2000'
    ],
    [
        'display_name'=> 'mattis aliquam',
        'cover_photo'=> 'https://secure.i.telegraph.co.uk/multimedia/archive/03161/D56P4E-smile_3161171b.jpg',
        'profile_photo'=> 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/power-of-smile-1446825602.jpg?crop=0.798xw:1xh;center,top&resize=100:*',
        'catch_phrase'=> 'mattis aliquam faucibus purus in',
        'bio'=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Commodo sed egestas egestas fringilla phasellus. Fermentum leo vel orci porta non pulvinar. Amet facilisis magna etiam tempor. Odio ut enim blandit volutpat.',
        'minimum_price'=> '2000'
    ],
    [
        'display_name'=> 'mattis aliquam',
        'cover_photo'=> 'https://secure.i.telegraph.co.uk/multimedia/archive/03161/D56P4E-smile_3161171b.jpg',
        'profile_photo'=> 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/power-of-smile-1446825602.jpg?crop=0.798xw:1xh;center,top&resize=100:*',
        'catch_phrase'=> 'mattis aliquam faucibus purus in',
        'bio'=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Commodo sed egestas egestas fringilla phasellus. Fermentum leo vel orci porta non pulvinar. Amet facilisis magna etiam tempor. Odio ut enim blandit volutpat.',
        'minimum_price'=> '2000'
    ],
    [
        'display_name'=> 'mattis aliquam',
        'cover_photo'=> 'https://secure.i.telegraph.co.uk/multimedia/archive/03161/D56P4E-smile_3161171b.jpg',
        'profile_photo'=> 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/power-of-smile-1446825602.jpg?crop=0.798xw:1xh;center,top&resize=100:*',
        'catch_phrase'=> 'mattis aliquam faucibus purus in',
        'bio'=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Commodo sed egestas egestas fringilla phasellus. Fermentum leo vel orci porta non pulvinar. Amet facilisis magna etiam tempor. Odio ut enim blandit volutpat.',
        'minimum_price'=> '2000'
    ],
    
];
$all_related_designers = json_decode(json_encode($related_designers));
@endphp

<div class="uk-container  mt-3">
        <div class="app-sec-header sec-center">
                <h1 class="title">Recommended for you</h1>
                <div class="underline"></div>
            </div>
    <div class="row mt-3">
            <div uk-slider="center: true">

                    <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
                
                        <ul class="uk-slider-items uk-child-width-1-4@s uk-grid">
                            @foreach($all_related_designers as $related_designer)
                            <li>
                                <div class="uk-card uk-card-default">
                                    <div class="uk-card-media-top">
                                    <img src="{{ $related_designer->cover_photo }}" alt="">
                                    </div>
                                    <div class="uk-card-body p-0">
                                    <div class="p-2">
                                        <ul class="m-0 p-0">
                                            <li class="d-inline-block">
                                            <img src="{{ $related_designer->profile_photo }}" class="uk-border-circle" uk-responsive width="36">
                                            </li>
                                            <li class="d-inline-block">
                                            <p class="m-0 designer_card_title">{{ $related_designer->display_name }}</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="pr-2 pl-2 pb-5">
                                    <p class="m-0 designer_card_catch_phrase">{{ $related_designer->catch_phrase }}</p>
                                    </div>
                                    <div class="border-top">
                                    <div class="row">
                                        <div class="col-6">
                                            <p class="m-0 p-2 designer_card_price_pane">STARTING PRICE</p>
                                        </div>
                                        <div class="col-6">
                                            <p class="m-0 uk-text-right p-2 designer_card_price_pane">{{ $related_designer->minimum_price }}</p>
                                        </div>
                                    </div>
                                    </div>

                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                
                        <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                        <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>
                
                    </div>
                
                    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
                
                </div>
    </div>
</div>

@endsection