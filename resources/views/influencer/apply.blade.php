@extends('layouts.layout')

@section('title', 'Become An Influencer' )

@section('content')


<div class="uk-container mt-3 mb-3">
        <div class="row justify-content-center">
       
       <div class="col-md-4">
       
         <div class="card app-box">
           <div class="box-header">
             <p class="title">SIGN UP AS INFLUENCER</p>
             <p class="sub-title">Please fill the form to create your account</p>
           </div>
           <div class="box-body"> 
               <form class="app-form" action="{{ route('app:influencer:apply') }}" method="POST" >
                   @csrf 
                   <div class="form-group {{ $errors->has('username') ? 'has-error' : ''}}">
                      <input type="text" class="form-control form-control-lg" placeholder="Username" name="username"  value="{{ old('username') }}">
                      <p class="form-text help-block">{{ $errors->first('username') }}</p>
                    </div>
                   
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                      <input type="email" class="form-control form-control-lg" placeholder="Email Address" name="email"  value="{{ old('email') }}">
                      <p class="help-block">{{ $errors->first('email') }}</p>
                    </div>
                   
                    <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                      <input type="password" class="form-control form-control-lg" placeholder="Password" name="password"  value="{{ old('password') }}">
                      <p class="form-text help-block">{{ $errors->first('password') }}</p>
                    </div>
    
                    <div class="form-group {{ $errors->has('confirm_password') ? 'has-error' : ''}}">
                      <input type="password" class="form-control form-control-lg" placeholder="Confirm Password" name="confirm_password"  value="{{ old('confirm_password') }}">
                      <p class="form-text help-block">{{ $errors->first('confirm_password') }}</p>
                    </div>

                    <div class="form-group {{ $errors->has('instagram') ? 'has-error' : ''}}">
                            <input type="url" class="form-control form-control-lg" placeholder="Instagram Link" name="instagram"  value="{{ old('instagram') }}">
                            <p class="help-block">{{ $errors->first('instagram') }}</p>
                    </div>

                    <div class="form-group {{ $errors->has('twitter') ? 'has-error' : ''}}">
                            <input type="url" class="form-control form-control-lg" placeholder="Twitter Link" name="twitter"  value="{{ old('twitter') }}">
                            <p class="help-block">{{ $errors->first('twitter') }}</p>
                    </div>

                    <div class="form-group form-row {{ $errors->has('tos') ? 'has-error' : ''}}">
                            <div class="col-md-12">
                               <div class="custom-control custom-checkbox mr-sm-2">
                                   <input type="checkbox" class="custom-control-input" id="customControlAutosizing" value="1" name="tos">
                                   <label class="custom-control-label remember-text" for="customControlAutosizing">I agree to the terms</label>
                                 </div>
                            </div>
                            <p class="help-block">{{ $errors->first('tos') }}</p>
                    </div>

    
                      <button type="submit" class="btn btn-info  btn-block rounded">SIGN IN</button>
                    </form>
    
           </div>
           
         </div>
           
       </div>
       
        </div>
    </div>

@endsection