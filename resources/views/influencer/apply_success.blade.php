@extends('layouts.layout')

@section('title', 'Success' )

@section('content')

<div class="uk-container mt-3">
    <div class="row justify-content-center">
    <div class="col-md-6">
            <div class="app-sec-header sec-center mb-4">
                    <h2 class="title">Congratulations!</h2>
            </div>
        <p class="uk-text-center">A link has been sent to your mail to confirm your email address to complete your account.</p>
        <p class="uk-text-center">Our Team shall review your application and send you an invitation code once we verify your data, meanwhile feel free to log into your account while we verify your account.</p>
        <p class="uk-text-center"><a href="{{ route('app:user:login') }}" class="btn btn-info-dark">LOGIN NOW</a></p>
    </div>
    </div>
</div>

@endsection