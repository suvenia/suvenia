@extends('layouts.dashboard')

@section('title', 'All Gigs' )

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header bg-light">
                My gigs
            </div>

            <div class="card-body">
                <div class="mb-2">

                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($gigs as $gigs)
                                @php
                                $photo = $gig->product->photos->first();
                                $design_colors = json_decode(json_decode($gig->product->design->colors));
                                $get_color = collect($design_colors)->random();
                                @endphp
                        <tr>
                            <td><img src="{{ asset($photo->public_url) }}" width="70" style="background: {{ $get_color }};"></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                {{ $gigs->links() }}
            </div>
        </div>
    </div>
</div>

@endsection