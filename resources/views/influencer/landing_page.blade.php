@extends('layouts.layout')

@section('title', 'Become An Influencer' )

@section('content')


<div class="uk-cover-container uk-height-large">
    <img src="{{ $utils->get_image('site.lpinfluencer') }}" alt="" uk-cover>
    <!--<div class="uk-overlay-primary uk-position-cover"></div>-->

    <div class="row">
       
        <div class="col-md-7" style="height: 450px; border:0px solid red;">
            
            <div class="uk-position-center ml-5" style="">
            <div class="home-hero">
                <b>EARN WITH SUVENIA</b>
            </div> 
            <div class="mt-4" style="width: 60%; color: #fff;"> 
                Are you an influencer? Get paid a percentage on products you share and promote on social media. 
            </div>
            <div class="mt-3" style="width: 60%; color: #fff;"> 
                <a class="btn btn-info">GET STARTED</a> 
            </div>

            </div>

        </div>
        <div class="col-md-5"></div>
    </div>

</div>


<div class="uk-section bg-white p-4">
        <div class="uk-container uk-container-small">
            <div class="app-sec-header sec-center mb-4">
                    <h1 class="title">HOW IT WORKS</h1>
            </div>
            <div class="row mt-2">

                <div class="col-md-3">
                    <div class="service-box">
                        <div class="icon-ball d-inline-block">
                            <span class="icon-enter"></span>
                        </div>
                        <h4 class="mt-2 mb-3 service-title">
                                Register
                            </h4>
                        <p class="mt-1 mb-0 service-desc">Register by filling short details about you as an influencer</p>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="service-box">
                        <div class="icon-ball d-inline-block">
                            <span class="icon-verified"></span>
                        </div>
                        <h4 class="mt-2 mb-3 service-title">
                            Verify
                        </h4>
                        <p class="mt-1 mb-0 service-desc">
                                Get your account verified as an influencer by following the verification process
                        </p>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="service-box">
                        <div class="icon-ball d-inline-block">
                            <span class="icon-share"></span>
                        </div>
                        <h4 class="mt-2 mb-3 service-title">
                            Share
                        </h4>
                        <p class="mt-1 mb-0 service-desc">
                            Choose from our products and share with your link.
                        </p>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="service-box">
                        <div class="icon-ball d-inline-block">
                            <span class="icon-email_money"></span>
                        </div>
                        <h4 class="mt-2 mb-3 service-title">
                            Get Paid
                        </h4>
                        <p class="mt-1 mb-0 service-desc">
                            Get your earnings paid directly to your preffered bank on all sales made
                        </p>
                    </div>
                </div>
                
            </div>
        </div>
</div>

<div class="uk-section bg-white p-4">
    <div class="uk-container">
            <div class="app-sec-header sec-center mb-4">
                    <h1 class="title">JOIN OUR INFLUENCERS COMMUNITY</h1>
            </div>

            <div class="row">
                
                <div class="col-md-3 mb-3">
                    <a href="">
                    <div class="uk-cover-container influncer-grid-single">
                            <canvas width="280" height="250"></canvas>
                            <img src="{{ asset('mamamamama.jpg') }}" alt="" uk-cover>
                            <div class="uk-overlay-prima uk-position-cover"></div>
                            <div class="uk-position-bottom p-3">
                                <p class="mt-1 mb-0 influncer_name">Designer</p>
                                <p class="mt-1 mb-0 influencer_title">Twitter Influencer</p>
                            </div>
                    </div>
                    </a>
                </div>
                
                <div class="col-md-3 mb-3">
                    <a href="">
                    <div class="uk-cover-container influncer-grid-single">
                            <canvas width="280" height="250"></canvas>
                            <img src="{{ asset('mamamamama.jpg') }}" alt="" uk-cover>
                            <div class="uk-overlay-prima uk-position-cover"></div>
                            <div class="uk-position-bottom p-3">
                                <p class="mt-1 mb-0 influncer_name">Designer</p>
                                <p class="mt-1 mb-0 influencer_title">Twitter Influencer</p>
                            </div>
                    </div>
                    </a>
                </div>
                
                <div class="col-md-3 mb-3">
                    <a href="">
                    <div class="uk-cover-container influncer-grid-single">
                            <canvas width="280" height="250"></canvas>
                            <img src="{{ asset('mamamamama.jpg') }}" alt="" uk-cover>
                            <div class="uk-overlay-prima uk-position-cover"></div>
                            <div class="uk-position-bottom p-3">
                                <p class="mt-1 mb-0 influncer_name">Designer</p>
                                <p class="mt-1 mb-0 influencer_title">Twitter Influencer</p>
                            </div>
                    </div>
                    </a>
                </div>
                
                <div class="col-md-3 mb-3">
                    <a href="">
                    <div class="uk-cover-container influncer-grid-single">
                            <canvas width="280" height="250"></canvas>
                            <img src="{{ asset('mamamamama.jpg') }}" alt="" uk-cover>
                            <div class="uk-overlay-prima uk-position-cover"></div>
                            <div class="uk-position-bottom p-3">
                                <p class="mt-1 mb-0 influncer_name">Designer</p>
                                <p class="mt-1 mb-0 influencer_title">Twitter Influencer</p>
                            </div>
                    </div>
                    </a>
                </div>
                
                <div class="col-md-3 mb-3">
                    <a href="">
                    <div class="uk-cover-container influncer-grid-single">
                            <canvas width="280" height="250"></canvas>
                            <img src="{{ asset('mamamamama.jpg') }}" alt="" uk-cover>
                            <div class="uk-overlay-prima uk-position-cover"></div>
                            <div class="uk-position-bottom p-3">
                                <p class="mt-1 mb-0 influncer_name">Designer</p>
                                <p class="mt-1 mb-0 influencer_title">Twitter Influencer</p>
                            </div>
                    </div>
                    </a>
                </div>
                
                <div class="col-md-3 mb-3">
                    <a href="">
                    <div class="uk-cover-container influncer-grid-single">
                            <canvas width="280" height="250"></canvas>
                            <img src="{{ asset('mamamamama.jpg') }}" alt="" uk-cover>
                            <div class="uk-overlay-prima uk-position-cover"></div>
                            <div class="uk-position-bottom p-3">
                                <p class="mt-1 mb-0 influncer_name">Designer</p>
                                <p class="mt-1 mb-0 influencer_title">Twitter Influencer</p>
                            </div>
                    </div>
                    </a>
                </div>
                
            </div>

    </div>
</div>

<div class="uk-section bg-white p-4">
        <div class="uk-container">
            <div class="row justify-content-center">
                <div class="col-md-6 uk-text-center">
                    <h3 class="mb-0 sign-in-info">Start creating awesome content by promoting a product</h3>
                    <p><a href="{{ route('app:influencer:apply') }}" class="btn btn-info">GET STARTED</a></p>
                </div>
            </div>
        </div>
</div>

@endsection