@extends('layouts.dashboard')

@section('title', 'My Products' )

@section('content')

<div class="row border-botto">
        <div class="col-md-12">
            <h2 class="dash-heading">My Products</h2>
        </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-4">
                                    <form class="uk-search uk-search-default" style="width: 100%;" action="{{ route('app:dashboard:product_search') }}" method="POST">
                                        @csrf
                                            <span uk-search-icon></span>
                                            <input class="uk-search-input" type="search" placeholder="Search Products..." name="q" value="{{ request()->q }}">
                                        </form>
                            </div>
                            
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <ul class="list-unstyled m-1">
                                    <li class="d-inline"><a href="{{ route('app:ecommerce:product_list') }}" class="btn app-btn-info" target="_blank">CREATE PRODUCT</a></li>
                                <li class="d-inline"><a href="{{ route('app:dashboard:product_delete') }}" class="btn app-btn-danger" id="boxTriggerButton">DELETE PRODUCT</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="mb-2">

                        </div>
                        <div class="table-responsive">
                            <table class="table app-table">
                                <thead>
                                <tr>
                                    <th>
                                            <p><input class="form-check-input ml-1" type="checkbox" id="selectAll"></p>
                                               
                                    </th>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Total Sold</th>
                                    <th>Status</th>
                                    <th>Created On</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($products as $product)
                                @php
                                $photo = $product->photos->first();
                                $design_colors = json_decode(json_decode($product->design->colors));
                                $get_color = collect($design_colors)->random();
                                $__cat = $product->categories->first();
                                @endphp
                                <tr>
                                <td>
                                        <p><input class="form-check-input singleSelect ml-1" type="checkbox" value="{{ $product->id }}"></p>
                                </td>
                                <td><img src="{{ asset($photo->public_url) }}" width="70" style="background: {{ $get_color }};"></td>
                                <td >{{ ucfirst($product->name) }}</td>
                                <td>&#x20A6;{{ number_format($product->design->total_earning + $product->brandable->price, 2) }}</td>
                                <td>{{ $product->order_metric->count() }}</td>
                                <td>{{ $product->is_approved ? 'Approved' : 'Pending'}}</td>
                                <td>{{ \Carbon\Carbon::parse($product->created_at)->toDayDateTimeString() }}</td>
                                   <td>
                                        <ul class="uk-iconnav">
                                                <li>
                                                    @if($product->categories->count() > 0)
                                                    <a href="{{ route('app:ecommerce:single_product', ['category'=> $__cat->slug, 'param'=> $product->slug . '_' . $product->ref]) }}" uk-icon="icon: link" class="app-icon-link" target="_blank"></a>
                                                    @else
                                                    <a href="javascript:;" uk-icon="icon: link" class="app-icon-link"></a>
                                                    @endif
                                                </li>
                                            <li><a href="{{ route('app:dashboard:product_edit', ['id'=> $product->id ]) }}" uk-icon="icon: file-edit" class="app-icon-link"></a></li>
                                                <li><a href="" class="btn table-btn-info btn-sm">Promote</a></li>
                                        </ul>
                                        
                                   </td>
                                </tr>
                                @empty
                                <tr><td colspan="6" class="uk-text-center"> You do not have any product</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>

                        {{ $products->appends(['q' => request()->q])->links() }}
                    </div>
                </div>
            </div>
        </div>

@endsection