@extends('layouts.dashboard')

@section('title', 'My Orders' )

@section('content')

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-light">
                        My Orders
                    </div>

                    <div class="card-body">
                        <div class="mb-2">

                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Order Date</th>
                                    <th>Transaction Ref</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($orders as $order)
                                
                                <tr>
                                <td>{{ \Carbon\Carbon::parse($order->created_at)->toDayDateTimeString() }}</td>
                                    <td>{{ $order->order_ref }}</td>
                                    <td><span class="_delivery_status">{{ isset($order->order_shipping) ? $utils->set_shipping_status($order->order_shipping->status) : '' }} </span></td>
                                
                                   
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        {{ $orders->links() }}
                    </div>
                </div>
            </div>
        </div>

@endsection