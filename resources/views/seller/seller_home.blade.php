@extends('layouts.layout')

@section('title', 'Welcome To Seller Portal')

@section('content')

<div class="uk-cover-container uk-height-large">
        <img src="{{ asset('img/seller_lp_banner.png') }}" alt="" uk-cover>

        <div class="uk-section uk-position-center">
            <div class="row uk-justify-center">
                <div class="col-md-12">
                    
                    <div class="uk-text-center">
                    <div class="home-hero">
                        <b>MAKE MONEY ON SUVENIA</b> 
                        <p class="mt-1">Sell your Designs and Products to our Customers</p>
                    </div>
                    <div class="mt-4"> 
                    <a class="btn btn-info" style="color: #fff;">GET STARTED</a>
                    </div>
                    </div>

                </div>
            </div>
        </div>

</div>

@endsection