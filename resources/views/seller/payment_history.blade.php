@extends('layouts.dashboard')

@section('title', 'Payment History' )

@section('content')

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-light">
                        Payment History
                    </div>

                    <div class="card-body">
                        <div class="mb-2">

                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Transaction Ref</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($payments as $payment)
                                
                                <tr>
                                <td>{{ \Carbon\Carbon::parse($payment->created_at)->toDayDateTimeString() }}</td>
                                    <td>{{ $payment->reference }}</td>
                                    <td>&#8358;{{ $payment->amount }}</td>
                                    <td><span class="_delivery_status">{{ !$payment->status ? 'Pending' : 'Paid'}} </span></td>
                                
                                   
                                </tr>
                                @empty
                                <tr><td>You Have Not Made Any Payment</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>

                        {{ $payments->links() }}
                    </div>
                </div>
            </div>
        </div>

@endsection