@extends('layouts.dashboard')

@section('title', 'Profile Settings' )

@section('content')

<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-light">
                    Basic Settings
                </div>
                <div class="card-body p-5">
                    <div class="row mb-2 justify-content-left">
                    <div class="col-md-6 uk-text-left">
                        <div class="mb-3">
                        <img src="{{ !is_null($utils->get_attachment('User', Auth::user()->id)) ? asset($utils->get_attachment('User', 1)->url) : asset('user/default.png') }}" alt="" id="ProfilePicPreview" width="100">
                        </div>
                        <progress id="js-progressbar" class="uk-progress" value="0" max="100" hidden></progress>
                        <div class="js-upload" uk-form-custom data-url="{{route('app:dashboard:profile-settings_upload', ['id'=> Auth::user()->id])}}">
                                <input type="file" multiple>
                                <button class="btn btn-info" type="button" tabindex="-1">Choose File</button>
                        </div>
                    <button class="btn btn-danger btn-sm" type="button" tabindex="-1" id="deleteOldProfileImage" data-url="{{ route('app:dashboard:profile-settings_upload_delete', ['id'=> Auth::user()->id]) }}">Delete</button>
                    </div>
                    </div>

                    <div class="row justify-content-left mt-3">
                    <div class="col-md-6">
                            <form action="{{ route('app:dashboard:profile_settings_basic') }}" method="post" data-post>
                                    <div class="form-group username">
                                            <label for="username">Username</label>
                                    <input type="text" class="form-control"  placeholder="Username" name="username" value="{{ Auth::user()->username }}">
                                            <span class="help-block"></span>
                                    </div>
                                          <div class="form-group firstname">
                                              <label for="firstname">Firstname</label>
                                              <input type="text" class="form-control"  placeholder="firstname" name="firstname" value="{{ Auth::user()->firstname }}">
                                              <span class="help-block"></span>
                                          </div>
                                          <div class="form-group lastname">
                                                  <label for="lastname">Lastname</label>
                                                  <input type="text" class="form-control"  placeholder="Lastname" name="lastname" value="{{ Auth::user()->lastname }}">
                                                  <span class="help-block"></span>
                                          </div>
                                          <div class="form-group phone">
                                                  <label for="phone">Phone</label>
                                                  <input type="phone" class="form-control"  placeholder="phone" name="phone" value="{{ Auth::user()->phone }}">
                                                  <span class="help-block"></span>
                                          </div>
                                    
                                    <div class="form-group">
                                        <button class="btn btn-info">Save</button>
                                    </div>

                                  </form>
                    </div>
                    </div>

                </div>
            </div>
        </div>
</div>

<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-light">
                    Change Password 
                </div>
                <div class="card-body p-5">
                        <div class="row justify-content-left mt-3">
                                <div class="col-md-6">
                                    <form action="{{ route('app:dashboard:profile_settings_password') }}" method="post" data-post>
                                        @csrf
                                            <div class="form-group password">
                                                    <label for="password">New Password</label>
                                            <input type="text" class="form-control"  placeholder="password" name="password" value="">
                                                    <span class="help-block"></span>
                                            </div>
                                            <div class="form-group confirm_password">
                                                    <label for="confirm_password">Confirm Password</label>
                                            <input type="text" class="form-control"  placeholder="Confirm Password" name="confirm_password" value="">
                                                    <span class="help-block"></span>
                                            </div>
                                            <div class="form-group">
                                                    <button class="btn btn-info">Save</button>
                                            </div>
            
                                </form>
                        </div>
                </div>
                </div>
            </div>
        </div>
</div>
<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-light">
                    Bank Settings
                </div>
                <div class="card-body p-5">
                <div class="row justify-content-left">
                <div class="col-md-6">

                        <form action="{{ route('app:dashboard:profile_settings_bank') }}" method="post" data-post>
                                <div class="form-group bank_name">
                                        <label for="bank_name">Bank Name</label>
                                <input type="text" class="form-control"  placeholder="Bank Name" name="bank_name" value="{{ Auth::user()->bank_name }}">
                                        <span class="help-block"></span>
                                </div>
                                <div class="form-group bank_account">
                                        <label for="bank_account">Bank Account</label>
                                <input type="text" class="form-control"  placeholder="Bank Account" name="bank_account" value="{{ Auth::user()->bank_account }}">
                                        <span class="help-block"></span>
                                </div>
                                <div class="form-group bank_bvn">
                                        <label for="bank_bvn">Bank Bvn</label>
                                <input type="text" class="form-control"  placeholder="Bank Bvn" name="bank_bvn" value="{{ Auth::user()->bank_bvn }}">
                                        <span class="help-block"></span>
                                </div>
                                <div class="form-group bank">
                                        <label for="bank_bvn">Bank</label>
                                        <select class="uk-select" id="BankSelect" name="bank">
                                                @foreach($utils->fetchBanks() as $key => $value)
                                                <option value="{{ $key }}" bank-code="{{ $value }}" {{ Auth::user()->bank_sort == $key ? 'selected' : ''}}>{{ $value }}</option>
                                                @endforeach
                                                </select>
                                        <span class="help-block"></span>
                                </div>
                                
                                <div class="form-group">
                                        <button class="btn btn-info">Save</button>
                                </div>

                    </form>

                </div>
                </div>
                </div>
            </div>
        </div>
</div>

@endsection

@push('PAGE_STYLES')
@endpush

@push('PAGE_SCRIPTS')
   
    <script>
        
        $(document).ready(function(){
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });

        var bar = document.getElementById('js-progressbar');

        UIkit.upload('.js-upload', {

            url: $('.js-upload').attr('data-url'),
            //mime: true,
            //allow:"*.png,*.jpg",
            loadStart: function (e) {
                console.log('loadStart', arguments);

                bar.removeAttribute('hidden');
                bar.max = e.total;
                bar.value = e.loaded;
            },

            progress: function (e) {
                console.log('progress', arguments);

                bar.max = e.total;
                bar.value = e.loaded;
            },

            loadEnd: function (e) {
                console.log('loadEnd', arguments);

                bar.max = e.total;
                bar.value = e.loaded;
            },

            completeAll: function () {
                var ImgUrl = JSON.parse(arguments[0].response);
                $('#ProfilePicPreview').attr('src', ImgUrl.img);
                $('.generalProfilePic').attr('src', ImgUrl.img);
                setTimeout(function () {
                    bar.setAttribute('hidden', 'hidden');
                }, 1000);
            }

        });

        //ajax for profile update
        $('#deleteOldProfileImage').on('click', (e)=>{
            e.preventDefault();
            $.ajax({
                url: $(e.currentTarget).attr('data-url'),
                type: 'POST',
                beforeSend: (args)=>{

                }
            })
            .catch((err)=>{
                console.log(err);
            })
            .done((res)=>{
                $('#ProfilePicPreview').attr('src', res.img);
                $('.generalProfilePic').attr('src', res.img);
                toastr.info('Image Deleted successfully!')
            });

        });

        });
    </script>
@endpush