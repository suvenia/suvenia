@extends('Dashboard::layouts.store_edit')

@section('title', 'Edit Store' )

@php
    $store_meta = json_decode($store->meta, true);
    $set_default_branding = function($type)use($store_meta){
        if(isset($store_meta['default_brand']) && $type == $store_meta['default_brand']){
                return 'checked';
        }else{
            return '';
        }
    };

    $set_default_banner_type = function($type)use($store_meta){
        if(isset($store_meta['default_banner']) && $type == $store_meta['default_banner']){
                return 'checked';
        }else{
            return '';
        }
    }
@endphp

@section('content')

        <div class="row border-bottom">
        <div class="col-md-9">
            <h2 class="dash-heading">Edit Store</h2>
        </div>
        </div>

        <div class="row">
                <div class="col-md-12">
                   
                        <iframe src="{{ route('app:base:store_view', ['slug'=> $store->slug]) }}" style="width:100%;" id="StoreFrame" uk-height-viewport></iframe>
                </div> 
        </div>
          
            
<!-- EDIT CANVAS -->

<div id="offcanvas-store-header" uk-offcanvas="overlay: true">
        <div class="uk-offcanvas-bar">
            <form action="{{ route('app:dashboard:edit_store:store_header', ['id'=> $store->id]) }}" method="POST" store-post>
                @csrf
                <div class="row mb-3">
                    <div class="col-6 borde" style="position: relative;">
                        <button class="uk-offcanvas-close" type="button" uk-icon="icon: chevron-left; ratio:1.5"></button>
                    </div>
                    <div class="col-6 borde uk-text-right">
                        <button type="submit" class="btn app-btn-canva btn-sm">SAVE</button>
                    </div>
    
                </div>
    
                <div class="row">
                    <div class="col-md-12">
    
                        <div class="form-group name">
                            <label for="username" class="form-label">Store Name</label>
                            <input type="text" class="form-control" placeholder="Store Name" name="name" value="{{ $store->name }}">
                            <span class="help-block"></span>
                        </div>
    
                        <div class="form-group">
                            <label class="form-label">Store Logo</label>
                            <small class="d-block">use logo or storename as brand idenity</small>
                            <div class="mb-3">
                                <div class="uploadCont">
                                        <small class="d-block">recomended dimension is 200 pixels width and 60 pixels height</small>
                                    <div class="mb-3">
                                        <img src="{{ !is_null($store_meta['store_logo']) ? asset('store_img/' . $store_meta['store_logo']) : asset('store_img/store.png') }}" alt="" id="LogoPreview" width="200" height="60" class="border">
                                        <textarea name="store_logo" style="display: none;">{{ $store_meta['store_logo'] }}</textarea>
                                        <textarea name="store_logo_temp" id="LogoTempInput" style="display: none;"></textarea>
                                    </div>
                                    <progress id="js-progressbar" class="uk-progress" value="0" max="100" hidden></progress>
                                    <div class="js-upload" uk-form-custom data-url="{{route('app:dashboard:edit_store_upload')}}" preview="#LogoPreview" input="#LogoTempInput" progress="js-progressbar" cropable>
                                        <input type="file">
                                        <button class="btn btn-info" type="button" tabindex="-1">Choose File</button>
                                    </div>
                                    <button type="button" class="btn btn-danger btn-sm ml-2 app-hide" id="CropImage">Crop</button>
                                </div>
                                <div class="textLogoCont">
                                    <div class="form-group">
                                    <label class="form-label">Store Logo Text</label>
                                    <input type="text" class="form-control mt-2" name="store_logo_text" value="{{ $store_meta['store_logo_text'] }}">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <small>Logo Text Color</small>
                                            <input type='text' class="colorpickers" data-target="#LogoTextColorInp" />
                                            <input type="hidden" id="LogoTextColorInp" name="store_logo_color" value="{{ $store_meta['store_logo_color'] }}">
                                        </div>
                                        <div class="col-md-6">
                                                <small>Select Font</small>
                                                <select name="store_logo_font" class="form-control">
                                                    @foreach(config('dashboard_config.fonts') as $key=>$value)
                                                <option value="{{ $key }}">{{ $key }}</option>
                                                    @endforeach
                                                </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="" app-switcher>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" name="default_brand" class="custom-control-input" show-content=".uploadCont" hide-content=".textLogoCont" id="customRadioInline1" value="IMAGE" {{ $set_default_branding( 'IMAGE') }}>
                                    <label class="custom-control-label" for="customRadioInline1">Use Image</label>
                                </div>
    
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" name="default_brand" class="custom-control-input" hide-content=".uploadCont" show-content=".textLogoCont" id="customRadioInline2" value="TEXT" {{ $set_default_branding( 'TEXT') }}>
                                    <label class="custom-control-label" for="customRadioInline2">Use Text</label>
                                </div>
    
                            </div>
                        </div>
    
                        <div class="form-group">
                            <label class="form-label">Store Text Color</label>
                            <small class="d-block">choose your store text color</small>
                            <div class="">
                                <input type='text' class="colorpickers" data-target="#LogoBackgroundInp" />
                                <input type="hidden" id="LogoBackgroundInp" name="store_theme_color" value="{{ $store_meta['store_theme_color'] }}">
                            </div>
                        </div>
    
                        <div class="form-group">
                            <label for="username" class="form-label">Store Theme</label>
                            <small class="d-block">Choose your header background color throughout your store</small>
                            <div class="">
                                    <input type='text' class="colorpickers" data-target="#StoreThemeInp" />
                                <input type='hidden' id="StoreThemeInp" name="store_theme" value="{{ $store_meta['store_theme'] }}">
                            </div>
                        </div>
    
                    </div>
                </div>
            </form>
        </div>
</div> 

<div id="offcanvas-store-banner" uk-offcanvas="overlay: true">
        <div class="uk-offcanvas-bar">
            <form action="{{ route('app:dashboard:edit_store:store_banner', ['id'=> $store->id]) }}" method="POST" store-post>
                @csrf
                <div class="row mb-3">
                    <div class="col-6 borde" style="position: relative;">
                        <button class="uk-offcanvas-close" type="button" uk-icon="icon: chevron-left; ratio:1.5"></button>
                    </div>
                    <div class="col-6 borde uk-text-right">
                        <button type="submit" class="btn app-btn-canva btn-sm">SAVE</button>
                    </div>
    
                </div>
    
                <div class="row">
                    <div class="col-md-12">
    
    
                        <div class="form-group">
                            <label class="form-label">Store Banner</label>
                            <small class="d-block">Recomended dimension: 1000 pixels by 400 pixels, Max Size 2MB</small>
                          
                            <div class="mb-3" app-switcher>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" name="default_banner" class="custom-control-input" show-content=".uploadBack" hide-content=".systemImg" id="customBannerRadio1" value="USER" {{ $set_default_banner_type('USER') }}>
                                    <label class="custom-control-label" for="customBannerRadio1">Upload</label>
                                </div>
    
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" name="default_banner" class="custom-control-input" hide-content=".uploadBack" show-content=".systemImg" id="customBannerRadio2" value="SYSTEM" {{ $set_default_banner_type('SYSTEM') }}>
                                    <label class="custom-control-label" for="customBannerRadio2">Use System</label>
                                </div>
    
                            </div>

                            <div class="mb-3">
                                    <div class="uploadBack">
                                        <div class="mb-3">
                                            <img src="{{ !is_null($store_meta['store_upload_banner']) ? asset('store_img/' . $store_meta['store_upload_banner']) : asset('user/default.png') }}" alt="" id="BannerPreview" width="100">
                                            <textarea name="store_banner_temp" id="BannerTempInput" style="display: none;"></textarea>
                                        </div>
                                        <progress id="js-progressbar-banner" class="uk-progress" value="0" max="100" hidden></progress>
                                        <div class="js-upload" uk-form-custom data-url="{{route('app:dashboard:edit_store_upload')}}" preview="#BannerPreview" input="#BannerTempInput" progress="js-progressbar-banner">
                                            <input type="file" multiple>
                                            <button class="btn btn-info" type="button" tabindex="-1">Choose File</button>
                                        </div>
                                    </div>
                                    <div class="systemImg">
                                            @php
                                            $banner_dir = public_path('/') . 'store_img/system_banner/';
                                            $defaultBanners = [];
                                            foreach(glob($banner_dir .'*') as $filename){
                                                $defaultBanners[] = basename($filename);
                                            }
                                            @endphp
                                            <div class="row">
                                                    @foreach($defaultBanners as $banner)
                                                    <div class="col-md-6 mb-2">
                                                    <label class="image_radio">
                                                    @php $isBannerSelected = $banner == basename($store_meta['store_banner_system']) ? 'checked' : ''; @endphp
                                                    <input type="radio" name="system_store_banner" class="single_select_checkbox" value="{{ 'store_img/system_banner/' . $banner }}" {{ $isBannerSelected }} />
                                                    <img src="{{ asset('store_img/system_banner/' . $banner) }}" width="400" uk-responsive>
                                                    </label>
                                                    </div>
                                                    @endforeach
                                            </div>
                                    </div>
                                </div>

                        </div>
    
    
                    </div>
                </div>
            </form>
        </div>
</div>

<div id="offcanvas-store-products" uk-offcanvas="overlay: true">
        <div class="uk-offcanvas-bar">
            <form action="{{ route('app:dashboard:edit_store:store_products', ['id'=> $store->id]) }}" method="POST" store-post>
                @csrf
                <div class="row mb-3">
                    <div class="col-6 borde" style="position: relative;">
                        <button class="uk-offcanvas-close" type="button" uk-icon="icon: chevron-left; ratio:1.5"></button>
                    </div>
                    <div class="col-6 borde uk-text-right">
                        <button type="submit" class="btn app-btn-canva btn-sm">SAVE</button>
                    </div>
    
                </div>
    
                <div class="row">
                    <div class="col-md-12">
    
    
                        <div class="form-group">
                            <label class="form-label">Store Banner</label>
                            <small class="d-block">Recomended dimension: 1000 pixels by 400 pixels, Max Size 2MB</small>
                          

                           <div class="row p-2 mt-2" style="background: #fff;">
                                    @foreach($products as $product)
                                    @php
                                    $photo = $product->photos->first();
                                    $design_colors = json_decode(json_decode($product->design->colors));
                                    $get_color = collect($design_colors)->random();
                                    @endphp
                                    <div class="col-md-6 mb-2">
                                    <label class="image_radio">
                                    @php $isProductSelected = in_array($product->id, $store_meta['products']) ? 'checked' : ''; @endphp
                                    <input type="checkbox" name="store_products[]" class="" value="{{ $product->id }}" {{ $isProductSelected }} />
                                    <img src="{{ asset($photo->public_url) }}" width="100" uk-responsive style="background: {{ $get_color }};">
                                    </label>
                                    </div>
                                    @endforeach
                            </div>
                        </div>
    
    
                    </div>
                </div>
            </form>
        </div>
</div>

<div id="offcanvas-store-about" uk-offcanvas="overlay: true">
        <div class="uk-offcanvas-bar">
            <form action="{{ route('app:dashboard:edit_store:store_about', ['id'=> $store->id]) }}" method="POST" store-post>
                @csrf
                <div class="row mb-3">
                    <div class="col-6 borde" style="position: relative;">
                        <button class="uk-offcanvas-close" type="button" uk-icon="icon: chevron-left; ratio:1.5"></button>
                    </div>
                    <div class="col-6 borde uk-text-right">
                        <button type="submit" class="btn app-btn-canva btn-sm">SAVE</button>
                    </div>
    
                </div>
    
                <div class="row">
                    <div class="col-md-12">
                            <div class="form-group description">
                            <label class="form-label">Store Description</label>
                            <textarea class="form-control" placeholder="Store Description" rows="5" name="description">{{ $store->description }}</textarea>
                            <span class="help-block"></span>
                            </div>
                            <div class="form-group website">
                            <label class="form-label">Your Website</label>
                            <input type="url" class="form-control" name="website" placeholder="https://website.com" value="{{ $store->website }}">
                            <span class="help-block"></span>
                            </div>
                            <div class="form-group facebook">
                            <label class="form-label">Facebook Link</label>
                            <input type="url" name="facebook" class="form-control" placeholder="https://facebook.com/myHandle" value="{{ $store->facebook}}">
                            <span class="help-block"></span>
                            </div>
                            <div class="form-group twitter">
                            <label class="form-label">Twitter Link</label>
                            <input type="url" name="twitter" class="form-control" placeholder="https://twitter.com/myHandle" value="{{ $store->twitter}}">
                            <span class="help-block"></span>
                            </div>
                            <div class="form-group instagram">
                            <label class="form-label">Instagram Link</label>
                            <input type="url" name="instagram" class="form-control" placeholder="https://instagram.com/myHandle" value="{{ $store->instagram}}">
                            <span class="help-block"></span>
                            </div>

                    </div>
                </div>
            </form>
        </div>
</div>

@endsection