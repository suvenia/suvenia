@extends('layouts.dashboard')

@section('title', 'Withdrawal History' )

@section('content')

<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-light">
                   Request Withdrawal
                </div>

                <div class="card-body">
                        <form action="{{ route('app:dashboard:withdraw') }}" method="POST" data-post>
                            @csrf
                                <div class="form-group amount">
                                        <label for="amount">Amount</label>
                                <input type="number" class="form-control"  placeholder="Amount" name="amount" value="">
                                        <span class="help-block"></span>
                                </div>
                                
                                <div class="form-group">
                                        <button class="btn btn-info">Submit</button>
                                </div>

                    </form>
                </div>
            </div>
        </div>
</div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-light">
                        Payment History
                    </div>

                    <div class="card-body">
                        <div class="mb-2">

                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Transaction Ref</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($withdrawals as $withdrawal)
                                
                                <tr>
                                <td>{{ \Carbon\Carbon::parse($withdrawal->created_at)->toDayDateTimeString() }}</td>
                                    <td>{{ $withdrawal->ref }}</td>
                                    <td>&#8358;{{ $withdrawal->amount }}</td>
                                    <td><span class="_delivery_status">{{ !$withdrawal->status ? 'Pending' : 'Paid'}} </span></td>
                                
                                   
                                </tr>
                                @empty
                                <tr><td>You Have Not Made Any Withdrawal</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>

                        {{ $withdrawals->links() }}
                    </div>
                </div>
            </div>
        </div>

@endsection