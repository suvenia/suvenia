@extends('layouts.dashboard')

@section('title', 'Edit Product' )

@section('content')

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-light">
                        Edit Product
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="mb-2">
                                        <sub>Total Price</sub>
                                    <h1 class="m-0">&#8358;<span class="price-pane-app">{{ $product->design->total_earning + $product->brandable->price }}</span><h1>
                                </div>
                                <form action="{{ route('app:dashboard:product_edit', ['id'=> $product->id]) }}" method="POST" data-post>
                                    @csrf
                                <div class="form-group name">
                                <label>Your Earning</label>
                                <input type="number" class="form-control price-pane-trigger" placeholder="Product Name" name="earning" value="{{ $product->design->total_earning }}" data-price="{{ $product->design->total_earning + $product->brandable->price }}">
                                </div>

                                <div class="form-group name">
                                <label>Product Name</label>
                                <input type="text" class="form-control" placeholder="Product Name" name="name" value="{{ $product->name }}">
                                </div>

                                <div class="form-group description">
                                <label>Product Description</label>
                                <textarea class="form-control" name="description" placeholder="Description">{{ $product->description }}</textarea>
                                </div>

                                <div class="form-group">
                                <button type="submit" class="btn btn-info">Save</button>
                                </div>

                                </form>
                            </div>
                            <div class="col-md-5">
                                @foreach($product->photos as $photo)
                                <img src="{{ asset($photo->public_url) }}" width="300" class="mb-3">
                                @endforeach
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

@push('PAGE_SCRIPTS')
<script>
    $(document).ready(function(){
        $('.price-pane-trigger').on('keyup', function(e){
            $('.price-pane-app').text(parseInt($(e.currentTarget).val()) + parseInt($(e.currentTarget).attr('data-price')));
        });
    });
</script>
@endpush