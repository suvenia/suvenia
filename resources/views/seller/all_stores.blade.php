@extends('layouts.dashboard')

@section('title', 'All Stores' )

@push('PAGE_STYLES')
<style>
.text-store-product{
    font-size: 16px;
  font-weight: 600;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #000000;
  text-transform: capitalize;
}
</style>
@endpush

@section('content')

        <div class="row border-bottom">
        <div class="col-md-9">
            <h2 class="dash-heading">My Stores</h2>
        </div>
        <div class="col-md-3 pb-2">
            <button class="btn app-btn-info" uk-toggle="target: #modal-close-default">CREATE NEW STORE</button>
        </div>

        </div> 

        <div class="row mt-3">
            @forelse($stores as $store)
            @php
            $store_meta = json_decode($store->meta, true);
          // print_r($store_meta);
            @endphp
            <div class="col-md-3 uk-text-center">
                <div  class="card uk-inline-clip uk-transition-toggle" tabindex="0">
                    <div class="card-body" style="height: 200px;">
                        @if($store_meta['default_brand'] == 'IMAGE')
                        <img src="{{ !is_null($store_meta['store_logo']) ? asset('store_img/' . $store_meta['store_logo']) : asset('store_img/store.png') }}"  uk-responsive width="150" class="uk-position-center">
                        @endif
                        @if($store_meta['default_brand'] == 'TEXT')
                            <h3 class="uk-position-center">{{ empty($store_meta['store_logo_text']) ? 'LOGO HERE' : $store_meta['store_logo_text'] }}</h3>
                        @endif
                    </div>
                    <div class="uk-transition-slide-bottom uk-position-cover  uk-overlay uk-overlay-primary">
                            <div class="mr-3 mt-2 uk-position-top-right"><a href="{{ route('app:dashboard:edit_store', ['id'=> $store->id ]) }}" class="" style="color: #fff; font-size: 13px;"><span class="mr-1" uk-icon="file-edit" style="font-size:11px;"></span> Edit</a></div>
                            <div class="uk-position-center"><a href="{{ route('app:base:store_view', ['slug'=> $store->slug]) }}" class="btn btn-outline-light btn-radius" target="_blank">Visit Store</a></div>
                    </div>
                </div>
                <p class="text-store-product m-1">{{ $store->name }}</p>
            </div>
            @empty
            <div class="col-md-12 uk-text-center">
                <p>You do not have any stores</p>
            </div>
            @endforelse

        </div>


<div id="modal-close-default" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <h4 class="uk-modal-title" style="font-size: 20px; font-weight: 600;">Create Store</h4>
            <div class="">
                <form action="{{ route('app:dashboard:create_store') }}" method="POST" data-post>
                    @csrf
                    <div class="form-group name">
                    <label for="">Enter Your Store Name</label>
                    <input type="name" class="form-control"  placeholder="Store Name" name="name">
                    <p class="help-block m-0"></p>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-app-info">CREATE STORE</button>
                    </div>
                </form>
            </div>
            </div>
</div>
            
@endsection