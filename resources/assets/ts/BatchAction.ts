class BatchAction{

    childrenCheckBox: any;

    public constructor(){
        this.triggerCheckBoxes();
        this.childrenCheckBox = '[data-children-checkbox]';
        this.submitSelected();
    }

    public triggerCheckBoxes(){
        let cls = this;
        $('[data-master-checkbox]').on('change', function(e){
                if($(e.currentTarget).is(':checked')){
                    $(cls.childrenCheckBox).prop('checked', true);
                }else{
                    $(cls.childrenCheckBox).prop('checked', false);
                }
        });
    }

    public submitSelected(){
        let cls = this;
        $(document).on('click', '[data-checkbox-trigger]',  (e)=>{
            e.preventDefault();
            var data: any = [];
            $(cls.childrenCheckBox+':checked').each(function(){
                data.push($(this).val());
            });
            let form = cls.setForm($(e.currentTarget).attr('href'), data);
            $(e.currentTarget).append(form.join(" "));
            $('#BatchDeleteForm').submit();
        });
    }

    public setForm(href: any, data: any){
        var csrf = $('meta[name="csrf-token"]').attr('content');
        var template = [
            '<form id="BatchDeleteForm" action="'+href+'" method="POST" style="display: none;">',
            '<input type="hidden" name="_token" value="'+csrf+'">',
            '<textarea name="deleteIds">'+JSON.stringify(data)+'</textarea>',
            '</form>'
        ];

        return template;
    }
}

const __batchAction = new BatchAction();