
class AppWidePage{

    public constructor(){
        this.set_ajax();
        this.set_search();
    }

    public set_ajax(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }); 
    } 

    public set_search(){
        $('#appWideSearchInp').on('keyup', (e)=>{
            let url = $(e.target).attr('data-url');
            $.ajax({
                url:  url,
                type: 'POST',
                data: {q: $(e.target).val()},
                beforeSend: function(){
                   
                },
                timeout: 20000
            })
            .done(function(data, textStatus, jqXHR){
                if(jqXHR.status == 200){
                    $('#appWideSearchContainer').html(data);
                }
            });
            
        });
    }
 

}

$(function(){ 
    const ____init_appWide_page = new AppWidePage();
}); 