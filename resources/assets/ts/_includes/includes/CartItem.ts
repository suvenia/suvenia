export class CartItem{

    public _delete_cart_item(): any{
        $('.cart_delete_order_link').each(function(){
            $(this).on('click', function(){
                $('body').prepend(
                    '<div id="__delete_cart_modal" uk-modal><div class="uk-modal-dialog uk-modal-body uk-text-center">'+
                    '<button class="uk-modal-close-default" type="button" uk-close></button>'+
                    '<h3 class="uk-modal-title mb-2 border-bottom" style="color: #000; font-size: 24px; font-weight: 600;">DELETE ITEM</h2>'+
                    '<p class="m-1" style="color: #333; font-size: 17px;">Do you really want to delete this Item?</p>'+
                    '<button class="btn btn-info mr-2 uk-modal-close" href="">NO</button>'+
                    '<a class="btn btn-info cart-item-delete-link" href="'+$(this).attr('data-url')+'">YES</a>'+
                    '</div></div>'
                );
    
                UIkit.modal("#__delete_cart_modal").show();
            });

        });
    }

    public __initiate_delete(){
        $(document).on('click', '.cart-item-delete-link', function(e){
           e.preventDefault();
          $.post($(e.currentTarget).attr('href'));
          window.location.reload();
       });
   }

}