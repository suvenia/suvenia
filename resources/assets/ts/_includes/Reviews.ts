
class ReviewPage{

    public constructor(){
        this.set_ajax();
        this.appReviewProduct();
        this.setRating();
    }

    public set_ajax(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }); 
    }

    public setRating(){
        $('.bar-rated').barrating({
            theme: 'css-stars',
            readonly: true,
            allowEmpty: true,
            initialRating:0,
        });
        
        $('#bar-rating').barrating({
            theme: 'css-stars',
            allowEmpty: true,
            initialRating:null,
            showSelectedRating: true,
            deselectable: true
        });
        //$('#bar-rating').barrating('clear');
    }
    
    public appReviewProduct(){
        $(document).on('click', '.appReviewProduct', (e)=>{
            e.preventDefault();
            let url: any = $(e.currentTarget).attr('href');
            $.ajax({
                url:  url,
                type: 'GET',
                data: {},
                beforeSend: function(){
                    $('body').LoadingOverlay("text", "Please Wait");
                },
                timeout: 20000
            })
            .done(function(data, textStatus, jqXHR){
                if(jqXHR.status == 200){
                    //console.log(data);
                    
                    $('body').prepend(
                        '<div id="__review_modal" uk-modal><div class="uk-modal-dialog uk-modal-body">'+
                        '<button class="uk-modal-close-default" type="button" uk-close></button>'+
                        '<h3 class="uk-modal-title mb-2 border-bottom" style="color: #000; font-size: 18px; font-weight: 600;">Write Your Review</h2>'+
                        '<p class="m-1" style="color: #333; font-size: 17px;">'+data+'</p>'+
                        '</div></div>'
                    );
                    UIkit.modal("#__review_modal").show();
                }
            });

        });
    }
 

}

$(function(){ 
    const ____init_savedItem_page = new ReviewPage();
}); 