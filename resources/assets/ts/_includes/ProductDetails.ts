 
 class ProductDetails{

    order_data: any = {
        size: "",
        qty: 1,
        id: 0,
        color: "",
        has_size: true,
    };

    buy_direct_url : any = $("#__buyUrl").val();

    public constructor(){ 
        this.set_ID();
        this.set_ajax();
        this.setRating();
        this.chooseColor();
        this.chooseSize();
        this.setQuantity();
        this.direct_buy();
        this.add_to_cart();
        this.favoriteProduct();
        this.setFaq();
    }

    public set_ID(){
        this.order_data.id = $('#ProductId').val();
        this.order_data.has_size = $('#hasSize').val();
    }

    public set_ajax(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

    public setRating(){
        $('.bar-rated').barrating({
            theme: 'css-stars',
            readonly: true,
            allowEmpty: true,
            initialRating:0
        });
        
        $('#bar-rating').barrating({
            theme: 'css-stars',
            allowEmpty: true,
            initialRating:null,
            showSelectedRating: true,
            deselectable: true
        });
        //$('#bar-rating').barrating('clear');
    }

    public chooseColor(){
        var colorInput = $('.color-box-input');
        var cls = this;
        colorInput.each(function(ind, val){
            var eachBox = this;
            $(eachBox).on('click', function(){
                //console.log($(eachBox).val());
                if($(eachBox).is(':checked')){
                    $('.color-box').removeClass('color-active');
                    $(eachBox).parent().addClass('color-active');
                    cls.order_data.color = $(eachBox).val();
                    $('.productImgColor').css('background', <any>$(eachBox).val());
                }
            });
        });
    }

    public chooseSize(){
        var sizeInput = $('.size-box-input');
        var cls = this;
        sizeInput.each(function(ind, val){
            var eachBox = this;
            $(eachBox).on('click', function(){
                if($(eachBox).is(':checked')){
                    $('.size-box').removeClass('active');
                    $(eachBox).parent().addClass('active');
                    cls.order_data.size = $(eachBox).val();
                }
            });
        });
    }

    public setQuantity(){
        var cls = this;
        var qtyInput = $('#numberInputSpiner');
        var currentVal = <any>qtyInput.val();
        var plus = $('#numberAdd');
        var minus = $('#numberMinus');
        qtyInput.on('change', function(e){
            let currentVal: any = $(e.currentTarget).val();
            cls.order_data.qty = parseInt(currentVal);
            if(currentVal.val() < 1){
                $(e.currentTarget).val(1);
                currentVal = parseInt(<any>$(this).val());
                cls.order_data.qty = currentVal;
            }
        });

        plus.on('click', function(){
            qtyInput.val(++ currentVal);
            cls.order_data.qty = parseInt(currentVal);
        });

        minus.on('click', function(){
            if(currentVal > 1){
                qtyInput.val(-- currentVal);
                cls.order_data.qty = parseInt(currentVal);
            }
        });

    }

    public __validate(){
        var cls = this;
        var ___data = cls.order_data;
        var status = true;
        if(cls.order_data.has_size){
            if(___data.size == null || ___data.size == ""){
                toastr.error('You must select a size before proceeding!');
                status = false;
            }
            
        }
    
        if(___data.color == null || ___data.color == ""){
            toastr.error('You must select a color before proceeding!');
                status = false;
        }
       
        return status;
    }


    public direct_buy(){
        var cls = this;
        $('#__buy_direct').on('click', function(e){
            e.preventDefault();
            if(cls.__validate()){
                window.location.href = cls.buy_direct_url + "/" + cls.b64EncodeUnicode(JSON.stringify(cls.order_data));
            }
            
        });
    }

    public add_to_cart(){
        var cls = this;
        $('#__add_to_cart').on('click', function(e){
            e.preventDefault();
            if(cls.__validate()){
                //window.location.href = $(this).attr('data-url');
                $.ajax({
                    url: $(this).attr('data-url'),
                    type: 'POST',
                    data: {item: cls.b64EncodeUnicode(JSON.stringify(cls.order_data))},
                    timeout: 20000
                }) 
                .done(function(data, textStatus, jqXHR){
                    if(jqXHR.status == 200){
                        $('#__data_cart_counter').empty();
                        $('#__data_cart_counter').text(data.cart_count);
                        $('body').prepend(
                            '<div id="__cart_modal" uk-modal><div class="uk-modal-dialog uk-modal-body">'+
                            '<button class="uk-modal-close-default" type="button" uk-close></button>'+
                            '<h3 class="uk-modal-title mb-2" style="color: #000; font-size: 24px; font-weight: 600;">Success!</h2>'+
                            '<p class="m-1" style="color: #333; font-size: 17px;">'+data.message+'</p>'+
                            '<button class="btn btn-white mr-2 uk-modal-close" href="">CONTINUE SHOPPING</button>'+
                            '<a class="btn btn-info" href="'+data.redirect_url+'">VIEW CART AND CHECKOUT</a>'+
                            '</div></div>'
                        );
                        //alert(data.message)
                        //uikit.modal.confirm(data.message);
                        UIkit.modal("#__cart_modal").show();
                    } 
                });
            }
        });
    }

    private b64EncodeUnicode(str: any) {
        // first we use encodeURIComponent to get percent-encoded UTF-8,
        // then we convert the percent encodings into raw bytes which
        // can be fed into btoa.
        return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
            function toSolidBytes(match, p1: any) {
                return String.fromCharCode(<any>'0x' + p1);
        }));
    }

    public favoriteProduct():void{
        let sendAjax = (url:any, id:any, message: any, callback: any)=>{
                    $.ajax({
                        url:  url,
                        type: 'POST',
                        data: {id:id},
                        beforeSend: function(){
                        
                        },
                        timeout: 20000
                    })
                    .done(()=>{
                        callback;
                        toastr.info(message);
                        window.location.reload();
                    });
        };

        $('#productFavorite').on('click', (e)=>{
            let url = $(e.currentTarget).attr('data-url');
            let id = $(e.currentTarget).attr('data-id');
            if($(e.currentTarget).hasClass('active')){
                
                sendAjax(url, id, 'Product removed from your saved items successfully!', $(e.currentTarget).removeClass('active'));
            }else{
                sendAjax(url, id, 'Product added to your saved item successfully!', $(e.currentTarget).addClass('active'));
                
            }
        });
    }

    public setFaq(){
        $(window).on('load', (e)=>{
            $('#FaqResponse').html('<div class="bg-light mt-2 mb-2 p-3">'+ $('#FaqInputTrigger').val() +'</div>');
        });
      
        $('#FaqInputTrigger').on('change', (e)=>{
            let Cont: any = $(e.currentTarget).val();
            $('#FaqResponse').html('<div class="bg-light mt-2 mb-2 p-3">'+ Cont+ '</div>');
        });
    }
    
}
$(function(){ 
    const ____init_product_page = new ProductDetails();
}); 
 
