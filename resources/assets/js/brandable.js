require("../ts/Post");

$(function(){
    
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
    
    $('#createUseravatar').on('fileuploaded', function(event, data, previewId, index) {
        var response = data.response;
        $('#file_id').val(response.file_id);
    });
    
    
    //Brandables
    if($('#CustomizerEditPage').length > 0){
        var product_data_front_edit = JSON.parse($(':input[name="front_dimension"]').val());
        var product_data_back_edit = JSON.parse($(':input[name="back_dimension"]').val());
        console.log(product_data_front_edit);
        $('.DesignAreaContainer > .designArea').css({
            left: product_data_front_edit.left,
            top: product_data_front_edit.top,
            width: product_data_front_edit.width,
            height: product_data_front_edit.height,
        });
        $('#frontImagePreview').attr('src', product_data_front_edit.image_url);
        
        $('.DesignAreaContainerBack > .designAreaBack').css({
            left: product_data_back_edit.left,
            top: product_data_back_edit.top,
            width: product_data_back_edit.width,
            height: product_data_back_edit.height,
        });
        
        $('#backImagePreview').attr('src', product_data_back_edit.image_url);
    }
    
    var DA = $( ".designArea" );
    
var product_data_front = {width: DA.outerWidth(), height: DA.outerHeight(), left: DA.position().left, top: DA.position().top};
 var product_data_back = {width: DA.outerWidth(), height: DA.outerHeight(), left: DA.position().left, top: DA.position().top};
    
  
        
        $('.designArea').draggable({
        containment: ".DesignAreaContainer", 
        scroll: false,
        stop: function(event,ui){
            console.log($(this).parent());
            var offset = $(this).position();
            var xPos = offset.left;
            var yPos = offset.top;
            product_data_front['left'] = xPos - 16;
            product_data_front['top'] = yPos - 1;
           str('#FrontDesignAreaDim', product_data_front); 
        },
        
    }).resizable({
        stop : function(event,ui) {
        var endW = $(this).outerWidth();
        var endH = $(this).outerHeight();
        product_data_front['width'] = endW;
        product_data_front['height'] = endH;
           str('#FrontDesignAreaDim', product_data_front); 
        }
    });
        
    
        
        $('.designAreaBack').draggable({
        containment: ".DesignAreaContainerBack", 
        scroll: false,
        stop: function(event,ui){
            var offset = $(this).position();
            var xPos = offset.left;
            var yPos = offset.top;
            //console.log($(this).parent());
            product_data_back['left'] = xPos - 16;
            product_data_back['top'] = yPos - 1;
           str('#BackDesignAreaDim', product_data_back); 
        },
        
    }).resizable({
        stop : function(event,ui) {
        var endW = $(this).outerWidth();
        var endH = $(this).outerHeight();
        product_data_back['width'] = endW;
        product_data_back['height'] = endH;
            str('#BackDesignAreaDim', product_data_back); 
        }
    });
        
 
    $('#BackViewTrigger').on('change', function(){
        if($(this).is(':checked')){
            $('#BackView').find('span').removeClass('overlay');
            $('#BackView').find('input').prop('disabled', false);
        }else{
             $('#BackView').find('span').addClass('overlay');
            $('#BackView').find('input').prop('disabled', true);
        }
    });
    
    var str = function(input, data){
       var data =  JSON.stringify(data);
        $(input).val(data);
    }
    
        function readImage(inputElement) {
        var deferred = $.Deferred();

        var files = inputElement.get(0).files;
        if (files && files[0]) {
        var fr= new FileReader();
        fr.onload = function(e) {
            deferred.resolve(e.target.result);
        };
        fr.readAsDataURL( files[0] );
        } else {
        deferred.resolve(undefined);
        }

        return deferred.promise();
        }
    
    
    $('.file-upload').each(function(){
        $(this).on('change',function(){
            var $imgTr = $(this);
        readImage($(this)).done(function(base64Data){
            var target = $imgTr.attr('data-target');
            var input = $imgTr.attr('data-input');
            $(target).attr('src', base64Data);
            $(input).val(base64Data);
        });
    });
    });
    
    
     
});