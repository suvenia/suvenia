<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoOrderShippingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_order_shipping", function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id");
            $table->integer("order_id");
            $table->integer("address_id");
            $table->string("price");
            $table->boolean("status");
            $table->timestamps();
            
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_order_shipping");
    }
}

