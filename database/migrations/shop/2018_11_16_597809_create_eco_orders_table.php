<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_orders", function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id");
            $table->integer("payment_id");
            $table->string("order_ref");
            $table->string("price");
            $table->boolean("is_invalid")->default(false);
            $table->timestamps();
            
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_orders");
    }
}

