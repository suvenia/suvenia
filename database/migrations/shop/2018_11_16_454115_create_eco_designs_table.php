<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoDesignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_designs", function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id");
            $table->integer("product_id");
            $table->integer("brandable_id");
            $table->string("name")->nullable();
            $table->string("earnings")->nullable();
            $table->string("target_quantity")->nullable();
            $table->string("total_earning")->nullable();
            $table->string("percentage")->nullable();
            $table->longText("raw_design")->nullable();
            $table->timestamps();
            
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_designs");
    }
}

