<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoAnalyticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('eco_analytics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('client_id')->nullable();
            $table->integer('model_id')->nullable();
            $table->integer('category')->nullable();
            $table->string('device');
            $table->string('platform');
            $table->string('browser');
            $table->boolean('is_mobile');
            $table->string('lang');
            $table->longText('country')->nullable();
            $table->string('path');
            $table->string('page_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eco_analytics');
    }
}
