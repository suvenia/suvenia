<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_products", function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id");
            $table->integer("brandable_id")->nullable();
            $table->integer("design_id")->nullable();
            $table->integer("coupon_id")->nullable();
            $table->string("name")->nullable();
            $table->string("slug")->nullable();
            $table->string("ref")->nullable();
            $table->longText("description")->nullable();
            $table->integer("price")->nullable();
            $table->integer("sku")->nullable();
            $table->integer("quantity")->nullable();
            $table->boolean("is_archived")->default(false);
            $table->boolean("is_published")->default(true);
            $table->boolean("is_approved")->default(false);
            $table->timestamp("approval_date")->nullable();
            $table->timestamps();
            
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_products");
    }
}

