<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoFaqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_faqs", function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id")->nullable();
            $table->integer("product_id");
            $table->longText("question")->nullable();
            $table->longText("response")->nullable();
            $table->timestamps();
            
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_faqs");
    }
}

