<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoBrandablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_brandables", function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id");
            $table->string("name")->nullable();
            $table->longText("description")->nullable();
            $table->integer("price")->nullable();
            $table->longText("data")->nullable();
            $table->longText("front_image")->nullable();
            $table->longText("back_image")->nullable();
            $table->timestamps();
            
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_brandables");
    }
}

