<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoArtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eco_arts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('ref');
            $table->string('parent_ref')->nullable();
            $table->boolean('is_parent')->nullable();
            $table->longText('photo_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eco_arts');
    }
}
