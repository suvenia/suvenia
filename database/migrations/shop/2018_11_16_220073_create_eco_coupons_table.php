<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_coupons", function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id");
            $table->string("name")->nullable();
            $table->string("percentage");
            $table->integer("amount");
            $table->boolean("is_closed")->default(false);
            $table->timestamp("starts_on")->nullable();
            $table->timestamp("expires_on")->nullable();
            $table->timestamps();
            
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_coupons");
    }
}

