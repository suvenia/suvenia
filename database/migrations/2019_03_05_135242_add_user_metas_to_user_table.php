<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserMetasToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('ref')->nullable();
            $table->string('username')->nullable();
            $table->string('slug')->nullable();
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('email_token')->nullable();
            $table->timestamp('email_token_expiry')->nullable();
            $table->boolean('has_password')->default(false);
            $table->boolean('email_verified')->default(false);
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedin')->nullable();
            $table->longText('photo_url')->nullable();
            $table->string('phone')->nullable();
            $table->decimal('balance', 8, 2)->default(0);
            $table->decimal('earnings', 8, 2)->default(0);
            $table->string('bank_sort')->nullable();
            $table->string('bank_bvn')->nullable();
            $table->string('bank_account')->nullable();
            $table->string('bank_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
