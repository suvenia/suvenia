<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchStoreMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("merch_store_meta", function (Blueprint $table) {
            $table->increments('id');
            $table->integer("store_id");
            $table->string("meta_key");
            $table->longText("meta_value")->nullable();
            $table->timestamps();
            
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("merch_store_meta");
    }
}

