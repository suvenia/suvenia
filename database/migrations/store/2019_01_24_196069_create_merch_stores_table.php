<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("merch_stores", function (Blueprint $table) {
            $table->increments('id');
            $table->string("name")->unique();
            $table->string("slug");
            $table->string("ref")->unique();
            $table->longText("description")->nullable();
            $table->string("facebook")->nullable();
            $table->string("twitter")->nullable();
            $table->string("youtube")->nullable();
            $table->string("instagram")->nullable();
            $table->string("website")->nullable();
            $table->boolean("is_published")->default(true);
            $table->boolean("is_deleted")->default(false);
            $table->timestamps();
            
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("merch_stores");
    }
}

