interface JQuery
{
    barrating(options?: any): JQuery;
    LoadingOverlay(options?: any, options2?: any): JQuery;
    intlTelInput(options?: any, options2?: any): JQuery;
    tagsinput(options?: any): JQuery;
}

interface StaticCanvas{
    customizeControls(options?: any): StaticCanvas
}

/*interface Window{
    intlTelInput(options?: any, options2?: any): Window;
}*/