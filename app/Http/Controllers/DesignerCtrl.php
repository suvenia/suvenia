<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DesignerCtrl extends Controller
{
    public function landing_page(){

        return view('designer.landing_page', []);
    }

    public function sign_up(){

        return view('designer.sign_up', []);
    }

    public function catalogue(){

        return view('designer.catalogue', []);
    }

    public function public_profile(){

        return view('designer.public_profile', []);
    }

    

}
