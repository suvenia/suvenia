<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerifyEmail;
use App\User as _User;
use App\Influencer as Influencer;

class InfluencerCtrl extends Controller
{
    public function landing_page(){

        return view('influencer.landing_page', []);
    }

    public function apply(Request $request){
        
        if ($request->isMethod('post')) {
            $role = DB::table('roles')->where('name', 'user')->first();

            $data = $request->all();
            $messages = [
                'tos.required' => 'You must agree to our terms and conditions',
            ];
 
            $validator = Validator::make($data, [
                'username' => 'required|max:255|min:4|unique:users',
                'password' => 'required|min:4',
                'confirm_password' => 'required|min:4|same:password',
                'email' => 'required|min:4|email|unique:users',
                'instagram' => 'required|min:4|url',
                'twitter' => 'required|min:4|url',
                'tos' => 'required|boolean',
            ], $messages);

            if ($validator->fails()) {

                return back()
                    ->withErrors($validator)
                    ->withInput();

            } else {

                $user = new _User();
                $data = $request->all();

                $user->username = $data['username'];
                $user->name = $data['username'];
                $user->ref = str_random(10);
                $user->slug = str_slug($data['username'], '-');
                $user->email = $data['email'];
                $user->password = bcrypt($data['password']);
                $user->has_password = true;
                $user->email_token = str_random(36);
                $user->email_token_expiry = \Carbon\Carbon::now()->addDay(1);
                
                if ($user->save()) {
                   DB::table('user_roles')->insert([
                        'user_id'=> $user->id,
                        'role_id'=> $role->id
                   ]);
                   $influencer = new Influencer();
                   $influencer->user_id =  $user->id;
                   $influencer->is_approved = false;
                   $influencer->invite_code = 'suv-inf-' . str_random(10);
                   $influencer->save();

                    $when = \Carbon\Carbon::now()->addSeconds(2);
                    Mail::to($user->email)->later($when, new VerifyEmail($user, 'INFLUENCER'));
                    return redirect()->route('app:influencer:apply_success');
                }

            }

        }

        return view('influencer.apply', []);
    }

    public function apply_success(){

        return view('influencer.apply_success', []);
    }

    public function sign_up(Request $request, $param){
        //$invite_code = 
        if ($request->isMethod('post')) {
            $role = DB::table('roles')->where('name', 'user')->first();

            $data = $request->all();
            $messages = [
                'invite_code.required' => 'You must Provide an invite code',
                'invite_code.exists' => 'Your invite code does not exists on our system',
            ];
 
            $validator = Validator::make($data, [
                'invite_code' => 'required|exists:influencer',
                'firstname' => 'required|max:255|min:4',
                'lastname' => 'required|max:255|min:4',
                'cellphone' => 'required|digit:11',
                'accepted_payment' => 'required|min:4',
                'minimum_amount' => 'required',
                'specialties' => 'required',
                'platform' => 'required',
                'bio' => 'required|min:4|max:300',
            ], $messages);

            if ($validator->fails()) {

                return back()
                    ->withErrors($validator)
                    ->withInput();

            } else {

                $influencer = Influencer::where('invite_code', $data['invite_code'])->first();
                $data = $request->all();
                $influencer->invite_code = null;
                $influencer->firstname = $data['firstname'];
                $influencer->lastname = $data['lastname'];
                $influencer->cellphone = $data['cellphone'];
                $influencer->accepted_payment = $data['accepted_payment'];
                $influencer->minimum_amount = $data['minimum_amount'];
                $influencer->platform = $data['platform'];
                $influencer->is_approved = $data['is_approved'];
                if ($influencer->save()) {
                    $when = \Carbon\Carbon::now()->addSeconds(2);
                    return redirect()->route('app:influencer:apply_success');
                }

            }

        }

        return view('influencer.sign_up', []);
    }


}
 