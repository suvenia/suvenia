<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Utils as Utils;
use App\ProductCategory as Category;
use App\Brandable as Brandable;

class BrandableCtrl extends Controller
{
    public function index(Request $request)
    {
        $brandables = Brandable::get();
        return view('vendor.voyager.eco-brandables.index', ['brandables' => $brandables]);
    }

    public function add(Request $request){

        if ($request->isMethod('post')) {

            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255|min:2|unique:eco_brandables',
                'price' => 'required|numeric',
                'description' => 'required|min:6',
            ]);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 500);

            } else {

                $data = $request->all();
                $utils = new Utils();

                $front_data = json_decode($request->front_dimension, true);
                $back_data = $request->enable_back_view ? json_decode($request->back_dimension, true) : [];
                $front_image = $request->front_product ? $utils->base64_img($request->front_product, 'Front', 'brandables') : '';
                $back_image = $request->back_product ? $utils->base64_img($request->back_product, 'Back', 'brandables') : '';

                $data_full = [
                    'backViewEnabled' => $request->enable_back_view ? true : false,
                    'sizeEnabled' => $request->enable_sizes ? true : false,
                    'description' => $request->description,
                    'price' => $request->price,
                    'name' => $request->name,
                    'category' => $request->category,
                    'front' => [
                        'left' => $front_data['left'],
                        'top' => $front_data['top'],
                        'width' => $front_data['width'],
                        'height' => $front_data['height'],
                        'image_url' => $front_image,
                    ],
                    'back' => !$request->enable_back_view ? [] : [
                        'left' => $back_data['left'],
                        'top' => $back_data['top'],
                        'width' => $back_data['width'],
                        'height' => $back_data['height'],
                        'image_url' => $back_image,
                    ],

                ];

                $brandable = new Brandable();

                $brandable->name = $request->name;
                $brandable->user_id = Auth::user()->id;
                $brandable->price = $request->price;
                $brandable->front_image = $front_image;
                $brandable->back_image = $back_image;
                $brandable->description = $request->description;
                $brandable->data = json_encode($data_full, true);

                if ($brandable->save()) {
                    $redirectUrl = action([BrandableCtrl::class, 'index']);
                    return response()->json(['message' => $data_full, 'redirect_url' => $redirectUrl, 'isPaused'=> true], 200);
                }

            }

        }
        $categories = Category::where('is_parent', true)->get();

        return view('vendor.voyager.eco-brandables.add', ['categories' => $categories]);
    }


    /**
 * edit_brandable method
 *
 * id Request $request
 * @return void
 */
public function edit_brandable(Request $request, $id)
{
    $brandable = Brandable::where('id', $id)->first();
    $categories = Category::where('parent_id', 0)->get();

    if ($request->isMethod('post')) {

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|min:2|unique:eco_brandables,name,' . $id,
            'price' => 'required|numeric',
            'description' => 'required|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 500);

        } else {

            $brandable = Brandable::where([['id', $id]])->first();
            $oldData = json_decode($brandable->data, true);

            $data = $request->all();
            $utils = new Utils();

            if (!is_null($brandable->front_image)) {
                $old_photo_front = basename($brandable->front_image);
                $link_front = public_path('brandables/') . $old_photo_front;
                if (file_exists($link_front)) {
                    unlink($link_front);
                }
            }

            if ($oldData['backViewEnabled'] === true || $request->enable_back_view === false) {
                if (!is_null($brandable->back_image)) {
                    $old_photo_back = basename($brandable->back_image);
                    $link_back = public_path('brandables/') . $old_photo_back;
                    if (file_exists($link_back)) {
                        unlink($link_back);
                    }
                }
            }

            $front_data = json_decode($request->front_dimension, true);
            $back_data = $request->enable_back_view ? json_decode($request->back_dimension, true) : [];
            $front_image = $request->front_product ? $utils->base64_img($request->front_product, 'Front', 'brandables') : '';
            if ($request->enable_back_view) {
                $back_image = $request->back_product ? $utils->base64_img($request->back_product, 'Back', 'brandables') : '';

            } else {
                $back_image = '';
            }

            $data_full = [
                'backViewEnabled' => $request->enable_back_view ? true : false,
                'sizeEnabled' => $request->enable_sizes ? true : false,
                'description' => $request->description,
                'price' => $request->price,
                'name' => $request->name,
                'category' => $request->category,
                'front' => [
                    'left' => $front_data['left'],
                    'top' => $front_data['top'],
                    'width' => $front_data['width'],
                    'height' => $front_data['height'],
                    'image_url' => $front_image,
                ],
                'back' => !$request->enable_back_view ? [] : [
                    'left' => $back_data['left'],
                    'top' => $back_data['top'],
                    'width' => $back_data['width'],
                    'height' => $back_data['height'],
                    'image_url' => $back_image,
                ],

            ];

            $brandable->name = $request->name;
            $brandable->price = $request->price;
            $brandable->front_image = $front_image;
            $brandable->back_image = $back_image;
            $brandable->description = $request->description;
            $brandable->data = json_encode($data_full, true);

            if ($brandable->save()) {
                $redirectUrl = action([BrandableCtrl::class, 'index']);
                return response()->json(['message' => $data_full, 'redirect_url' => $redirectUrl, 'isPaused'=> true], 200);
            }

        }

    }

    return view('vendor.voyager.eco-brandables.edit', ['brandable' => $brandable, 'categories' => $categories]);
}


    /**
 * delete_brandable method
 *
 * id Request $request
 * @return void
 */
public function delete_brandable(Request $request, $id)
{
    $brandable = Brandable::where([['id', $id]])->first();
    $oldData = json_decode($brandable->product_data, true);

    if (!is_null($brandable->front_image)) {
        $old_photo_front = basename($brandable->front_image);
        $link_front = public_path('brandables/') . $old_photo_front;
        if (file_exists(public_path('brandables/') . $old_photo_front)) {
            unlink($link_front);
        }
    }

    if ($oldData['backViewEnabled'] === true) {
        $old_photo_back = basename($brandable->back_image);
        $link_back = public_path('brandables/') . $old_photo_front;
        if (file_exists(public_path('brandables/') . $old_photo_back)) {
            unlink($link_back);
        }
    }
    if ($brandable->delete()) {
        return back()->with('success', 'Product has been deleted.');
    }

}

}
