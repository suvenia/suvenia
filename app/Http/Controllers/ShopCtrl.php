<?php
namespace App\Http\Controllers;

use App\Analytic as Analytic;
use App\Faq as Faq;
use App\Http\Controllers\Controller;
use App\Order as Order;
use App\OrderItem as OrderItem;
use App\OrderMetric as OrderMetric;
use App\OrderShipping as OrderShipping;
use App\Payment as Payment;
use App\Product;
use App\ProductCategory as Category;
use App\Review as Review;
use App\SavedItem as SavedItem;
use App\SellerOrder as SellerOrder;
use App\ShippingAddress as Shipping;
use App\ShippingLocation as Location;
use App\ShippingZone as Zone;
use App\Store as Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Jenssegers\Agent\Agent;
use LaraCart;
use Victorybiz\GeoIPLocation\GeoIPLocation;

class ShopCtrl extends Controller
{

    public function product_search(Request $request)
    {
        if ($request->ajax()) {
            $products = Product::with(['photos', 'categories'])
                ->where('name', 'like', $request->q . '%')
                ->Orwhere('slug', 'like', $request->q . '%')
                ->Orwhere('description', 'like', $request->q . '%')
                ->limit(5)
                ->get();
            return View::make('partials.search', ['products' => $products]);
        }

        if ($request->isMethod('post')) {
            return redirect()->route('app:ecommerce:product_search', ['q' => $request->q]);
        }

        if ($request->isMethod('get')) {
            $qry = Product::with(['photos', 'categories'])
                ->where('name', 'like', $request->q . '%')
                ->Orwhere('slug', 'like', $request->q . '%')
                ->Orwhere('description', 'like', $request->q . '%');

            $product_count = $qry->count();
            $products = $qry->paginate(10);
            return view('shop.search', ['products' => $products, 'product_count' => $product_count]);
        }

    }

    public function product_catlogue(Request $request, $param)
    {
        $category = Category::with(['children', 'products'])->where('slug', $param)->firstOrFail();
        $all_stores = Store::limit(20)->inRandomOrder()->get();
        $default_prods = [];
        $stores_prod = [];
        $color_prods = [];

        if ($request->q_store) {
            $store = Store::where('id', $request->q_store)->first();
            $ids = json_decode($store->meta, true);
            $f_qry = $category->products->whereIn('id', $ids['products'])->pluck('id');
            $stores_prod = $f_qry->all();
        }

        if ($request->q_col) {
            $products_with_color = [];
            $all_Prods = Product::with('design')->get();
            foreach ($all_Prods as $pro) {
                $allCols = json_decode(json_decode($pro->design->colors, true));
                $chosenColor = base64_decode($request->q_col);
                if (in_array($chosenColor, $allCols)) {
                    $products_with_color[] = $pro->id;
                }
            }
            $f_qry_col = $category->products->whereIn('id', $products_with_color)->pluck('id');
            $color_prods = $f_qry_col->all();
        }

        if (!$request->q_col && !$request->q_store) {
            $default_prods = $category->products->pluck('id')->all();
        }

        $productIds = array_unique(array_merge($color_prods, $stores_prod, $default_prods));

        $products = Product::with('photos')->whereIn('id', $productIds)->paginate(12);

        return view('shop.catalogue', ['category' => $category, 'products' => $products, 'stores' => $all_stores]);
    
    }

    public function single_product(Request $request, $category, $param)
    {
        //LaraCart::destroyCart();
        //echo json_encode(LaraCart::getItems());
        $category = Category::where('slug', $category)->first();
        $Sortref = explode('_', $param);
        $ref = end($Sortref);
        $product = Product::with('photos', 'user')->where('ref', $ref)->first();
        $this->__set_product_views($product, $request);
        $faqs = Faq::where('product_id', $product->id)->get();
        $reviews = Review::where('product_id', $product->id)->get();
        return view('shop.details', ['category' => $category, 'product' => $product, 'faqs' => $faqs, 'reviews' => $reviews]);
    }

    public function ask_question(Request $request)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'question' => 'required|max:400|min:4',
            ]);
            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();

            } else {
                $faq = new Faq();
                $faq->product_id = $request->product_id;
                $faq->question = $request->question;
                $faq->save();
                return back()->with('success', 'Question saved successfully.');
            }
        }
    }

    public function favorite_product(Request $request, $task)
    {
        if ($request->isMethod('post')) {
            if ($task == 'add') {
                $saved = new SavedItem();
                $saved->product_id = $request->id;
                $saved->user_id = Auth::user()->id;
                $saved->save();
            }
            if ($task == 'rem') {
                $saved = SavedItem::where('product_id', $request->id)->first();
                $saved->delete();
            }

        }
    }

    public function __set_product_views($product, $request)
    {
        $analytics = Analytic::where([['category', 'PRODUCTS'], ['client_id', $request->ip()], ['path', url()->full()]]);
        $agent = new Agent();
        $geoip = new GeoIPLocation();

        if ($analytics->get()->count() < 1) {
            $analytic = new Analytic();
            $analytic->user_id = Auth::check() ? Auth::user()->id : '';
            $analytic->client_id = $request->ip();
            $analytic->device = $agent->device();
            $analytic->lang = json_encode($agent->languages());
            $analytic->platform = $agent->platform();
            $analytic->browser = $agent->browser();
            $analytic->model_id = $product->id;
            $analytic->category = 'PRODUCTS';
            $analytic->country = $geoip->getCountry();
            $analytic->page_name = 'ng';
            $analytic->path = url()->full();
            $analytic->is_mobile = !$agent->isPhone() ? false : $agent->isPhone();
            $analytic->save();

        }
    }

    public function buy_direct(Request $request, $code)
    {
        if ($code) {
            $__data = json_decode(base64_decode($code), true);
            if (array_key_exists('id', $__data) && !is_null($__data['id'])) {
                $product = Product::with(['photos', 'user', 'brandable', 'design'])->where('id', $__data['id'])->first();
                //print_r($product);
                LaraCart::addLine(
                    $product->id,
                    $product->name,
                    $__data['qty'],
                    $product->brandable->price + $product->design->total_earning,
                    [
                        "size" => $__data['size'],
                        "color" => $__data['color'],
                    ],
                    $taxable = false
                );

                return redirect()->route('app:ecommerce:step_shipping');

            }

        } else {
            return back()->with('error', 'Invalid direct purchase link!');
        }
    }

    /**
     * Adds a single product to cart
     *
     * @param Request $request
     * @param Integer $id
     * @return void
     */
    public function add_to_cart(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $__data = json_decode(base64_decode($request->item), true);

            $product = Product::with(['photos' => function ($q) {
                $q->first();
            }, 'user', 'brandable', 'design'])->where('id', $id)->first();

            LaraCart::addLine(
                $product->id,
                $product->name,
                $__data['qty'],
                $product->brandable->price + $product->design->total_earning,
                [
                    "size" => $__data['size'],
                    "color" => $__data['color'],
                ],
                $taxable = false
            );

            $message = "$product->name added to Cart";
            return response()->json(['message' => $message, 'cart_count' => LaraCart::count(), 'redirect_url' => route('app:ecommerce:cart_review')], 200);
        }
    }

    public function cart_review(Request $request)
    {

        return view('shop.cart_step.cart_review');
    }

    public function delete_cart_item(Request $request, $hash)
    {
        if ($request->isMethod('post')) {
            LaraCart::removeItem($hash);
            return back()->with('success', 'Item removed from cart');
        }
    }

    public function update_cart_item(Request $request, $hash)
    {
        if ($request->isMethod('post')) {
            LaraCart::updateItem($hash, 'qty', $request->qty);
            return back()->with('success', 'Item updated successfully!');
        }
    }

    public function step_shipping(Request $request)
    {
        $zones = Zone::with('addresses')->get();
        return view('shop.cart_step.shipping', [
            'zones' => $zones,
        ]);
    }

    public function add_address_zone_modal(Request $request, $id)
    {
        $states = Location::where('zone_id', $id)->get();
        return View::make('shop.cart_step.snippet.zone_address', ['id' => $id, 'states' => $states]);
    }

    public function edit_address_zone_modal(Request $request, $id, $address_id)
    {
        $states = Location::where('zone_id', $id)->get();
        $address = Shipping::where('id', $address_id)->first();
        return View::make('shop.cart_step.snippet.edit_zone_address', ['id' => $id, 'states' => $states, 'address_id' => $address_id, 'address' => $address]);
    }

    public function set_default_zone_modal(Request $request, $id)
    {
        $addresses = Shipping::where('zone_id', $id)->get();
        return View::make('shop.cart_step.snippet.select_address', ['addresses' => $addresses, 'id' => $id]);
    }

    public function add_shipping_address(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'firstname' => 'required|max:200',
                'lastname' => 'required|max:200',
                'email' => 'required|max:200|email',
                'address' => 'required|max:200',
                'phone_number' => 'required|max:200',
                //'country' => 'requiredIf',
            ]);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 500);
            } else {

                $shipping = new Shipping();

                //$shipping->user_id = Auth::user()->id;
                $shipping->user_id = Auth::user()->id;
                $shipping->location_id = !$request->location ? 0 : $request->location;
                $shipping->zone_id = $id;
                $shipping->price = 3000;
                $shipping->firstname = $request->firstname;
                $shipping->lastname = $request->lastname;
                $shipping->email = $request->email;
                $shipping->address = $request->address;
                $shipping->phone_number = $request->phone_number;
                $shipping->country = $request->location ? "Nigeria" : $request->country;

                $shipping->save();

                return response()->json(['message' => 'Shipping details saved successfully', 'redirect_url' => url()->previous()], 200);

            }

        }
    }

    public function edit_shipping_address(Request $request, $id, $address_id)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'firstname' => 'required|max:200',
                'lastname' => 'required|max:200',
                'email' => 'required|max:200|email',
                'address' => 'required|max:200',
                'phone_number' => 'required|max:200',
                //'country' => 'requiredIf',
            ]);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 500);
            } else {

                $shipping = Shipping::where(['id' => $address_id])->first();

                //$shipping->user_id = Auth::user()->id;
                $shipping->user_id = Auth::user()->id;
                $shipping->location_id = !$request->location ? 0 : $request->location;
                $shipping->zone_id = $id;
                $shipping->price = 3000;
                $shipping->firstname = $request->firstname;
                $shipping->lastname = $request->lastname;
                $shipping->email = $request->email;
                $shipping->address = $request->address;
                $shipping->phone_number = $request->phone_number;
                $shipping->country = $request->location ? "Nigeria" : $request->country;

                $shipping->save();

                return response()->json(['message' => 'Shipping details updated successfully', 'redirect_url' => url()->previous()], 200);

            }

        }
    }

    public function delete_shipping_address(Request $request, $id)
    {
        // $request->session()->forget('CartShippingPrice');
        Shipping::where('id', $id)->delete();
        return back()->with('success', 'Address deleted sucessfully!');
    }

    public function select_shipping_address(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            DB::table('eco_buyer_addresses')->where([['is_default', true], ['zone_id', $id]])->update(['is_default' => false]);
            $shipping = Shipping::where('id', $request->shipping_address)->first();
            $shipping->is_default = true;
            $shipping->save();
            return response()->json(['message' => 'Changes saved successfully', 'redirect_url' => url()->previous()], 200);
        }
    }

    public function select_zone(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $shipping = Shipping::where([['zone_id', $id], ['is_default', true]])->first();
            //$checkAddress = Shipping::where([['zone_id', $id]])->count();

            $count = LaraCart::count();
            $shippingPrice = 0;
            if (isset($shipping->location_id)) {
                $location = Location::where('id', $shipping->location_id)->first();

                if (isset($location) && count(json_decode($location->data, true)) > 0) {
                    foreach (json_decode($location->data, true) as $key => $value) {
                        $range = explode(",", $key);
                        $low = intval($range[0]);
                        $high = intval($range[1]);
                        if ($this->nBetween($count, $high, $low)) {
                            $shippingPrice = $value;
                        }
                    }
                }
                $request->session()->put('CartShippingAddressID', $shipping->id);
            } else {
                $request->session()->put('CartShippingAddressID', 0);
            }

            $request->session()->put('CartShippingPrice', $shippingPrice);
            $request->session()->put('CartZoneID', $id);
            Zone::where('is_default', true)->update(['is_default' => false]);
            $zone = Zone::where('id', $id)->first();
            $zone->is_default = true;
            $zone->save();

            return response()->json(['message' => 'Shipping Selected Successfuly!', 'redirect_url' => url()->previous()], 200);
        }
    }

    public function nBetween($varToCheck, $high, $low)
    {
        if ($varToCheck < $low) {
            return false;
        } elseif ($varToCheck > $high) {
            return false;
        } else {
            return true;
        }

    }

    public function process_shipping_price_for_payment(Request $request)
    {
        if ($request->isMethod('post')) {
            //$request->session()->pull('CartZoneID');
            $zone = Zone::where('id', $request->__zone_id)->first();
            if (!$zone) {
                return back()->with('error', 'Please select a shipping zone.');
            } else {
                $location = Shipping::where([['zone_id', $zone->id], ['is_default', true]])->first();
                if ($zone->has_option && !isset($location)) {
                    return back()->with('error', 'Please select or add an address for your shipping zone.');
                }

            }

            return redirect()->route('app:ecommerce:step_pay');
        }
    }

    public function step_pay(Request $request)
    {
        if ($request->session()->exists('CartZoneID')) {
            $zone = Zone::where('id', session('CartZoneID'))->first();
        } else {
            return back()->with('error', 'Invalid Request!');
        }
        return view('shop.cart_step.pay', ['zone' => $zone]);
    }

    public function process_payment(Request $request, $type)
    {
        if ($request->isMethod('post')) {
            Auth::loginUsingId(1);
            $order_id = 'SUV-' . str_random(10);
            $total_amount = LaraCart::total($formatted = false) + session('CartShippingPrice');

            try {
                $decrypted_type = decrypt($type);
            } catch (DecryptException $e) {
                return back()->with('error', 'Invalid token!');
            }

            //save payment
            $payment = new Payment();
            $payment->user_id = Auth::user()->id;
            $payment->order_id = $order_id;
            $payment->amount = $total_amount;
            $payment->reference = $order_id;
            $payment->gateway = $decrypted_type;
            $payment->save();

            $order = new Order();
            $order->user_id = Auth::user()->id;
            $order->payment_id = $payment->id;
            $order->order_ref = $order_id;
            $order->price = $total_amount;
            $order->save();

            //save order shipping
            $order_shipping = new OrderShipping();
            $order_shipping->user_id = Auth::user()->id;
            $order_shipping->order_id = $order->id;
            $order_shipping->order_ref = $order_id;
            $order_shipping->address_id = session('CartShippingAddressID');
            $order_shipping->price = session('CartShippingPrice');
            $order_shipping->zone_id = session('CartZoneID');
            $order_shipping->status = 0;
            $order_shipping->save();

            //save order items
            foreach (LaraCart::getItems() as $item) {
                $order_item = new OrderItem();
                $order_item->user_id = Auth::user()->id;
                $order_item->order_id = $order->id;
                $order_item->product_id = $item->id;
                $order_item->order_ref = $order_id;
                $order_item->quantity = $item->qty;
                $order_item->price = $item->price;
                $order_item->data = json_encode(['color' => $item->color, 'size' => $item->color]);
                $order_item->save();

                $seller_order = new SellerOrder();
                $product = Product::with(['design', 'brandable'])->where('id', $item->id)->first();
                $seller_order->order_id = $order_id;
                $seller_order->user_id = $product->user_id;
                $seller_order->save();

                $order_metric = new OrderMetric();
                $percentage = 20; //20%
                $order_metric->product_id = $product->id;
                $order_metric->order_item_id = $order_item->id;
                $order_metric->user_id = $product->user_id;
                $order_metric->order_id = $order->id;
                $order_metric->payment_id = $payment->id;
                $order_metric->user_profit = $product->design->total_earning - $product->design->total_earning * 0.2;
                $order_metric->site_profit = $product->design->total_earning * 0.2;
                $order_metric->expenses = $product->brandable->price;
                $order_metric->save();

            }

            //TODO ORDER: SAVE ORDER METRICS HERE...

            if ($decrypted_type == 'PAYSTACK') {
                $paystack = new \Yabacon\Paystack(config('ecommerce_config.paystack_code'));
                try
                {
                    $tranx = $paystack->transaction->initialize([
                        'amount' => $total_amount * 100, // in kobo
                        'email' => Auth::user()->email, // unique to customers
                        'reference' => $order_id, // unique to transactions
                    ]);
                } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                    die($e->getMessage());
                }

                return redirect($tranx->data->authorization_url);
            } elseif ($decrypted_type == 'PAYLATER') {
                return redirect()->route('app:ecommerce:payment_notify_status', ['status' => encrypt($payment->id)]);
            }

        }

    }

    public function payment_verify(Request $request)
    {
        if ($request->reference) {
            $paystack = new \Yabacon\Paystack(config('ecommerce_config.paystack_code'));
            try {

                $tranx = $paystack->transaction->verify([
                    'reference' => $request->reference, // unique to transactions
                ]);
                if ('success' === $tranx->data->status) {
                    $payment = Payment::where('order_id', $request->reference)->first();
                    $payment->reference = $request->reference;
                    $payment->status = true;
                    $payment->save();
                    return redirect()->route('app:ecommerce:payment_notify_status', ['status' => encrypt($payment->id)]);
                }
            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                // return redirect()->route('shop:cart_payment_error');
            }
        }
    }

    public function payment_notify(Request $request, $status)
    {
        if (!$status) {
            return redirect()->route()->with('error', 'Invalid Link!');
        } else {
            try {
                $pid = decrypt($status);
            } catch (DecryptException $e) {
                return redirect()->route()->with('error', 'Invalid Link!');
            }
            LaraCart::destroyCart();
            $request->session()->forget('CartZoneID');
            $request->session()->forget('CartShippingPrice');
            $request->session()->forget('CartShippingAddressID');
            $payment = Payment::where('id', $pid)->first();
        }
        return view('shop.cart_step.payment_notify', ['payment' => $payment]);
    }

}
