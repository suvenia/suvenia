<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Order as Order;
use App\SellerOrder as SellerOrder;
use App\Payment as Payment;
use App\Withdraw as Withdraw;
use App\Product as Product;
use App\Design as Design;
use App\Store as Store;
use App\Attachment as Attachment;
use App\User as _User;
use App\Rules\PasswordExists;
use App\Rules\AmountEquals;
use App\Rules\NumberBetween;
use App\Helpers\Utils; 
use Illuminate\Validation\Rule;
use App\InfluencerGig;

class DashboardCtrl extends Controller
{
    public $store_meta = [ 
        'store_logo_text'=>'',
        'store_logo_font'=>'',
        'store_logo_colors'=>'',
        'store_logo'=>'',
        'default_brand'=> 'TEXT',
        'store_theme'=> '',
        'store_theme_colors'=> '',
        'store_upload_banner'=> '',
        'store_banner_system'=>'',
        'default_banner'=> 'SYSTEM',
        'products'=>[],
        'featured_products'=>[],
        'display_product'=>[],
    ];

    public function index(Request $request){

        return view('dashboard.home', []);
    }

    public function my_orders(Request $request){
        $duration = isset($request->dur) ? \Carbon\Carbon::now()->subDays(intval(base64_decode($request->dur))) : \Carbon\Carbon::now()->subDays(0);
        
        $orders = Order::with(['order_items' => function($q){
            $q->with('product')->get();
        }, 'order_shipping'])->whereDate('created_at', '<=', $duration)->where('user_id', Auth::user()->id )->paginate(10);
       
        return view('seller.my_orders', ['orders'=> $orders]);
    }

    public function customer_orders(Request $request){
        $seller_orders = SellerOrder::where('user_id', Auth::user()->id)->get();
        $duration = isset($request->dur) ? \Carbon\Carbon::now()->subDays(intval(base64_decode($request->dur))) : \Carbon\Carbon::now()->subDays(0);
        $orders = Order::with(['order_items' => function($q){
            $q->with('product')->get();
        }, 'order_shipping', 'user', 'order_metrics'])->whereDate('created_at', '<=', $duration)->whereIn('order_ref', $seller_orders->pluck('order_id'))->paginate(10);
        
        return view('seller.customer_orders', ['orders'=> $orders]);
    }

    public function profile_settings(Request $request){
        
        return view('seller.profile_settings', []);
    }

    public function profile_settings_save_image(Request $request, $id){
        if($request->isMethod('post')){
        $files = $request->file('files');
        if ($request->hasFile('files')) {
            $old_attach = Attachment::where([['model_id', $id], ['model', 'User']])->first();
            if($old_attach && file_exists($old_attach->path)){
                unlink($old_attach->path);
                 $old_attach->delete();
            }
           
            foreach($files as $file){
                $path = $file->store('graph', 'user');
                $attchment = new Attachment();
                $attchment->model = 'User';
                $attchment->model_id = $id;
                $attchment->name = basename($path);
                $attchment->path = public_path('user/' . $path);
                $attchment->url = 'user/' . $path;
                $attchment->save();
                return response()->json(['img'=> asset($attchment->url) ], 200);
            }
           
        }
        }
       
        //return view('seller.profile_settings', []);
    }

    public function profile_settings_delete_image(Request $request, $id){
        if($request->isMethod('post')){
            $att = Attachment::where([['model', 'User'], ['model_id', $id]])->first();
            if(file_exists($att->path)){
                unlink($att->path);
                $att->delete();
                return response()->json(['img'=> asset('user/default.png') ], 200);
            }
        }
    }

    public function profile_settings_basic(Request $request){
        if($request->isMethod('post')){
        $user = _User::where('id', Auth::user()->id)->first();

        $validator = Validator::make($request->all(), [
            'username' => 'required|max:255|min:4|unique:users,username,' . $user->id,
            'firstname' => 'max:255|min:2',
            'lastname' => 'max:255|min:2',
            'phone' => 'digits:11|required',
        ]);

        if ($validator->fails()) {

            return response()->json(['errors'=> $validator->errors()], 500);

        } else {
            $user = _User::where('id', Auth::user()->id)->first();
            $user->username = $request->username;
            $user->slug = str_slug($request->username, '-');
            $user->phone = $request->phone;
            $user->firstname = $request->firstname;
            $user->lastname = $request->lastname;

            if ($user->save()) {
                return response()->json(['message'=> 'Account settings updated successfully', 'redirect_url'=> route('app:dashboard:profile-settings')], 200);
            }
         }
       }
    }

    public function profile_settings_password(Request $request){
        if($request->isMethod('post')){
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'password' => ['max:255', 'min:4', new PasswordExists],
                    'confirm_password' => 'required|min:4|same:password',
                ]);
    
                if ($validator->fails()) {
                    return response()->json(['errors'=> $validator->errors()], 500);
                } else {
    
                    $user = _User::where('id', Auth::user()->id)->first();
                    $user->password = bcrypt($request->password);
                    if ($user->save()) {
                        return response()->json(['message'=> 'Password setting updated successfully', 'redirect_url'=> route('app:dashboard:profile-settings')], 200);
                    }
                }
    
            }
        }
    }

    public function profile_settings_bank(Request $request){
        if($request->isMethod('post')){
			$utils = new Utils();
			$user = \App\User::where('id', Auth::user()->id)->first();
			$validator = Validator::make($request->all(), [
				'bank' => 'required',
				'bank_name' => 'required',
				'bank_bvn' => 'required|digits:11',
				'bank_account' => 'required|digits:10',
			]);
			
			if ($validator->fails()) {
				return response()->json(['errors'=> $validator->errors()], 500);
				
			}else{
				
            $paystack = new \Yabacon\Paystack(config('ecommerce_config.paystack_code'));
            
            try
			{
			  $tranx = $paystack->transferrecipient->create([
				'name' => $request->bank_name,
                'description'=> 'Transfer Reciepent',
                'bank_code'=> $request->bank,
                'currency'=> 'NGN',
                'account_number'=> $request->bank_account
			  ]);
              
               if($tranx->status === true){
                $user->bank_sort = $request->bank;
				$user->bank_name = $request->bank_name;
				$user->bank_bvn = $request->bank_bvn;
				$user->bank_account = $request->bank_account;
                $user->transfer_code = $tranx->data->recipient_code;
				if($user->save()){
					return response()->json(['message'=> 'Your Profile updated was successfull.', 'redirect_url'=> route('app:dashboard:profile-settings')], 200);
				} 
               }
				
			}catch(\Yabacon\Paystack\Exception\ApiException $e){
				//die($e->getMessage());
                return response()->json(['errors'=> str_replace("'", " ", str_after($e->getMessage(), ':'))], 500);
			}
			
		}
      }
    }

    public function payment_history(Request $request){
        $payments = Payment::where('user_id', Auth::user()->id)->paginate(10);
        return view('seller.payment_history', ['payments'=> $payments]);
    }

    public function withdraw(Request $request){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                 'amount' => ['required', new NumberBetween(1000, 50000), new AmountEquals(Auth::user()->balance)]
             ]);
             if ($validator->fails()) {
                 return response()->json(['errors'=> $validator->errors()], 500);
             }else{
                 $withdraw = new Withdraw();
                 $withdraw->user_id = Auth::user()->id;
                 $withdraw->code = Auth::user()->transfer_code;
                 $withdraw->amount = $request->amount;
                 $withdraw->ref = 'SUV-' . str_random(20);
                 $withdraw->status = false;
                 $withdraw->save();
                 $userQuery = \App\User::where('id', Auth::user()->id)->first(); 
                 $userQuery->decrement('balance', $request->amount);
                 
                 return response()->json(['message'=> 'Your request has been recieved! After validation, your payment would be made within 5 working days.', 'redirect_url'=> route('app:dashboard:withdraw')], 200);
             }
        }
        $withdrawals = Withdraw::where('user_id', Auth::user()->id)->paginate(10);
        return view('seller.withdrawal', ['withdrawals'=> $withdrawals]);
    }

    public function my_products(Request $request){
        $q = $request->q ? $request->q : '';
        $products = Product::with(['order_metric', 'design', 'brandable'])->where([['user_id', Auth::user()->id], ['is_archived', false], ['name', 'like', $request->q . '%']])->paginate(15);
        return view('seller.products', ['products'=> $products]);
    }

    public function delete_product(Request $request){
        $ids = collect(json_decode($request->deleteIds, true));
        if($ids->count() > 0){
            $products = Product::whereIn('id', $ids)->update(['is_archived'=> true]);
            return back()->with('success', 'Product(s)');
        }

    }

    public function search_product(Request $request){
        if($request->isMethod('post')){
            $q = $request->q;
            return redirect()->route('app:dashboard:my_products', ['q'=> $q]);
        }
    }

    public function edit_product(Request $request, $id){
        if($request->isMethod('post')){
            $product = Product::where('id', $id)->first();
            $product->name = $request->name;
            $product->description = $request->description;
            $product->save();

            $design = Design::where('product_id', $product->id)->first();
            $design->total_earning = $request->earning;
            $design->save();

            return response()->json(['message'=> 'Product Updated Successfully', 'redirect_url'=> route('app:dashboard:product_edit', ['id'=> $product->id]) ], 200);
        }
        if($request->isMethod('get')){
        $product = Product::with(['order_metric', 'design', 'brandable', 'photos'])->where([['id', $id], ['user_id', Auth::user()->id], ['is_archived', false]])->first();
        }
        return view('seller.edit_product', ['product'=> $product]);
    }

    public function all_store(Request $request){
        $stores = Store::paginate(10);
        return view('seller.all_stores', ['stores'=> $stores]);
    }

    public function create_store(Request $request){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
				'name' => 'required|unique:merch_stores',
			]);
			
			if ($validator->fails()) {
				return response()->json(['errors'=> $validator->errors()], 500);
			}else{

                $store = new Store();
                $store->name = $request->name;
                $store->slug = str_slug($request->name);
                $store->ref = str_random(10);
                $store->meta = json_encode($this->store_meta);
                $store->save();
                return response()->json(['message'=> 'Store created successfully!', 'redirect_url'=> route('app:dashboard:all_store')], 200);
            }
        }
    }

    public function edit_store(Request $request, $id){
        $store = Store::where('id', $id)->first();
        $products = Product::get();
        return view('seller.edit_store', ['store'=> $store, 'products'=> $products]);
    } 

    public function store_view(Request $request, $slug){
        $store = Store::where('slug', $slug)->first();
        
        return view('seller.store_view', ['store'=> $store]);
    } 

    public function store_header_edit(Request $request, $id){
        if($request->isMethod('post')){
            $store = Store::where('id', $id)->first();
            $meta = json_decode($store->meta, true);
            $data = $request->all();
            $validator = Validator::make($request->all(), [
				'name' => ['required', Rule::unique('merch_stores')->ignore($store->id)],
			]);
			
			if ($validator->fails()) {
				return response()->json(['errors'=> $validator->errors()], 500);
			}else{

            $meta['store_logo_text'] = $data['store_logo_text'];
            $meta['store_logo_font'] = $data['store_logo_font'];
            $meta['store_logo_color'] = $data['store_logo_color'];
            $meta['store_theme_color'] = $data['store_theme_color'];
            $meta['store_theme'] = $data['store_theme'];
            $meta['default_brand'] = $data['default_brand'];
           if(isset($data['store_logo_temp'])){
            $Imageurl  = $this->save_base64($store->ref, public_path('store_img/logos/'),'logos', $data['store_logo_temp']);
            $meta['store_logo'] = $Imageurl;
           }else{
            $meta['store_logo'] = isset($data['store_logo']) ? $data['store_logo'] : '';
           }
            $store->name = $request->name;
            $store->slug = str_slug($request->name);
            $store->meta = json_encode($meta);
            $store->save();
           return response()->json(['message'=> 'Store updated successfully!'], 200);
           
         }

        }
    } 

    public function store_banner_edit(Request $request, $id){
        if($request->isMethod('post')){
            $store = Store::where('id', $id)->first();
            $meta = json_decode($store->meta, true);
            $data = $request->all();
            if(isset($data['store_banner_temp']) && $data['default_banner'] == 'USER'){
                $Imageurl  = $this->save_base64($store->ref, public_path('store_img/banner/'),'banner', $data['store_banner_temp']);
                $meta['store_upload_banner'] = $Imageurl;
            }

            if(isset($data['system_store_banner']) && $data['default_banner'] == 'SYSTEM'){
                $meta['store_banner_system'] = $data['system_store_banner'];
            }

            $meta['default_banner'] = $data['default_banner'];
            $store->meta = json_encode($meta);
            $store->save();
            return response()->json(['message'=> 'Store updated successfully!'], 200); 

        } 
    }

    public function store_products_edit(Request $request, $id){
        if($request->isMethod('post')){
            $store = Store::where('id', $id)->first();
            $meta = json_decode($store->meta, true);
            $data = $request->all();
            $meta['products'] = $data['store_products'];
            $store->meta = json_encode($meta);
            $store->save();
            return response()->json(['message'=> 'Store updated successfully!'], 200);
        }
    }

    public function store_about(Request $request, $id){
        if($request->isMethod('post')){
            $store = Store::where('id', $id)->first();
            $meta = json_decode($store->meta, true);
            $data = $request->all();
            $validator = Validator::make($request->all(), [
				'facebook' => 'present',
				'twitter' => 'present',
				'instagram' => 'present',
				'website' => 'present',
			]);
			
			if ($validator->fails()) {
				return response()->json(['errors'=> $validator->errors()], 500);
			}else{
                $store->description = $request->description;
                $store->facebook = $request->facebook;
                $store->twitter = $request->twitter;
                $store->instagram = $request->instagram;
                $store->website = $request->website;
                $store->save();
                return response()->json(['message'=> 'Store updated successfully!'], 200);
            }
        }
    }

    public function edit_store_save_image(Request $request){
        if($request->isMethod('post')){
            $files = $request->file('files');
            foreach($files as $file){
                $img = file_get_contents($file->getPathName());
                $type = $file->getMimeType();
                $data = 'data:' . $type . ';base64,' . base64_encode($img);
                return response()->json(['img'=> $data ], 200);
            }
        }
    }

    public function save_base64($filename, $path, $sub_folder, $data){
        $imga = substr($data, 5, strpos($data, ';')-5);
        $type = explode('/', $imga);
        if(!file_exists($path . $filename)){
            mkdir($path . $filename);
        }else{
            $old_files = glob($path . $filename . '/*');

            //Loop through the file list.
            foreach($old_files as $file){
            //Make sure that this is a file and not a directory.
            if(is_file($file)){
            //Use the unlink function to delete the file.
            unlink($file);
            }
            }
        }
        $image_path = file_put_contents($path . $filename . '/' . $filename . '.' . $type[1], base64_decode(explode(',', $data)[1]));
        return $sub_folder . '/' . $filename . '/' . $filename . '.' . $type[1];
    }

    //INFLUENCER
    public function influencer_all_gigs(Request $request){
        $gigs = InfluencerGig::with(['product'=> function($q){
            $q->with(['order_metric', 'design', 'brandable']);
        }])->paginate(15);
        return view('influencer.gigs', ['gigs'=> $gigs]);
    }

}