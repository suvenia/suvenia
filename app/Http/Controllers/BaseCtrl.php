<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\ProductCategory as Category;

class BaseCtrl extends Controller{

    public function index(Request $request){

        $categories = Category::with(['children'=> function($q){
            $q->with('children');
        }])->where('parent_id', 0)->get();

        return view('shop.index', ['categories'=> $categories]);
    }
}