<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Brandable as Brandable;
use App\ProductCategory as Category;
use App\Template as Template;
use App\Product as Product;
use App\Design as Design;
use App\Photo as Photo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use \Imagine\Image\Box;
use \Imagine\Image\Point;

class BrandableCtrl extends Controller
{

    public function product_list(Request $request)
    {
        $brandables = Brandable::get();
        return view('customizer.product_list', ['brandables' => $brandables]);
    }

    public function design(Request $request)
    {
        $brandables = Brandable::get();
        $arts = DB::table('eco_arts')->where('is_parent', true)->paginate(10);
        $templates = Template::paginate(20);
        $sub_categories = Category::where('is_sub', true)->get();

        return view('customizer.design', ['brandables' => $brandables, 'sub_categories' => $sub_categories, 'arts'=> $arts, 'templates'=> $templates]);
    }

    public function upload_design(Request $request)
    {
        if ($request->isMethod('post')) {
            if ($request->file('file')->isValid()) {
                $file = $request->file('file');
                $ff = file_get_contents($request->file('file')->path());
                $mime = $request->file('file')->getMimeType();
                $FName = $request->file('file')->getClientOriginalName();
                $encodedFile = 'data: ' . $mime . ';base64,' . base64_encode($ff);
                return response()->json(['file' => $encodedFile, 'image_path' => $request->file('file')->path(), 'name'=> $FName], 200);
            }
        }
    }

    public function fetch_arts(Request $request){
        if($request->pid){
            $arts = DB::table('eco_arts')->where('parent_ref', $request->pid)->paginate(6);
            return View::make('customizer.arts_paginate')->with(['arts'=> $arts, 'parent_ref'=> $request->pid])->render();
        }else{
            $arts = DB::table('eco_arts')->where('is_parent', true)->paginate(12);
            return View::make('customizer.art_paginate_base')->with('arts', $arts)->render();
        }
    } 

    public function design_success(Request $request)
    {

        return view('customizer.design_success', []);
    }

    public function process_design(Request $request){
        if($request->isMethod('post')){
            $foldername = str_random(10);
            $validator = Validator::make($request->all(), [
				'name' => 'required|max:255|min:2',
				'description' => 'required|min:2',
				'user_price' => 'required',
				//'target_quantity' => 'required',
            ]);
            if ($validator->fails()) {
				return response()->json(['error'=> $validator->errors()], 500);
			}else{
                $product = new Product();
                $product->name = $request->name;
                $product->slug = str_slug($request->name, '-');
                $product->ref = str_random(10);
                $product->user_id = Auth::user()->id;
                $product->description = $request->description;
                $product->price = $request->user_price;
                $product->brandable_id = $request->design_id;
                $product->save();
                $this->uploadDesigned($request, $product->ref, $product->id);
                return response()->json(['message'=> 'Product was saved successfully', 'redirect_url'=> route('app:ecommerce:design')], 200);

            }

        }
    }

    public function uploadDesigned($request, $foldername, $id){
        $designs =  json_decode($request->design_data, true);
         mkdir(public_path('products/'. $foldername));
            if(isset($designs['back']) && !empty($designs['back']['design'])){
                foreach($designs as $key=>$design){ 
                $imagine = new \Imagine\Gd\Imagine();
                $imagine_design = new \Imagine\Gd\Imagine();
                $imgDir = basename($design['img_url']);
                $image = $imagine->open(public_path('brandables/' . $imgDir));
                
                //save main design
                $SaveAndGetPath = $this->DataToPNG($design['design'], public_path('products/') , $foldername);

                $user_design = $imagine_design->open($SaveAndGetPath);

                //$user_design = $imagine_design->open($design['design']);
                $art_position = new \Imagine\Image\Point($design['left'], $design['top']);
                $image->paste($user_design, $art_position);
                /*$position = new \Imagine\Image\Point(0, 0);
                    
                $palette = new \Imagine\Image\Palette\RGB();
                $size  = new \Imagine\Image\Box(530, 630);
                $color = $palette->color($request->color);
                $new_image = $imagine->create($size, $color);
                    
                $new_image->paste($image, $position);*/

               // $user_design->save(public_path('products/'. $foldername . '/design-' . $key . '-' . $foldername . '.png'));
                $image->save(public_path('products/'. $foldername . '/' . $key . '-' . $foldername . '.png'));
                
                //save meta datas
                $imgPathsArray = [
                    'url'=> 'products/'. $foldername . '/' . $key . '-' . $foldername . '.png',
                    'path'=> public_path('products/'. $foldername . '/' . $key . '-' . $foldername . '.png')
                ];
                $this->saveProductMetas($id, $request, $imgPathsArray);

                    
                }
            }else{
                unset($designs['back']);
                $imagine = new \Imagine\Gd\Imagine();
                $imagine_design = new \Imagine\Gd\Imagine();
                $imgDir = basename($designs['front']['img_url']);
                $image = $imagine->open(public_path('brandables/' . $imgDir));
                
                //save main design
                $SaveAndGetPath = $this->DataToPNG($designs['front']['design'], public_path('products/') , $foldername);

                $user_design = $imagine_design->open($SaveAndGetPath);
                $art_position = new \Imagine\Image\Point($designs['front']['left'], $designs['front']['top']);
                $image->paste($user_design, $art_position);
                /*$position = new \Imagine\Image\Point(0, 0);
                
                $palette = new \Imagine\Image\Palette\RGB();
                $size  = new \Imagine\Image\Box(530, 630);
                $color = $palette->color($request->color);
                $new_image = $imagine->create($size, $color);
                
                $new_image->paste($image, $position);*/
               // $user_design->save(public_path('products/'. $foldername . '/design-front' . '-' . $foldername . '.png'));
                $image->save(public_path('products/'. $foldername . '/front' . '-' . $foldername . '.png'));

                $imgPathsArray = [
                    'url'=> 'products/'. $foldername . '/front' . '-' . $foldername . '.png',
                    'path'=> public_path('products/'. $foldername . '/front' . '-' . $foldername . '.png')
                ];
                $this->saveProductMetas($id, $request, $imgPathsArray);
            }
          
    }

    /**
     * saves the product meta datas to database
     *
     * @param integer $product_id
     * @param array $photos
     * @return void
     */
    public function saveProductMetas($product_id, $request, $photos = []){
        $design = new Design;
        $photo = new Photo;

        $design->product_id = $product_id;
        $design->user_id = Auth::user()->id;
        $design->brandable_id = $request->design_id;
        $design->name = $request->name;
        $design->earnings = $request->earnings;
        $design->target_quantity = $request->target_quantity;
        $design->total_earning = $request->total_earning;
        $design->percentage = $request->percentage;
        $design->raw_design = $request->design_data;
        $design->colors = json_encode($request->colors);
        $design->save();

        //save photo to product photo DB
            $photo->user_id = Auth::user()->id;
            $photo->product_id = $product_id;
            $photo->path = $photos['path'];
            $photo->public_url = $photos['url'];
            $photo->save();
       
        
    }

    public function DataToPNG($data, $path, $filename){
        $image_path = file_put_contents($path . $filename . '/' . $filename . '.png', base64_decode(explode(',', $data)[1]));
        return $path . $filename . '/' . $filename . '.png';
    }

}
