<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{

    protected $table = 'eco_faqs';

    public function user(){
        return $this->belongsTo("App\User");
    }
public function product(){
        return $this->belongsTo("App\Product");
    }
}
