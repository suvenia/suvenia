<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfluencerGig extends Model
{
    protected $table = 'gigs';

    public function product(){
        return $this->belongsTo("App\Product");
    }
}
