<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingLocation extends Model
{

    protected $table = 'eco_shipping_locations';

    public function zone(){
        return $this->belongsTo("App\ShippingZone", "zone_id");
    }
}
