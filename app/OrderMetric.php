<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderMetric extends Model
{

    protected $table = 'eco_order_metrics';

    public function order(){
        return $this->belongsTo("App\Order");
    }
public function payment(){
        return $this->belongsTo("App\Payment");
    }
}
