<?php
namespace App;

//use Ghanem\Rating\Traits\Ratingable as Rating;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $table = 'eco_products';

   // use Rating;

    public function user()
    {
        return $this->belongsTo("App\User");
    }
    public function categories()
    {
        return $this->belongsToMany("App\Category", "eco_category_product");
    }
    public function brandable()
    {
        return $this->belongsTo("App\Brandable");
    }
    public function design()
    {
        return $this->hasOne("App\Design");
    }
    public function photos()
    {
        return $this->hasMany("App\Photo");
    }
    public function orders()
    {
        return $this->hasMany("App\Order");
    }
    public function order_item()
    {
        return $this->hasOne("App\OrderItem");
    }
    public function coupon()
    {
        return $this->belongsTo("App\Coupon");
    }
    public function faqs()
    {
        return $this->hasMany("App\Faq");
    }
    public function feedbacks()
    {
        return $this->hasMany("App\Feedback");
    }

    public function order_metric()
    {
        return $this->hasMany("App\OrderMetric");
    }
}
