<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{

    protected $table = 'eco_withdrawals';

    public function user(){
        return $this->belongsTo("App\User");
    }

}
