<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\ProductCategory as Category;
use App\Product;
use App\Analytic;
use App\SavedItem;
use App\Attachment as Attachment;

class Utils{
    
    public function isActive($routes = []){
        if(in_array(Route::currentRouteName(), $routes)){
            return true;
        }
        
        return false;
    }

    public function makeActive($routes = [], $class){
        if(in_array(Route::currentRouteName(), $routes)){
            return $class;
        }
        return '';
    }
     
    public function can($abilities){
        $user_perm = Auth::user()->getAbilities()->pluck('name')->toArray();
        if(array_intersect($user_perm, $abilities)){
            return true;
        }
        return false;
    }

    public function userRoles(){
        $selected_roles = DB::table('user_roles')->where('user_id', Auth::user()->id)->get();
        $roles = DB::table('roles')->whereIn('id', $selected_roles->pluck('role_id'))->get();
        return $roles->pluck('name')->toArray();
    }

    public function device(){
        $agent = new \Jenssegers\Agent\Agent();
        return $agent;
    }

    public function get_categories(){
        $categories = Category::with(['children'])->where('is_parent', true)->get();
        return $categories;
    }


    public function set_shipping_status($status){
        switch ($status){ 
            case 0:
            return 'Queued';
            break;
            case 1:
            return 'In Production';
            break;
            case 2:
            return 'Awaiting Delivery';
            case 3:
            return 'Delivered';
            default:
            return 'Cancelled';
        }
    }


    public function get_photos($id){
        $photos = \App\Photo::where('product_id', $id)->get();
        return isset($photos) ? $photos : [] ;
    }

    public function get_address($order_shipping){
        $zone_id = $order_shipping->zone_id;
        $address_id = $order_shipping->address_id;
        $data = [
            "name"=>'',
            "address"=>'',
        ];

        if(isset($address_id) && $address_id != 0){
            $address = \App\ShippingAddress::where('id', $address_id)->first();
            if(is_null($address)){
                return [
                    "name" => json_encode($address),
                    "address"=> ''
                ];
            }else{
                return [
                    "name" => $address->firstname . ' ' . $address->lastname,
                    "address"=> $address->address
                ];
            }
            
        }else{
            $zone = \App\ShippingZone::where('id', $zone_id)->first();
            return [
                "name" => $zone->name,
                "address"=> $zone->description
            ];
        }
    }

    public function base64_img($data, $name, $folder){
		$data = str_replace('data:image/png;base64,', '', $data);
		$data = str_replace(' ', '+', $data);
		$data = base64_decode($data);
		$url_setup = $name . '-' . str_random(10) . '.png';
		$url = public_path() . '/'. $folder .'/' . $url_setup;
		file_put_contents($url, $data);
		return asset($folder .'/' . $url_setup); 
     }

     public function browsing_history(){
         $current_user = request()->ip();
         $analytics = Analytic::where('client_id', request()->ip())->get();
         $ids = $analytics->pluck('model_id');
         $products = Product::with(['user','categories'=>function($q){
            $q->where('parent_id', '!=', null);
        }, 'photos'])->whereIn('id', $ids)->get();
         return $products;
     }

     public function get_product_view_count($id){
        $analytics = Analytic::where('model_id', $id)->count();
        return $analytics;
     }

     public function fetch_related_product($id){
        $category = Category::with('products')->where('id', $id)->first();
        $ids = $category->products->pluck('id');
        $products = Product::with(['user','categories'=>function($q){
            $q->where('parent_id', '!=', null);
        }, 'photos'])->limit(12)->whereIn('id', $ids)->get();
         return $products;
     }

     public function check_if_favorited($id){
            $saveditem = SavedItem::where('product_id', $id)->first();
            if($saveditem){
                return true;
            }else{
                return false;
            }
     }

     public function get_attachment($model, $model_id){
        $att = Attachment::where([['model', $model], ['model_id', $model_id]])->first();
        return $att;
     }

     public function fetchBanks(){
        $data = [
            '044'=> 'Access Bank',
            '063'=> 'Diamond Bank',
            '050'=> 'Ecobank',
            '070'=> 'Fidelity Bank',
            '011'=> 'First Bank',
            '214'=> 'First City Monument Bank',
            '058'=> 'Guaranty Trust Bank',
            '076'=> 'Skye Bank',
            '221'=> 'Stanbic IBTC Bank',
            '068'=> 'Standard Chartered Bank',
            '232'=> 'Sterling Bank',
            '032'=> 'Union Bank',
            '215'=> 'Unity bank',
            '035'=> 'Wema Bank',
            '057'=> 'Zenith Bank'
        ];

		return $data;
     }
     
    
     public function get_image($key){
        return str_replace('\\', '/', '/storage/' . setting($key));
     }
 
}