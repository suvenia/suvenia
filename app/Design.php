<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Design extends Model
{

    protected $table = 'eco_designs';

    public function product(){
        return $this->belongsTo("App\Product");
    }
}
